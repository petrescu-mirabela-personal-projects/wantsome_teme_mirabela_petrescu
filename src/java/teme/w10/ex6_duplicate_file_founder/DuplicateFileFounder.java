package teme.w10.ex6_duplicate_file_founder;

/*
6) Duplicate file founder:

Write a program which will look for duplicate files, anywhere under a given base folder (in any subfolder).
It should also support these 3 ways to check for duplicates:
- by name: find files having exactly same name (but located in different subfolders, and possibly with different content)
- by size: files having exactly same size (in bytes; but possibly different name and/or content)
- by content: files having exactly same content (but possibly different name)

Ideally, it should support combinations of the 3 criteria, like: same name;  same name+size;  same name+content(+size)

The program should print out some summary info (number of duplicate files/groups found), and then the list of duplicate files,
printing for each the full path+name and size, one per line, and grouped together (and separated with some marker lines like '------')

Example of output:

Under folder 'C:\wantsome' found 5 duplicate files (in 2 groups), with same: name+content, total size: 9000 bytes
----------------
C:\wantsome\file1.txt 1500
C:\wantsome\dir1\file1.txt 1500
----------------
C:\wantsome\file2.txt 2000
C:\wantsome\dir1\file2.txt 2000
C:\wantsome\dir2\file2.txt 2000
----------------

Optional:
- support some filtering of the checked files (to avoid scanning them all), by extra criteria like:
  file extension (ex: 'java', 'doc'), minimum and/or maximum file size
 */

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

public class DuplicateFileFounder {

    private static List<File> filesFromPath = new ArrayList<>();

    /*
    search under that base dir (and all its subfolder) - all the files/directories are listed
     */
    private static void recursiveSearch(File dir) {
        // the file or directory denoted by this abstract pathname exists
        File[] filesOrDir = dir.listFiles();
        for (File itemFileOrDir : filesOrDir != null ? filesOrDir : new File[0]) {
            if (itemFileOrDir.isDirectory()) {
                recursiveSearch(itemFileOrDir);
            }
            if (!itemFileOrDir.isDirectory()) {
                /*
                add into a list all the files from the path given as input
                */
                filesFromPath.add(itemFileOrDir);
                System.out.println(itemFileOrDir);
            }
        }
    }

    /*
    add into the list the property of each file
    */
    private static List<FileProperty> addFileProperty() {
        List<FileProperty> listFileProperty = new ArrayList<>();
        for (File itemFileOrDir : filesFromPath) {
            int lastDot;
            if (!itemFileOrDir.isDirectory()) {   // file case
                lastDot = itemFileOrDir.getName().lastIndexOf('.');
                listFileProperty.add(new FileProperty(itemFileOrDir.getPath(), itemFileOrDir.getName(), itemFileOrDir.getName().substring(lastDot + 1), itemFileOrDir.length(), getFileContent(itemFileOrDir)));
            }

        }
        return listFileProperty;
    }

    private static String getFileContent(File file) {
        try {
            StringBuilder fileContent = new StringBuilder();
            Scanner fileScanner = new Scanner(file);
            while (fileScanner.hasNext()) {
                fileContent.append(fileScanner.next());
            }
            return fileContent.toString();
        } catch (IOException e) {
            e.getStackTrace();
        }
        return "";
    }

    /*
    Write a program which will look for duplicate files, anywhere under a given base folder (in any subfolder).
    It should also support these 3 ways to check for duplicates:
        - by name: find files having exactly same name (but located in different subfolders, and possibly with different content)
        - by size: files having exactly same size (in bytes; but possibly different name and/or content)
        - by content: files having exactly same content (but possibly different name)
    Ideally, it should support combinations of the 3 criteria, like: same name;  same name+size;  same name+content(+size)
     */
    private static List<Map.Entry<String, Long>> fileNameCounter(List<FileProperty> listFileProperty) {
        Map<String, Long> listFileNameCounter = listFileProperty
                .stream()
                .map(FileProperty::getFileName)
                .collect(Collectors.groupingBy(s -> s, Collectors.counting()));

        return listFileNameCounter.entrySet()
                .stream()
                .filter(s -> s.getValue() > 1)
                .collect(Collectors.toList());
    }

    /*
    status true if the name is found in the list with duplicate by name files
     */
    private static boolean filterByName(List<FileProperty> listFileProperty, FileProperty s) {
        return (fileNameCounter(listFileProperty)
                .stream()
                .map(Map.Entry::getKey)
                .collect(Collectors.toList()))
                .contains(s.getFileName());
    }

    /*
    get the files which have duplicate names in the base directory
     */
    private static List<FileProperty> findDuplicatedByName(List<FileProperty> listFileProperty) {
        return listFileProperty
                .stream()
                .filter(s -> filterByName(listFileProperty, s))
                .collect(Collectors.toList());
    }


    private static List<Map.Entry<Long, Long>> fileSizeCounter(List<FileProperty> listFileProperty) {
        Map<Long, Long> listFileSizeCounter = listFileProperty
                .stream()
                .map(FileProperty::getFileSize)
                .collect(Collectors.groupingBy(s -> s, Collectors.counting()));

        return listFileSizeCounter.entrySet()
                .stream()
                .filter(s -> s.getValue() > 1)
                .collect(Collectors.toList());
    }

    /*
    status true if the name is found in the list with duplicate by size files
    */
    private static boolean filterBySize(List<FileProperty> listFileProperty, FileProperty s) {
        return (fileSizeCounter(listFileProperty)
                .stream()
                .map(Map.Entry::getKey)
                .collect(Collectors.toList()))
                .contains(s.getFileSize());
    }

    /*
    get the files which have duplicate sizes in the base directory
    */
    private static List<FileProperty> findDuplicatedBySize(List<FileProperty> listFileProperty) {
        return listFileProperty
                .stream()
                .filter(s -> filterBySize(listFileProperty, s))
                .collect(Collectors.toList());
    }

    /*
    get the files which have duplicate name and size in the base directory
    */
    private static List<Map.Entry<String, Long>> fileContentCounter(List<FileProperty> listFileProperty) {
        Map<String, Long> listFileContentCounter = listFileProperty
                .stream()
                .map(FileProperty::getFileContent)
                .collect(Collectors.groupingBy(s -> s, Collectors.counting()));

        return listFileContentCounter.entrySet()
                .stream()
                .filter(s -> s.getValue() > 1)
                .collect(Collectors.toList());
    }

    /*
    status true if the name is found in the list with duplicate by content files
    */
    private static boolean filterByContent(List<FileProperty> listFileProperty, FileProperty s) {
        return (fileContentCounter(listFileProperty)
                .stream()
                .map(Map.Entry::getKey)
                .collect(Collectors.toList()))
                .contains(s.getFileContent());
    }

    /*
    get the files which have duplicate content in the base directory
    */

    private static List<FileProperty> findDuplicatedByContent(List<FileProperty> listFileProperty) {
        return listFileProperty
                .stream()
                .filter(s -> filterByContent(listFileProperty, s))
                .collect(Collectors.toList());
    }

    /*
    support some filtering of the checked files (to avoid scanning them all), by extra criteria like:
  file extension (ex: 'java', 'doc'), minimum and/or maximum file size
     */
    private static List<FileProperty> findFilesByExtension(List<FileProperty> listFileProperty, String fileExtension) {
        return listFileProperty
                .stream()
                .filter(s -> s.getFileExtension().equals(fileExtension))
                .collect(Collectors.toList());
    }

    /*
    Read from user: the path of a base directory and a pattern to search for (a String value)
     */
    private static String readPath() {
        System.out.println("Enter the path of a base directory (example: d:\\Trainings\\Wantsome\\teme_mirabela_petrescu\\src\\java\\teme\\ )");
        Scanner in = new Scanner(System.in); //need this first, to read from input
        return in.next();
    }

    private static String userOption(String text) {
        System.out.print(text);
        return new Scanner(System.in).next();
    }

    public static void main(String[] args) {

        String path = DuplicateFileFounder.readPath();
        File dir = new File(path);
        if (dir.exists()) {     // the file or directory denoted by this abstract pathname exists
            System.out.println("\n >>>>>>>>>> display all files from the path " + path + " >>>>>>>>>>>>");
            recursiveSearch(dir);
            List<FileProperty> listFileProperty = addFileProperty();

            String fileExtensionFromUser;

            switch (userOption("Introduce the ways to check for duplicates: (" +
                    "\n 1 - name OR " +
                    "\n 2 - size OR  " +
                    "\n 3 - content OR  " +
                    "\n 4 - name+size OR  " +
                    "\n 5 - name+content(+size) OR " +
                    "\n 6 - name+file extension OR" +
                    "\n 7 - size+file extension OR  " +
                    "\n 8 - content+file extension OR  " +
                    "\n 9 - name+size+file extension OR  " +
                    "\n 10 - name+content+size+file extension OR " +
                    "\n 11 - files by extension")) {
                case "1":
                    System.out.println("\n >>>>>>>>>> display all duplicates (by name - only name) files - from the path " + path + " >>>>>>>>>>>>");
                    System.out.println(fileNameCounter(listFileProperty));

                    System.out.println("\n >>>>>>>>>> display all duplicates (by name - all properties) files from the path " + path + " >>>>>>>>>>>>");
                    System.out.println(findDuplicatedByName(listFileProperty));
                    break;
                case "2":
                    System.out.println("\n >>>>>>>>>> display all duplicates (by size - only size) files - from the path " + path + " >>>>>>>>>>>>");
                    System.out.println(fileSizeCounter(listFileProperty));

                    System.out.println("\n >>>>>>>>>> display all duplicates (by size - all properties) files from the path " + path + " >>>>>>>>>>>>");
                    System.out.println(findDuplicatedBySize(listFileProperty));
                    break;
                case "3":
                    System.out.println("\n >>>>>>>>>> display all duplicates (by content - all properties) files from the path " + path + " >>>>>>>>>>>>");
                    System.out.println(findDuplicatedByContent(listFileProperty));
                    break;
                case "4":
                    System.out.println("\n >>>>>>>>>> display all duplicates (by name + size - all properties) files from the path " + path + " >>>>>>>>>>>>");
                    System.out.println(findDuplicatedBySize(findDuplicatedByName(listFileProperty)));
                    break;
                case "5":
                    System.out.println("\n >>>>>>>>>> display all duplicates (by name + content + size - all properties) files from the path " + path + " >>>>>>>>>>>>");
                    System.out.println(findDuplicatedByContent(findDuplicatedBySize(findDuplicatedByName(listFileProperty))));
                    break;
                case "6":
                    fileExtensionFromUser = userOption("Enter the file extension to filter: ");
                    System.out.println("\n >>>>>>>>>> display all duplicates (by name - only name) files - from the path " + path + " >>>>>>>>>>>>");
                    System.out.println(fileNameCounter(listFileProperty));

                    System.out.println("\n >>>>>>>>>> display all duplicates (by name - all properties) files from the path " + path + " with the extension " + fileExtensionFromUser + " >>>>>>>>>>>>");
                    System.out.println(findDuplicatedByName(findFilesByExtension(listFileProperty, fileExtensionFromUser)));
                    break;
                case "7":
                    System.out.println("\n >>>>>>>>>> display all duplicates (by size - only size) files - from the path " + path + " >>>>>>>>>>>>");
                    System.out.println(fileSizeCounter(listFileProperty));

                    fileExtensionFromUser = userOption("Enter the file extension to filter: ");
                    System.out.println("\n >>>>>>>>>> display all duplicates (by size - all properties) files from the path " + path + " with the extension " + fileExtensionFromUser + " >>>>>>>>>>>>");
                    System.out.println(findDuplicatedBySize(findFilesByExtension(listFileProperty, fileExtensionFromUser)));
                    break;
                case "8":
                    fileExtensionFromUser = userOption("Enter the file extension to filter: ");
                    System.out.println("\n >>>>>>>>>> display all duplicates (by content - all properties) files from the path " + path + " with the extension " + fileExtensionFromUser + " >>>>>>>>>>>>");
                    System.out.println(findDuplicatedByContent(listFileProperty));
                    break;
                case "9":
                    fileExtensionFromUser = userOption("Enter the file extension to filter: ");
                    System.out.println("\n >>>>>>>>>> display all duplicates (by name + size - all properties) files from the path " + path + " with the extension " + fileExtensionFromUser + " >>>>>>>>>>>>");
                    System.out.println(findDuplicatedBySize(findDuplicatedByName(findFilesByExtension(listFileProperty, fileExtensionFromUser))));
                    break;
                case "10":
                    fileExtensionFromUser = userOption("Enter the file extension to filter: ");
                    System.out.println("\n >>>>>>>>>> display all duplicates (by name + content + size - all properties) files from the path " + path + " with the extension " + fileExtensionFromUser + " >>>>>>>>>>>>");
                    System.out.println(findDuplicatedByContent(findDuplicatedBySize(findDuplicatedByName(findFilesByExtension(listFileProperty, fileExtensionFromUser)))));
                    break;
                case "11":
                    fileExtensionFromUser = userOption("Enter the file extension to filter: ");
                    System.out.println("\n >>>>>>>>>> display files from the path " + path + " with the extension " + fileExtensionFromUser + " >>>>>>>>>>>>");
                    System.out.println(findFilesByExtension(listFileProperty, fileExtensionFromUser));
            }
        } else {
            System.out.println("!!!!!! Not valid directory (path) name");
        }
    }
}