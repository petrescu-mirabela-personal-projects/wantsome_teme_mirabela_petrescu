package teme.w10.ex6_duplicate_file_founder;

class FileProperty {

    private String filePath;
    private String fileName;
    private String fileExtension;
    private long fileSize;
    private String fileContent;

    public FileProperty(String filePath, String fileName, String fileExtension, long fileSize, String fileContent) {
        this.filePath = filePath;
        this.fileName = fileName;
        this.fileExtension = fileExtension;
        this.fileSize = fileSize;
        this.fileContent = fileContent;
    }

    public String getFileName() {
        return fileName;
    }

    public long getFileSize() {
        return fileSize;
    }

    public String getFileContent() {
        return fileContent;
    }

    public String getFileExtension() {
        return fileExtension;
    }

    @Override
    public String toString() {
        return "\nFileProperty{" +
                ", filePath='" + filePath + '\'' +
                ", fileName='" + fileName + '\'' +
                ", fileExtension='" + fileExtension + '\'' +
                ", fileSize=" + fileSize +
                '}';
    }
}
