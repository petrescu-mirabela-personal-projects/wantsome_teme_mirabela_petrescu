package teme.w10.ex2_caesar_cipher;

/*
2) Caesar cipher:

a) Write methods to encrypt and decrypt a text (String) using the Caesar cipher
   see: https://en.wikipedia.org/wiki/Caesar_cipher
   Method will receive as param the original text (String), and a shift value to use (int)

   question: do we need here 2 separate functions, or is one enough?..

b) Write an app which reads from the user a name of an existing text file, and gives him the option
   to encrypt or decrypt that file, using the Caesar cipher (and with a shift value read from user)
   The new text should be written to a new file (with same name as old one, but with a suffix like '_encrypted'/'_decrypted')
 */

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CaesarCipher {

    private static String filePath;
    private static String fileInfoPath;


    public static void main(String[] args) {

        StringBuilder textShifted;
        /*
        a)
         */
        CaesarCipher caesarCipher = new CaesarCipher();
        caesarCipher.caesarGenerator("abcdz", 3);
        caesarCipher.caesarGenerator("defgc", -3);

        /*
        b)
         */
        try {
            filePath = caesarCipher.readFromUser("Enter the file path (example: D:\\Trainings\\Wantsome\\teme_mirabela_petrescu\\src\\java\\teme\\w10\\ex2_caesar_cipher\\CaesarCipher.txt )");
            String shiftValue = caesarCipher.readFromUser("Please add a shift value (positive - to the right, negative - to the left)");
            String encryptDecryptOption = caesarCipher.readFromUser("Please select an option: E for encrypt OR D for decrypt");
            List<String> listAllFileContent = caesarCipher.readFileContent();
            textShifted = new StringBuilder();
            if ((encryptDecryptOption.compareTo("E") == 0) || (encryptDecryptOption.compareTo("D") == 0)) {
                for (String itemLine : listAllFileContent) {
                    textShifted.append(caesarCipher.caesarGenerator(itemLine, Integer.valueOf(shiftValue))).append("\n");
                }
            } else {
                System.out.println(" !!!!!! Wrong option was introduced.");
            }
            fileInfoPath = caesarCipher.generateFileInfoPath();
            try {
                caesarCipher.writeToFileInfo(textShifted.toString());
            } catch (IOException e) {
                System.out.println(" !!!!!! Not a valid info path file => " + fileInfoPath);
            }
        } catch (IOException e) {
            System.out.println(" !!!!!! No file introduced");
        }
    }

    private String caesarGenerator(String sir, int shift) {

        int rangeLength = 'z' - 'a' + 1;
        char[] charArray = sir.toCharArray();

        StringBuilder result = new StringBuilder();
        for (char c : charArray) {
            char newC = (char) (c + shift);
            if ((int) newC > 'z') {
                newC -= rangeLength;
            } else if ((int) newC < 'a') {
                newC += rangeLength;
            }
            result.append(newC);
        }
        System.out.println("Coding/Deconding " + sir + " with shift " + shift + " => " + result.toString());
        return result.toString();
    }

    private String readFromUser(String textToUser) {
        System.out.println(textToUser);
        Scanner in = new Scanner(System.in); //need this first, to read from input
        return in.next();
    }

    private List<String> readFileContent() throws IOException {
        List<String> allFileContent = new ArrayList<>();
        File file = new File(filePath);
        Scanner fileScanner = new Scanner(file);
        while (fileScanner.hasNext()) {
            allFileContent.add(fileScanner.nextLine());
        }
        return allFileContent;
    }

    private String generateFileInfoPath() {
        File f = new File(filePath);
        return "D:" + File.separator + "Trainings" + File.separator + "Wantsome" + File.separator + "teme_mirabela_petrescu" + File.separator + "src" + File.separator + "java" + File.separator + "teme" + File.separator + "w10" + File.separator + "ex2_caesar_cipher" + File.separator + f.getName().replace(".txt", "_ecrypted.txt");
    }

    private void writeToFileInfo(String text) throws IOException {
        try (OutputStream out = new FileOutputStream(fileInfoPath)) {
            char[] cArray = text.toCharArray();
            for (char textChar : cArray) {
                out.write(textChar);
            }
        }
    }
}
