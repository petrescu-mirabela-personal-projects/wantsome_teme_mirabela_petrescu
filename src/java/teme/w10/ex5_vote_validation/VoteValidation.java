package teme.w10.ex5_vote_validation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/*
5) Vote validation:

Implement an application which validates info regarding citizen presence at a voting event.
Requirements:
- the program should work with a base folder (configurable, or hard-coded value), under which it
  will expect 4 folders (should create them if missing), with these fixed names and roles:
   - input          - contains the input csv files to be handled
   - input_archive  - the input files, after their data was read and handled, will be moved here (as a kind of backup), leaving the 'input' folder empty
   - output_valid   - contains the output csv files with the valid results (at most one file is generated on each run of the app)
   - output_invalid - contains the output csv files with the invalid result  (at most one file is generated on each run of the app)

- input data - represents the list of all existing files from the input folder
  - input file names follow this format: <voting location>.csv (ex: 'iasi_sectia1.csv', 'barlad_sectia24.csv')
  - input file content is in CSV format (no header row), with these columns:
    name (string), cnp (string, 13 digits), date (a date+time value, format: 'yyyy-MM-dd HH:mm:ss')

- output data: represents 2 files:
  - a file with all valid results: name format: 'valid_<current date+time>.csv', folder: 'output_valid', content:
    data in CSV format, contains all the valid data from the set of all input files, with these 4 columns:
     - voting location (string, from the name of the row's original file)
     - name, cnp, date - the exact values and format like in the original files (see input file format above)
  - a file with all invalid rows: name: 'invalid_<current date+time>.csv', folder: 'output_invalid', content:
    data in CSV format, contains all the invalid rows from the set of input files, with these 3 columns:
     - error message: a short explanation (with no commas) on why the row is invalid, like: 'missing columns', 'wrong cnp/data format', 'duplicate cnp' ...
     - voting location (string, from the name of the row's original file)
     - row data (string, contains the full/exact row from the original file)

- program operation:
  - read the content of all input files (and the move the input files to the archive folder)
  - try to parse the read lines, according to the expected CSV format; invalid rows should be written to the output file for invalid data
  - for successfully parsed rows, perform some extra validation:
    - CNP format should be correct;
    - CNP values should be unique in whole set of rows
    - voter age (computed from CNP value and current date) should be >= 18 years
    - voter name should contain at least 2 parts (separated by spaces), and each part should have length of at least 3 chars
  - the rows failing the extra validations should again be written to the output file for invalid data
  - the remaining rows should be sorted by voter name, then all written to the output file for valid data (in the expecting format - you need to add the location column..)

- notes:
  - program should be able to run repeatedly, without problems: each time it reads/uses all the files from input folder,
    generates at most 2 output files added to the output folders (with unique names due to including date+time)
    and cleans up the input folder (moving files to archive)
  - if any of the 2 output files to be generated contains no data, they should not be created at all

- hints: for date operations, see: http://tutorials.jenkov.com/java-date-time/parsing-formatting-dates.html

 */
public class VoteValidation {

    public static List<String> listCsvInvalidVoter = new ArrayList<>();

    /*
        search under that base dir (and all its subfolder) - all the files/directories are listed
         */
    private static List<File> recursiveSearch(File dir) {
        List<File> filesFromPath = new ArrayList<>();
        // the file or directory denoted by this abstract pathname exists
        File[] filesOrDir = dir.listFiles();
        for (File itemFileOrDir : filesOrDir != null ? filesOrDir : new File[0]) {
            if (itemFileOrDir.isDirectory()) {
                recursiveSearch(itemFileOrDir);
            }
            if (!itemFileOrDir.isDirectory()) {
                    /*
                    add into a list all the files from the path given as input
                     */
                filesFromPath.add(itemFileOrDir);
            }
        }
        return filesFromPath;
    }

    private static void createDirectories(String dirName) {
        File dir = new File(dirName);
        dir.mkdir();
    }

    /*
    the input files, after their data was read and handled, will be moved here (as a kind of backup), leaving the 'input' folder empty
     */
    private static void copyFileInputToInputArchive(List<File> filesFromInputPath, String inputArchivePath) {
        try {
            for (File csvFileItem : filesFromInputPath) {
                System.out.println(csvFileItem);
                System.out.println(inputArchivePath + File.separator + csvFileItem.getName());
                // !!!!!!!!!!1 move is not working - maybe due to gitlab
                Files.copy(Paths.get(csvFileItem.getPath()), Paths.get(inputArchivePath + File.separator + csvFileItem.getName()));
            }
        } catch (IOException e) {
            System.out.println("!!!! File not found - move");
        }
    }

    private static List<Map.Entry<String, Long>> getListUniqueCnp(List<String> listAllCnp) {
        Map<String, Long> mapCnpCounter = listAllCnp
                .stream()
                .collect(Collectors.groupingBy(s -> s, Collectors.counting()));

        return mapCnpCounter.entrySet()
                .stream()
                .filter(s -> s.getValue() == 1)
                .collect(Collectors.toList());
    }

    /*
    get the list with the unique cnp
     */
    private static List<CsvInputProperty> getVoterWithUniqueCnp(List<CsvInputProperty> listCsvInput) {
        List<Map.Entry<String, Long>> listUniqueCnp = getListUniqueCnp(listCsvInput.stream()
                .map(CsvInputProperty::getCnp)
                .collect(Collectors.toList()));

        return listCsvInput.stream()
                .filter(s -> listUniqueCnp
                        .stream()
                        .map(Map.Entry::getKey)
                        .collect(Collectors.toList())
                        .contains(s.getCnp()))
                .collect(Collectors.toList());
    }

    private static <T> List<T> difference(List<T> list1, List<T> list2) {
        return list1.stream()
                .filter(elem1 -> !list2.contains(elem1))
                .collect(Collectors.toList());
    }

    public static void main(String[] args) {

        String inputPath = "", inputArchivePath = "", outputValidPath = "", outputInvalidPath = "";
        try {
            /*
            contains the input csv files to be handled
             */
            inputPath = "D:\\Trainings\\Wantsome\\teme_mirabela_petrescu\\src\\java\\teme\\w10\\ex5_vote_validation\\input";
            createDirectories(inputPath);
        } catch (Exception e) {
            System.out.println("Folder already created");
        }

        try {
            /*
            the input files, after their data was read and handled, will be moved here (as a kind of backup), leaving the 'input' folder empty
             */
            inputArchivePath = "D:\\Trainings\\Wantsome\\teme_mirabela_petrescu\\src\\java\\teme\\w10\\ex5_vote_validation\\input_archive";
            createDirectories(inputArchivePath);
        } catch (Exception e) {
            System.out.println("Folder already created");
        }

        try {
            /*
            contains the output csv files with the valid results (at most one file is generated on each run of the app)
             */
            outputValidPath = "D:\\Trainings\\Wantsome\\teme_mirabela_petrescu\\src\\java\\teme\\w10\\ex5_vote_validation\\output_valid";
            createDirectories(outputValidPath);
        } catch (Exception e) {
            System.out.println("Folder already created");
        }

        try {
            /*
            contains the output csv files with the invalid result  (at most one file is generated on each run of the app)
             */
            outputInvalidPath = "D:\\Trainings\\Wantsome\\teme_mirabela_petrescu\\src\\java\\teme\\w10\\ex5_vote_validation\\output_invalid";
            createDirectories(outputInvalidPath);
        } catch (Exception e) {
            System.out.println("Folder already created");
        }

        try {
            File inputFiles = new File(inputPath);
            List<File> filesFromInputPath = recursiveSearch(inputFiles);
            CsvInputReader csvInputReader = new CsvInputReader();
            CsvOutputWriter csvOutputWriter = new CsvOutputWriter();
            for (File file : filesFromInputPath) {
                csvInputReader.readCsvInput(file);
            }

            List<CsvInputProperty> listVoterWithUniqueCnp = getVoterWithUniqueCnp(csvInputReader.getListCsvInputReader());

            /*
            a file with all valid results: name format: 'valid_<current date+time>.csv', folder: 'output_valid', content:
            */
            String timeStamp = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss").format(new Date());
            System.out.println(outputValidPath + File.separator + "valid_" + timeStamp + ".csv");

            csvOutputWriter.writeCsvOutput(listVoterWithUniqueCnp, outputValidPath + File.separator + "valid_" + timeStamp + ".csv");

            /*
              - a file with all invalid rows: name: 'invalid_<current date+time>.csv', folder: 'output_invalid', content:
            data in CSV format, contains all the invalid rows from the set of input files, with these 3 columns:
             - error message: a short explanation (with no commas) on why the row is invalid, like: 'missing columns', 'wrong cnp/data format', 'duplicate cnp' ...
             - voting location (string, from the name of the row's original file)
             - row data (string, contains the full/exact row from the original file)
             */
            listCsvInvalidVoter.addAll(difference(csvInputReader.getListCsvInputReader(),
                    listVoterWithUniqueCnp).stream()
                    .map(CsvInputProperty::getCnp)
                    .collect(Collectors.toList()));

            csvOutputWriter.writeInvalidCsvOutput(listCsvInvalidVoter, outputInvalidPath + File.separator + "invalid_" + timeStamp + ".csv");

            /*
            the input files, after their data was read and handled, will be moved here (as a kind of backup), leaving the 'input' folder empty
             */
            copyFileInputToInputArchive(filesFromInputPath, inputArchivePath);

        } catch (FileNotFoundException e) {
            System.out.println(" !!!! File not found - main.");
        }


    }

}
