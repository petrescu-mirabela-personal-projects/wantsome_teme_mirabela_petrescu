package teme.w10.ex5_vote_validation;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

/*
  - a file with all valid results: name format: 'valid_<current date+time>.csv', folder: 'output_valid', content:
    data in CSV format, contains all the valid data from the set of all input files, with these 4 columns:
     - voting location (string, from the name of the row's original file)
     - name, cnp, date - the exact values and format like in the original files (see input file format above)
 */

class CsvOutputWriter {

    public void writeCsvOutput(List<CsvInputProperty> listCsvInput, String csvFilePath) {
        try {
            StringBuilder stringToCsvCopy = new StringBuilder();
            for (CsvInputProperty itemCsvInput : listCsvInput) {
                stringToCsvCopy.append(itemCsvInput.getName()).append(",").append(itemCsvInput.getCnp()).append(",").append(itemCsvInput.getDate()).append("\n");
            }
            writeToFileInfo(stringToCsvCopy.toString(), csvFilePath);
        } catch (IOException e) {
            System.out.println(" !!!! File not found");
        }
    }

    public void writeInvalidCsvOutput(List<String> listCsvInput, String csvFilePath) {
        try {
            StringBuilder stringToCsvCopy = new StringBuilder();
            for (String itemCsvInput : listCsvInput) {
                stringToCsvCopy.append(itemCsvInput).append("\n");
            }
            writeToFileInfo(stringToCsvCopy.toString(), csvFilePath);
        } catch (IOException e) {
            System.out.println(" !!!! File not found");
        }
    }

    private void writeToFileInfo(String text, String csvFilePath) throws IOException {
        try (OutputStream out = new FileOutputStream(csvFilePath)) {
            char[] cArray = text.toCharArray();
            for (char textChar : cArray) {
                out.write(textChar);
            }
        }
    }
}
