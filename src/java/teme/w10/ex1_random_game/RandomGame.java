package teme.w10.ex1_random_game;

import java.util.Random;
import java.util.Scanner;

/*
1) Number guessing game:
a) Code this simple game:
   - computer chooses a random number between 1 and 100
   - it then asks the user to guess the number
   - if user guesses it, is game over; if he didn't guess it, the computer responds with 'Higher'/'Lower'
     depending on how the guess value is compared to the real number
   - it should allow the user max 10 tries (after that, game is lost)

b) Reversed game:
   - now the user is asked to choose a number between 1..100 (and keep it secret)
   - then computer tries to guess it (max 10 attempts), and the user needs to respond if number is lower, equal or higher to his chosen number
   Can you make the computer always win? (what strategy does it need when picking a guess?)
 */

public class RandomGame {

    public static void main(String[] args) {

        System.out.println("\n >>>>>>>>>>>>>>>> User has to introduce the number until it reaches the random value generated");
        userFindTheNumber();

        System.out.println("\n >>>>>>>>>>>>>>>> Computer has to introduce the number until it reaches the user value");
        computerFindTheNumber();
    }

    /*
    a) Code this simple game:
   - computer chooses a random number between 1 and 100
   - it then asks the user to guess the number
   - if user guesses it, is game over; if he didn't guess it, the computer responds with 'Higher'/'Lower'
     depending on how the guess value is compared to the real number
   - it should allow the user max 10 tries (after that, game is lost)
     */

    private static void userFindTheNumber() {
        System.out.println("Enter a number: ");
        String numberIn = new Scanner(System.in).next();
        System.out.println("You enter the " + numberIn);

        int randomNumber = randomNumberGenerator(1, 100);
        int countTries = 1;
        System.out.println("Number of tries is " + countTries);

        while (randomNumber != Integer.valueOf(numberIn) && countTries < 10) {
            if (Integer.valueOf(numberIn) > randomNumber) {
                System.out.println("Enter a number lower than " + Integer.valueOf(numberIn));
                numberIn = new Scanner(System.in).next();
                System.out.println("You enter the " + numberIn);
            } else if (Integer.valueOf(numberIn) < randomNumber) {
                System.out.println("Enter a number higher than " + Integer.valueOf(numberIn));
                numberIn = new Scanner(System.in).next();
                System.out.println("You enter the " + numberIn);
            }
            countTries++;
            System.out.println("Number of tries is " + countTries);
        }
        System.out.println("Game over :)");
        System.out.println("Random number is " + randomNumber);
    }

    /*
    b) Reversed game:
   - now the user is asked to choose a number between 1..100 (and keep it secret)
   - then computer tries to guess it (max 10 attempts), and the user needs to respond if number is lower, equal or higher to his chosen number
   Can you make the computer always win? (what strategy does it need when picking a guess?)
     */

    private static void computerFindTheNumber() {

        System.out.println("Enter a number: ");
        String numberIn = new Scanner(System.in).next();
        System.out.println("You enter the " + numberIn);

        int randomNumber = randomNumberGenerator(1, 100);
        int countTries = 1;

        int min = 1, max = 100;

        // Link: https://www.thoughtco.com/how-to-generate-random-numbers-2034206
        while (randomNumber != Integer.valueOf(numberIn) && countTries < 10) {
            if (Integer.valueOf(numberIn) > randomNumber) {
                min = Integer.valueOf(numberIn);
                randomNumber = randomNumberGenerator(min, max - min + 1);
                System.out.println("Computer enters " + randomNumber);
            } else if (Integer.valueOf(numberIn) < randomNumber) {
                max = Integer.valueOf(numberIn);
                randomNumber = randomNumberGenerator(min, max - min + 1);
                System.out.println("Computer enters " + randomNumber);
            }
            countTries++;
        }
        System.out.println("Game over :)");
        System.out.println("Random number is " + randomNumber);
    }

    private static int randomNumberGenerator(int minRange, int maxRange) {
        Random randomGenerator = new Random();
        return randomGenerator.nextInt(maxRange) + minRange;
    }
}
