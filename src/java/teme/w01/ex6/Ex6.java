package teme.w01.ex6;

/**
 * Ex 6:
 * Digit remover:
 * Write a program that for a given an integer positive number, of exactly 5 digits,
 * it computes and prints a new number based on first one from which the middle digit was
 * removed.
 * For invalid number (not of exactly 5 digits), it should print -1 instead.
 * Examples:
 * 12345 => should print: 1245
 * 1234  => should print: -1
 */
public class Ex6 {

    static int removeMiddleDigit(int n) {
        int nSplit = (n % 100) + (n / 1000) * 100;  // 12345 --> 1245
        System.out.println("nSplit: " + nSplit);

/*      //Case without String:
        boolean validNumber = validDigit(n) && validDigit(n / 10) && validDigit(n / 100) && validDigit(n / 1000) && validDigit(n / 10000);
        System.out.println("validNumber = " + validNumber);

        validNumber = validNumber && (validLength(n) == 5);
        return (validNumber ? nSplit : -1);*/

/*        int sumDigits = n % 10 + (n / 10) % 10 + (n / 100) % 10 + (n / 1000) % 10 + (n / 10000) % 10;
        System.out.println("Length = " + sumDigits);*/

        //
        return (validLength(n) == 5 ? nSplit : -1);
    }

    static int validLength(int n) {
/*       //Case without String:
        System.out.println("validLength call -> n % 100000 = " + (n % 100000));
        System.out.println("validLength call -> n = " + n);
                System.out.println("validLength call: " + (n % 100000 == n && n > 0));
        return (n % 100000 == n && n > 0) ? (n % 100000) : -1;*/

        int nLength = String.valueOf(n).length();
        System.out.println("validLength call -> n = " + nLength);
        return (nLength == 5) ? nLength : -1;
    }

/*  //Case without String:
    static boolean validDigit(int n) {
        return ((n % 10)  == 0) || ((n % 10)  == 1) || ((n % 10)  == 2) || ((n % 10)  == 3) || ((n % 10)  == 4) ||
                ((n % 10)  == 5) ||((n % 10)  == 7) || ((n % 10)  == 8) || ((n % 10)  == 9);
    }*/

    public static void main(String[] args) {
        System.out.println(removeMiddleDigit(12345));
    }
}
