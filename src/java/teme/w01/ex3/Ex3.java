package teme.w01.ex3;

/**
 * Ex 3: Height Converter
 * <p>
 * Write a program to:
 * <p>
 * a. Convert the height of a person from feet and inches (e.g 5 feet 10 inches) to
 * centimeters (178cm). The printed value should be an integer (no decimal part)
 * Hint: to convert (truncate) a double value to an integer, you can use the cast
 * operator: double d = 2.45; int i = ​ (int) ​ d; //i will be 2
 * <p>
 * b. Convert the height of a person from centimeters to feet and inches.
 * The printed values should be two integers.
 */


public class Ex3 {

    /**
     * Convert a length value of X feet + Y inches to equivalent Z centimeters (rounded down to an integer value)
     *
     * @param feet   number of feet
     * @param inches number of inches
     * @return equivalent length in centimeters
     */

    /**
     * Link: http://www.manuelsweb.com/ft_in_cm.htm
     */

    static int convertToCentimeters(int feet, int inches) {

        /**
         * 1 foot = 1 inch / 12  = 1 cm / 2.54 / 12
         * 1 cm = 1 foot * 12 * 254
         * 1 cm  = 2.54 * inches
         * 1cm =  1 foot * 12 * 254 + 2.54 * inches
         */

        return (int) (feet * 12 * 2.54 + inches * 2.54);
    }

    static String convertToFeetAndInches(int centimeters) {

        /**
         * 1 foot = 1 inch / 12  = 1 cm / 2.54 / 12
         * 1 cm = 1 foot * 12 * 254
         * 1 cm  = 2.54 * inches
         * 1 cm =  1 foot * 12 * 2.54 + 2.54 * inches
         */

        double inchTotal = centimeters / 2.54;
        double feetTotal = inchTotal / 12;
        int inches = (int) ((feetTotal - (int) (feetTotal)) * 12);
        int feet = (int) (feetTotal);

        return feet + " feet, " + inches + " inches";
    }

    /**
     * Main method, just for running manual tests
     */
    public static void main(String[] args) {
        //0feet + 3inches = 7.62 cm => should return 7 cm
        System.out.println("convertToCentimeters( 0 feet + 3 inches) = " + convertToCentimeters(0, 3));

        //2feet + 3inches => should return 68 cm
        System.out.println("convertToCentimeters( 2 feet + 3 inches) = " + convertToCentimeters(2, 3));

        //convert 69cm to feet+inches => should return: "2 feet, 3 inches"
        System.out.println("convertToFeetAndInches(68 cm) = '" + convertToFeetAndInches(69) + "'");
    }
}
