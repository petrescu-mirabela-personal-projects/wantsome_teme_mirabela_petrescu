package teme.w01.ex14;

import java.text.DecimalFormat;

/**
 * 14. Quadratic equation:
 * a. Print the complex solutions of the quadratic equation, not just the real ones
 * b. Print the solutions of the quadratic equation with only 2 decimals (3.44 instead of 3.438485435…)
 * Info: algebraic equations of degree two and their solutions
 */

//Link: https://mathbitsnotebook.com/Algebra2/Quadratics/QDQuadratics.html

public class Ex14 {

    private static DecimalFormat df2 = new DecimalFormat(".##");

    /**
     * quadratic equation formula: coeff1*x*x + coeff2*x + coeff3 = 0;
     */
    public static double realPart(double coeff1, double coeff2) {
        return -coeff2 / 2 * coeff1;
    }

    public static double imagPart(double coeff1, double coeff2, double coeff3, double discriminantValue) {

        if (discriminantValue > 0) {
            return Math.sqrt(discriminantValue) / 2 * coeff1;
        } else {
            return Math.sqrt(-discriminantValue) / 2 * coeff1;
        }
    }

    public static String printSolutions(double re, double im, double discriminantValue) {
        if (im == 0) {
            return df2.format(re) + "";
        }
        if (discriminantValue < 0)  // complex solutions
        {
            if (re == 0) {
                return im == 1 ? "i" : df2.format(im) + "i";
            }
            if (im < 0) {
                return im == -1 ? df2.format(re) + "-" + "i" : df2.format(re) + "-" + df2.format((-im)) + "i";
            }
            return im == 1 ? df2.format(re) + "+" + "i" : df2.format(re) + "+" + df2.format(im) + "i";
        } else {
            return df2.format(re + im) + "";
        }
    }

    public static String quadraticEq(double coeff1, double coeff2, double coeff3) {

        double re, im;
        double discriminantValue = coeff2 * coeff2 - 4 * coeff1 * coeff3;
        re = realPart(coeff1, coeff2);
        im = imagPart(coeff1, coeff2, coeff3, discriminantValue);
        if (coeff1 != 0) {
            return "(" + printSolutions(re, im, discriminantValue) + "," + printSolutions(re, -im, discriminantValue) + ")";

        } else {
            System.out.println("Not quadratic equation");
            return "";
        }
    }
}
