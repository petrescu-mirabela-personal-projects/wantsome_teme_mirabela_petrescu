package teme.w01.ex14;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static teme.w01.ex14.Ex14.quadraticEq;

public class Ex14_Tests {

    private static final double DELTA = 0.00001; //precision to use when comparing double values in asserts

    @Test
    public void testlightWavelength() {
        assertEquals("(-2.0+i,-2.0-i)", quadraticEq(1, 4, 5));
        assertEquals("(-1.0,-5.0)", quadraticEq(1, 6, 5));
        assertEquals("(1.0,1.0)", quadraticEq(1, -2, 1));
        assertEquals("(1.5+2.78i,1.5-2.78i)", quadraticEq(1, -3, 10));
    }


}
