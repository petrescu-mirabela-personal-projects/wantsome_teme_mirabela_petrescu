package teme.w01.ex0;

import org.junit.Test;

import static org.junit.Assert.*;
import static teme.w01.ex0.Ex0.*;

/**
 * Automatic (unit) tests for methods of Ex0 class.
 * <p>
 * - You can run all these tests by: right-click on class / 'Run..' option
 * - You should complete/fix your code (in Ex0 class) until all these tests pass with success! (are green on run)
 */

public class Ex0_Tests {

    @Test
    public void testSum() {
        assertEquals(3, computeSum(1, 2));
        assertEquals(0, computeSum(10000, -10000));
    }

    @Test
    public void testSquare() {
        assertEquals(9, squareValue(3));
        assertEquals(25, squareValue(-5));
    }

    @Test
    public void testJoin() {
        assertEquals("abcd", joinStrings("ab", "cd"));
        assertEquals("abc", joinStrings("abc", ""));
        assertEquals("abab", joinStrings("ab", "ab"));
    }

    @Test
    public void testLengthOf() {
        assertEquals(0, lengthOf(""));
        assertEquals(3, lengthOf("abc"));
    }

    @Test
    public void testTruncate() {
        assertEquals(3, truncate(3.123));
        assertEquals(2, truncate(2.999));
    }

    @Test
    public void testIsInInterval() {
        assertTrue(isInInterval(3, 0, 3));
        assertFalse(isInInterval(3, 4, 5));
        assertFalse(isInInterval(3, 0, 2));
    }
}
