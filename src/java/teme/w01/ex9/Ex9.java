package teme.w01.ex9;

/**
 * Ex9: Absolute value method:
 * Write a method to compute and return the absolute value of an input value. The absolute
 * value is defined as: the value itself, if it’s positive, or the value with reversed sign, if it’s
 * negative. Signature should be: static double abs (double value) {...}
 * Write some code to test it for a few values.
 */
public class Ex9 {

    /**
     * @param x some number
     * @return the absolute value of x (value without the sign)
     */
    static double abs(double x) {
        return x > 0 ? x : -x;
    }
}
