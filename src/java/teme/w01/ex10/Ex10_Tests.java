package teme.w01.ex10;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static teme.w01.ex10.Ex10.*;

public class Ex10_Tests {

    private static final double DELTA = 0.00001; //precision to use when comparing double values in asserts

    @Test
    public void testCircleArea() {
        assertEquals(78.5398, computeCircleArea(5), DELTA);
        assertEquals(0.78539, computeCircleArea(0.5), DELTA);
    }

    @Test
    public void testCircleLength() {
        assertEquals(31.41592, computeCircleLength(5), DELTA);
        assertEquals(3.14159, computeCircleLength(0.5), DELTA);
    }

    @Test
    public void testRectangleArea() {
        assertEquals(64.0, computeRectangleArea(8), DELTA);
        assertEquals(0.64, computeRectangleArea(0.8), DELTA);
    }

    @Test
    public void testRectanglePerimeter() {
        assertEquals(32.0, computeRectanglePerimeter(8), DELTA);
        assertEquals(3.2, computeRectanglePerimeter(0.8), DELTA);
    }

    @Test
    public void testGreaterArea() {
        assertEquals("circle", whichHasGreaterArea(5, 8));
        assertEquals("rectangle", whichHasGreaterPerimeter(5, 8));
    }
}
