package teme.w01.ex1;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static teme.w01.ex1.Ex1b.canFormValidRightAngledTriangle;
import static teme.w01.ex1.Ex1b.canFormValidTriangle;

/**
 * Automatic (unit) tests for methods of Ex1b class.
 * <p>
 * - You can run all these tests by: right-click on class / 'Run..' option
 * - You should complete/fix your code in Ex1a class until all these tests pass with success (are green on run)
 */
public class Ex1b_Tests {

    @Test
    public void test_sides_1_2_4() {
        assertFalse("canFormValidTriangle(1,2,4) should return false", canFormValidTriangle(1, 2, 4));
        assertFalse("canFormValidRightAngledTriangle(1,2,4) should return false", canFormValidRightAngledTriangle(1, 2, 4));
    }

    @Test
    public void test_sides_4_1_3() {
        assertFalse("canFormValidTriangle(4,1,3) should return false", canFormValidTriangle(4, 1, 3));
        assertFalse("canFormValidRightAngledTriangle(4,1,3) should return false", canFormValidRightAngledTriangle(4, 1, 3));
    }

    @Test
    public void test_sides_1_2_2() {
        assertTrue("canFormValidTriangle(1,2,2) should return true", canFormValidTriangle(1, 2, 2));
        assertFalse("canFormValidRightAngledTriangle(1,2,2) should return false", canFormValidRightAngledTriangle(1, 2, 2));
    }

    @Test
    public void test_sides_3_4_5() {
        assertTrue("canFormValidTriangle(3,4,5) should return true", canFormValidTriangle(3, 4, 5));
        assertTrue("canFormValidRightAngledTriangle(3,4,5) should return true", canFormValidRightAngledTriangle(3, 4, 5));
    }

    @Test
    public void test_sides_5_3_4() {
        assertTrue("canFormValidTriangle(5,3,4) should return true", canFormValidTriangle(5, 3, 4));
        assertTrue("canFormValidRightAngledTriangle(5,3,4) should return true", canFormValidRightAngledTriangle(5, 3, 4));
    }
}
