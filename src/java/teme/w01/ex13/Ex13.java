package teme.w01.ex13;

/**
 * 13. Given the frequency of some photons in hertzs (e.g 4.3MHz = 4300000), print the
 * corresponding wavelength of the light. If its value is in range of the visible spectrum, also
 * print the corresponding color (eg “green”, “red”, etc.)
 */

// Link 1: http://www.emc2-explained.info/The-Constant-Speed-of-Light/#.XJv2X6NPqM9
// Link 2: https://www.chegg.com/homework-help/photon-frequency-60-x-104-hz-convert-frequency-wavelength-nm-chapter-7-problem-17p-solution-9780077414436-exc
// Link 3: http://www.emc2-explained.info/Speed-Frequency-and-Wavelength/#.XJyyMqNPobw

public class Ex13 {

    public static double lightWavelength(double protonFreq) {
        /**
         * visible light spectrum
         * FORMULA: frequency of photon = speed of wave / wavelength of wave
         */
        // 1nm = Math.pow(10,-9);

        double violetMin = 380;     // [nm] unit
        double violetMax = 450;     // [nm] unit
        double blueMax = 495;       // [nm] unit
        double greenMax = 570;      // [nm] unit
        double yellowMax = 590;     // [nm] unit
        double orangeMax = 620;     // [nm] unit
        double redMax = 750;        // [nm] unit
        double waveSpeed = 3 * Math.pow(10, 8);      // [m/s] unit

        double wavelength;      // [nm] unit

        wavelength = (waveSpeed / protonFreq) / Math.pow(10, -9);

        System.out.println("wave length = " + wavelength);

        if ((wavelength > violetMin) && (wavelength < violetMax)) {
            System.out.println("VIOLET");
        } else if ((wavelength >= violetMax) && (wavelength < blueMax)) {
            System.out.println("BLUE");
        } else if ((wavelength >= blueMax) && (wavelength < greenMax)) {
            System.out.println("GREEN");
        } else if ((wavelength >= greenMax) && (wavelength < yellowMax)) {
            System.out.println("YELLOW");
        } else if ((wavelength >= yellowMax) && (wavelength < orangeMax)) {
            System.out.println("ORANGE");
        } else if ((wavelength >= orangeMax) && (wavelength < redMax)) {
            System.out.println("RED");
        } else {
            System.out.println("out of visible light spectrum");
        }
        return wavelength;
    }
}
