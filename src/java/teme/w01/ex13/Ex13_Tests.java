package teme.w01.ex13;

import org.junit.Test;

import static org.junit.Assert.*;
import static teme.w01.ex13.Ex13.*;

public class Ex13_Tests {

    private static final double DELTA = 0.00001; //precision to use when comparing double values in asserts

    @Test
    public void testlightWavelength() {
        assertEquals(5 * Math.pow(10, 12), lightWavelength(6 * Math.pow(10, 4)), DELTA);
        assertEquals(669.6428571428571, lightWavelength(4.48 * Math.pow(10, 14)), DELTA);
    }


}
