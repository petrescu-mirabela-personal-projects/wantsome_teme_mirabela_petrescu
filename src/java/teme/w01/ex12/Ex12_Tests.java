package teme.w01.ex12;

import org.junit.Test;

import static org.junit.Assert.*;
import static teme.w01.ex12.Ex12.*;

public class Ex12_Tests {

    @Test
    public void testComputeDestinationIfValid() {
        assertEquals(5, numberIntegerInRange(5, 20, 3));
        assertEquals(9, numberIntegerInRange(6, 22, 2));
    }
}
