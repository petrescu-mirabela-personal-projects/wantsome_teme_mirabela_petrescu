package teme.w01.ex12;

/**
 * Write a program to find the number of integers within the range of two numbers received
 * as input and that are divisible by another number (also received as input).
 * Example: x=5, y=20 and p=3, the output should be 5.
 * For x=6, y=22 and p=2, the output should be 9
 */

public class Ex12 {

    static int numberIntegerInRange(int rangeMin, int rangeMax, int number) {
        int count = 0;
        for (int i = rangeMin; i <= rangeMax; i++) {
            if ((i % number) == 0) {
                count = count + 1;
            }
        }
        return count;
    }

    public static void main(String[] args) {
        numberIntegerInRange(5, 20, 3);
        numberIntegerInRange(6, 22, 2);
    }
}
