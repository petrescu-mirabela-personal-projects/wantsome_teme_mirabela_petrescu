package teme.w14.ex2_logger_factory;

/*
Create also a LoggerFactory class, which will be responsible for creating a logger
instance, of one of the available types:
- should have a getLogger(LoggerType) method, which will return a logger
implementation depending on requested type (LoggerType is an enum, currently
with 2 values, for the 2 logger types)
 */

enum LoggerType {
    CONSOLEFILE,
    FILELOGGER
}

public class LoggerFactory {

    public static Logger getLogger(LoggerType type) {
        if (LoggerType.FILELOGGER == type) {
            return new FileLogger();
        } else if (LoggerType.CONSOLEFILE == type) {
            return new ConsoleLogger();
        }
        return null;
    }
}
