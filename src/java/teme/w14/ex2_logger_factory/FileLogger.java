package teme.w14.ex2_logger_factory;

/*
FileLogger: should write log lines to a single text file (file path can be hard-coded
in a constant, and it should create the file if missing, or else just append to it)
 */

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Scanner;

public class FileLogger implements Logger {

    private final static String FILE_PATH = readPath();

    private static String readPath() {
        System.out.println("Enter a file path: (D:\\Trainings\\Wantsome\\teme_mirabela_petrescu\\src\\java\\teme\\w14\\ex2_logger_factory\\loggerFactory.txt)");
        return new Scanner(System.in).next();
    }

    @Override
    public void printInfo() {
        writeToFile("[INFO]\n");
    }

    @Override
    public void printWarn() {
        writeToFile("[WARN]\n");
    }

    @Override
    public void printError() {
        writeToFile("[ERROR]\n");
    }

    private void writeToFile(String text) {
        try {
            OutputStream out = new FileOutputStream(FILE_PATH, true);
            char[] cArray = text.toCharArray();
            for (char textChar : cArray) {
                out.write(textChar);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
