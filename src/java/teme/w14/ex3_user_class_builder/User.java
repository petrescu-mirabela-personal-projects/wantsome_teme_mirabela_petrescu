package teme.w14.ex3_user_class_builder;

/*
Create a User class, for representing the users of some system.
It needs to have these fields:
- username: String, mandatory
- email: String, mandatory
- firstName, lastName: String, is mandatory to have one of them set (user can
have only one or both of them set, but cannot have both missing)
- age: int, optional
- gender: an enum, optional (but if specified, must be >=14)
- address: String, optional (if specified, must be at least 8 char long)
 */

enum Gender {
    FEMALE,
    MALE
}

public class User {

    //1) Has many fields (some mandatory + some optional)
    /*
    mandatory fields
     */
    String userName;
    String email;
    /*
    optional fields
     */
    String firstName;
    String lastName;
    int age;
    Gender gender;
    String address; // 8 char long

    //2) Has a SINGLE CONSTRUCTOR, PRIVATE, with params for ALL fields! (mandatory+optional)
    private User(String userName, String email, String firstName, String lastName, int age, Gender gender, String address) {
        this.userName = userName;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.gender = gender;
        this.address = address;
    }

    //3) Fields are read-only, so ONLY GETTERS here (no setters)
    public String getUserName() {
        return userName;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public Gender getGender() {
        return gender;
    }

    public String getAddress() {
        return address;
    }

    @Override
    public String toString() {
        return "User{" +
                "userName='" + userName + '\'' +
                ", email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", gender=" + gender +
                ", address='" + address + '\'' +
                "}\n";
    }

    //1) SEPARATE BUILDER CLASS, NEEDS to be a STATIC INNER CLASS of it (to have access to Car private constructor)
    public static class UserBuilder {

        //2) Has COPIES of ALL FIELDS of target class (Car)
        String userName;
        String email;
        String firstName;
        String lastName;
        int age;
        Gender gender;
        String address; // 8 char long

        //3) Has a SINGLE CONSTRUCTOR, PUBLIC, with params for all MANDATORY fields

        public UserBuilder(String userName, String email) {
            this.userName = userName;
            this.email = email;
        }

        //4) Has 'setter-like' methods for all OPTIONAL fields; each one sets the field value (like a regular setter),
        // and then also returns 'this', instead of void (for easier chaining of multiple calls later)

        public UserBuilder setFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public UserBuilder setLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public UserBuilder setAge(int age) {
            this.age = age;
            return this;
        }

        public UserBuilder setGender(Gender gender) {
            this.gender = gender;
            return this;
        }

        public UserBuilder setAddress(String address) {
            if (address.length() >= 8) {
                this.address = address;
            } else {
                this.address = "Address shall be at least 8 char long";
            }
            return this;
        }

        //5) Has a build() method, to be called at the end to actually build and return an instance of target class
        // (based on current builder settings), possibly also running now some final validations
        public User build() {
            if (this.firstName == null && this.lastName == null) {
                firstName = "! User can not have both firstName and lastName missing";
                lastName = "! User can not have both firstName and lastName missing";
                return new User(userName, email, firstName, lastName, age, gender, address);
            } else {
                return new User(userName, email, firstName, lastName, age, gender, address);
            }
        }
    }
}
