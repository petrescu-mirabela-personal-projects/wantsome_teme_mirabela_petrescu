package teme.w12.ex0_warmup_todo;

/*
Create a simple application to manage a Todo list:
- It should manage Todo items, each having these properties:
- description (String, up to 200 char; mandatory)
- priority (low/medium/high; mandatory)
- deadline (a date; optional, can be empty)
- state (completed or still active; mandatory)
 */

import java.sql.Date;

public class TodoDTO {
    private long id;
    private String description;
    private DBConstants.Priority priority;
    private Date deadline;
    private DBConstants.State state;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public DBConstants.Priority getPriority() {
        return priority;
    }

    public void setPriority(DBConstants.Priority priority) {
        this.priority = priority;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public DBConstants.State getState() {
        return state;
    }

    public void setState(DBConstants.State state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "Todo{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", priority=" + priority +
                ", deadline=" + deadline +
                ", state=" + state +
                '}';
    }
}
