-- 0. Warmup: To-do list
-- Create a simple application to manage a Todo list:
-- - It should manage Todo items, each having these properties:
-- - description (String, up to 200 char; mandatory)
-- - priority (low/medium/high; mandatory)
-- - deadline (a date; optional, can be empty)
-- - state (completed or still active; mandatory)
-- - It should persist the introduced data in a database (so it is available
-- when run again later)
-- - It should allow the user to perform these operations:
-- - Display a list of all items still Active, with 2 possible sorting
-- options:
-- - by date (ascending)
-- - or: by priority (descending, from High to Low)
-- - create new items
-- - delete existing items
-- - update existing items by changing their state (between Active
-- and Completed)

create table todo(
id int primary key auto_increment,
description char(200) not null,
priority enum ('LOW', 'MEDIUM', 'HIGH') not null,
deadline DATETIME,
state enum ('COMPLETED', 'STILL_ACTIVE') not null);

insert into todo (description, priority, deadline, state) values ('Do the homework', 'HIGH', '2019/06/15', 'COMPLETED');
insert into todo (description, priority, deadline, state) values ('Clean the room', 'MEDIUM', '2019/06/16', 'STILL_ACTIVE');
insert into todo (description, priority, deadline, state) values ('Go to work', 'MEDIUM', '2019/06/18', 'STILL_ACTIVE');
insert into todo (description, priority, deadline, state) values ('Go to beach', 'LOW', '2019/08/15', 'STILL_ACTIVE');
insert into todo (description, priority, deadline, state) values ('Stay at home', 'LOW', null, 'STILL_ACTIVE');

select * from todo;

-- drop table todo;
