package teme.w12.ex1_pet_shop;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class PersonReader {

    private final List<PersonDTO> listPersonReader = new ArrayList<>();

    public List<PersonDTO> getListPersonReader() {
        return listPersonReader;
    }

    @Override
    public String toString() {
        return "PersonReader{" +
                "listPersonReader=" + listPersonReader +
                '}';
    }

    public void readPersonFromCsv(String csvFilePath) throws FileNotFoundException, ArrayIndexOutOfBoundsException {
        File csvFile = new File(csvFilePath);
        int lineCount = 0;
        Scanner fileScanner = new Scanner(csvFile);
        while (fileScanner.hasNext()) {
            String line = fileScanner.nextLine();
            String[] csvLineArray = line.split(",");
            lineCount++;
            try {
                listPersonReader.add(new PersonDTO(Integer.valueOf(csvLineArray[0]), csvLineArray[1], csvLineArray[2], Integer.valueOf(csvLineArray[3]), csvLineArray[4]));
            } catch (NumberFormatException e1) {
                System.out.println("Line " + lineCount + " has wrong type => " + Arrays.toString(csvLineArray));
            } catch (ArrayIndexOutOfBoundsException e2) {
                System.out.println("Line " + lineCount + " has less fields than expected => " + Arrays.toString(csvLineArray));
            } catch (IllegalArgumentException e3) {
                System.out.println("Line " + lineCount + " has wrong type => " + Arrays.toString(csvLineArray));        // SEX enum case
            }
        }
    }
}
