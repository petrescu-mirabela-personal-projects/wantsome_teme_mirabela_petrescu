-- 1. Pet Shop
-- Design the database and offer full CRUD functionality for a basic pet shop
-- application (handling pets and pet owners).
-- Given these three tables, with these columns:
	-- PETS – id int not null, name varchar(100) not null, person_id int
	-- nullable, birthdate date not null, gender varchar(1) not null, type int
	-- not null
	-- PET_TYPES – id int not null, description varchar(50) not null
	-- PERSONS – id int not null, first_name varchar(100) not null,
	-- last_name varchar(100) not null, age int not null, gender varchar(1)
	-- not null
-- 1. Create database structure in MySql (tables, appropriate constraints:
-- primary keys, foreign keys as a bonus)
-- 2. Implement full CRUD functionality for each entity (Pet, PetType, Person)
-- 3. Populate the tables with information from a CSV file (read from file and
-- use DAOs to populate the DB)
-- 4. Offer the option of assigning pets to persons (update pets with
-- appropriate person id)
-- 5. Select all pets of a certain type.
-- 6. Select all owned pets with their description and information about their
-- owners.

drop table pets;
drop table pet_types;
drop table person;

create table pet_types(
id int primary key auto_increment, 
description varchar(50) not null);

create table person(
id int primary key auto_increment, 
first_name varchar(100) not null,
last_name varchar(100) not null, 
age int not null, 
gender varchar(1) not null);

create table pets( 
id int primary key auto_increment, 
name varchar(100) not null,
person_id int,
birthdate date not null,
gender varchar(1) not null,
type int not null,
foreign key (type) references pet_types (id),
foreign key (person_id) references person (id));

select * from person;
select * from pets;
select * from pet_types;