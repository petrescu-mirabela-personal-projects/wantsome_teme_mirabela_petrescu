package teme.w12.ex1_pet_shop;

/*
1. Pet Shop
Design the database and offer full CRUD functionality for a basic pet shop
application (handling pets and pet owners).
Given these three tables, with these columns:
PETS – id int not null, name varchar(100) not null, person_id int
nullable, birthdate date not null, gender varchar(1) not null, type int
not null
PET_TYPES – id int not null, description varchar(50) not null
PERSONS – id int not null, first_name varchar(100) not null,
last_name varchar(100) not null, age int not null, gender varchar(1)
not null
1. Create database structure in MySql (tables, appropriate constraints:
primary keys, foreign keys as a bonus)
2. Implement full CRUD functionality for each entity (Pet, PetType, Person)
3. Populate the tables with information from a CSV file (read from file and
use DAOs to populate the DB)
4. Offer the option of assigning pets to persons (update pets with
appropriate person id)
5. Select all pets of a certain type.
6. Select all owned pets with their description and information about their
owners.
 */

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class PetShopMain {

    public static void main(String[] args) {

        PersonReader personReader = new PersonReader();
        PetsReader petsReader = new PetsReader();
        PetTypesReader petTypesReader = new PetTypesReader();

        try {
            String petTypesCsvPath = readInputFromUser("System.out.println(Enter the csv path: D:\\Trainings\\Wantsome\\teme_mirabela_petrescu\\src\\java\\teme\\w12\\ex1_pet_shop\\pet_types.csv");
            String personCsvPath = readInputFromUser("System.out.println(Enter the csv path: D:\\Trainings\\Wantsome\\teme_mirabela_petrescu\\src\\java\\teme\\w12\\ex1_pet_shop\\person.csv");
            String petsCsvPath = readInputFromUser("System.out.println(Enter the csv path: D:\\Trainings\\Wantsome\\teme_mirabela_petrescu\\src\\java\\teme\\w12\\ex1_pet_shop\\pets.csv");

            petTypesReader.readPetTypesFromCsv(petTypesCsvPath);
            personReader.readPersonFromCsv(personCsvPath);
            petsReader.readPetsFromCsv(petsCsvPath);


            petTypesReader.getListPetTypesReader();
            System.out.println("\n >>>>>>>>>>>> Pet types from csv:  >>>>>>>>>>>> \n" + petTypesReader.toString());
            personReader.getListPersonReader();
            System.out.println("\n >>>>>>>>>>>> Persons from csv:  >>>>>>>>>>>> \n" + personReader);
            petsReader.getListPetsReader();
            System.out.println("\n >>>>>>>>>>>> Pets from csv:  >>>>>>>>>>>> \n" + petsReader);

            /*
            3. Populate the tables with information from a CSV file (read from file and use DAOs to populate the DB)
             */
            PersonDAO personDAO = new PersonDAO();
            PetsDAO petsDAO = new PetsDAO();
            PetTypesDAO petTypesDAO = new PetTypesDAO();

            List<PersonDTO> personDTOList;
            List<PetsDTO> petsDTOList;
            List<PetTypesDTO> petTypesDTOList;

            petTypesReader.getListPetTypesReader().forEach(petTypesDAO::insert);
            personReader.getListPersonReader().forEach(personDAO::insert);
            petsReader.getListPetsReader().forEach(petsDAO::insert);

            System.out.println();
            petTypesDTOList = petTypesDAO.get();
            System.out.println("After first insert pet types list size is: " + petTypesDTOList.size());
            personDTOList = personDAO.get();
            System.out.println("After first insert person list size is: " + personDTOList.size());
            petsDTOList = petsDAO.get();
            System.out.println("After first insert pets list size is: " + petsDTOList.size());

            /*
            4. Offer the option of assigning pets to persons (update pets with appropriate person id)
             */
            int updatePetsId = Integer.valueOf(readInputFromUser("\nGive the pets id to be replaced. Between 1 and " + petsDTOList.size()));
            petsDAO.update(assignPetToPerson(updatePetsId, petsDTOList, personDTOList));
            System.out.println("Updated pet (from database): ");
            System.out.println(petsDAO.get(updatePetsId));

            /*
            5. Select all pets of a certain type.
             */
            System.out.println(" >>>>>>>> All pets of certain type >>>>>>>: \n" + getPetsOfCertainType(petsDTOList, petTypesDTOList));

            System.out.println();

            /*
            6. Select all owned pets with their description and information about their owners.
            */
            System.out.println(" >>>>>>>> All owned pets with their description and information about their owners >>>>>>>: \n");
            getOwnedPetsAndOwnerInfo(petsDTOList, personDTOList);

        } catch (FileNotFoundException e) {
            System.out.println("Not a valid csv path");
        }
    }

    private static String readInputFromUser(String text) {
        System.out.println(text);
        Scanner in = new Scanner(System.in);
        return in.next();
    }


    /**
     * 4. Offer the option of assigning pets to persons (update pets with appropriate person id)
     *
     * @param petsDTOList
     * @param personDTOList
     */
    private static PetsDTO assignPetToPerson(int updatePetsId, List<PetsDTO> petsDTOList, List<PersonDTO> personDTOList) {

        Integer updatePetsWithPersonId = Integer.valueOf(readInputFromUser("\nGive the person_id to be replaced in pets. Between 1 and " + personDTOList.size()));

        PetsDTO petsNewItem = petsDTOList.get(updatePetsId - 1);
        petsNewItem.setPerson_id(updatePetsWithPersonId);
        System.out.println("Updated pet (value which shall be introduced in database): " + petsNewItem);
        return petsNewItem;
    }

    /**
     * 5. Select all pets of a certain type.
     *
     * @param petsDTOList
     * @param petTypesDTOList
     * @return
     */
    private static List<PetsDTO> getPetsOfCertainType(List<PetsDTO> petsDTOList, List<PetTypesDTO> petTypesDTOList) {
        String userPetTypeOption = readInputFromUser("\nGive a type to search. Possible options (description): dog OR cat");
        return petsDTOList.stream()
                .filter(s ->
                        (petTypesDTOList
                                .stream()
                                .filter(v -> v.getDescription().equalsIgnoreCase(userPetTypeOption))
                                .findFirst()
                        ).get().getId() == s.getType())
                .collect(Collectors.toList());
    }


    /**
     * 6. Select all owned pets with their description and information about their owners.
     *
     * @param petsDTOList
     * @param personDTOList
     */
    private static void getOwnedPetsAndOwnerInfo(List<PetsDTO> petsDTOList, List<PersonDTO> personDTOList) {
        for (PetsDTO petsDTO : petsDTOList) {
            for (PersonDTO personDTO : personDTOList) {
                if (petsDTO.getPerson_id() == personDTO.getId()) {
                    System.out.print(petsDTO);
                    System.out.print(personDTO);
                    System.out.println();
                }
            }
        }
    }
}
