package teme.w12.ex1_pet_shop;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class PetsReader {

    private final List<PetsDTO> listPetsReader = new ArrayList<>();

    public List<PetsDTO> getListPetsReader() {
        return listPetsReader;
    }

    @Override
    public String toString() {
        return "PetsReader{" +
                "listPetsReader=" + listPetsReader +
                '}';
    }

    public void readPetsFromCsv(String csvFilePath) throws FileNotFoundException, ArrayIndexOutOfBoundsException {
        File csvFile = new File(csvFilePath);
        int lineCount = 0;
        Scanner fileScanner = new Scanner(csvFile);
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        while (fileScanner.hasNext()) {
            String line = fileScanner.nextLine();
            String[] csvLineArray = line.split(",");
            lineCount++;
            try {
                // Link: https://stackoverflow.com/questions/10413350/date-conversion-from-string-to-sql-date-in-java-giving-different-output/17673514
                java.util.Date date = format.parse(csvLineArray[3]);
                java.sql.Date sqlStartDate = new java.sql.Date(date.getTime());
                listPetsReader.add(new PetsDTO(Integer.valueOf(csvLineArray[0]), csvLineArray[1], Integer.valueOf(csvLineArray[2]), sqlStartDate, csvLineArray[4], Integer.valueOf(csvLineArray[5])));
            } catch (NumberFormatException e1) {
                System.out.println("Line " + lineCount + " has wrong type => " + Arrays.toString(csvLineArray));
            } catch (ArrayIndexOutOfBoundsException e2) {
                System.out.println("Line " + lineCount + " has less fields than expected => " + Arrays.toString(csvLineArray));
            } catch (IllegalArgumentException e3) {
                System.out.println("Line " + lineCount + " has wrong type => " + Arrays.toString(csvLineArray));        // SEX enum case
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }
}
