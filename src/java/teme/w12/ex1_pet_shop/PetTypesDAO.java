package teme.w12.ex1_pet_shop;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class PetTypesDAO extends AbstractDao<PetTypesDTO> {
    @Override
    protected List<PetTypesDTO> executeSelect(Connection c) {
        List<PetTypesDTO> petTypesDTOList = new ArrayList<>();
        String sql = "SELECT * FROM pet_types ORDER BY id";
        try (PreparedStatement preparedStatement = c.prepareStatement(sql);
             ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                PetTypesDTO petTypesDTO = new PetTypesDTO();
                petTypesDTO.setId(resultSet.getInt("id"));
                petTypesDTO.setDescription(resultSet.getString("description"));
                petTypesDTOList.add(petTypesDTO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return petTypesDTOList;
    }

    @Override
    protected PetTypesDTO executeSelect(Connection c, int id) {
        String sql = "SELECT * FROM pet_types WHERE id = ?";
        PetTypesDTO petTypesDTO = new PetTypesDTO();
        try (PreparedStatement preparedStatement = c.prepareStatement(sql)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                petTypesDTO.setId(resultSet.getInt("id"));
                petTypesDTO.setDescription(resultSet.getString("description"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return petTypesDTO;
    }

    @Override
    protected void executeInsert(Connection c, PetTypesDTO object) {
        String sql = "INSERT INTO pet_types (id, description) VALUES (?, ?)";
        try (PreparedStatement preparedStatement = c.prepareStatement(sql)) {
            preparedStatement.setInt(1, object.getId());
            preparedStatement.setString(2, object.getDescription());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void executeUpdate(Connection c, PetTypesDTO object) {
        String sql = "UPDATE pet_types SET description = ? WHERE id = ?";
        try (PreparedStatement preparedStatement = c.prepareStatement(sql)) {
            preparedStatement.setString(1, object.getDescription());
            preparedStatement.setInt(2, object.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void executeDelete(Connection c, PetTypesDTO object) {
        String sql = "DELETE from pet_types WHERE id = ?";
        try (PreparedStatement preparedStatement = c.prepareStatement(sql)) {
            preparedStatement.setInt(1, object.getId());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
