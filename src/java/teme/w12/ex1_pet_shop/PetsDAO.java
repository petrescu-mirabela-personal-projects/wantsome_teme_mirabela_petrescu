package teme.w12.ex1_pet_shop;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PetsDAO extends AbstractDao<PetsDTO> {

    @Override
    protected List<PetsDTO> executeSelect(Connection c) {
        String sql = "SELECT * FROM pets ORDER BY id";
        List<PetsDTO> petsDAOList = new ArrayList<>();
        try (PreparedStatement preparedStatement = c.prepareStatement(sql);
             ResultSet resultSet = preparedStatement.executeQuery();) {
            while (resultSet.next()) {
                PetsDTO petsDTO = new PetsDTO();
                petsDTO.setId(resultSet.getInt("id"));
                petsDTO.setName(resultSet.getString("name"));
                petsDTO.setPerson_id(resultSet.getInt("person_id"));
                petsDTO.setBirthdate(resultSet.getDate("birthdate"));
                petsDTO.setGender(resultSet.getString("gender"));
                petsDTO.setType(resultSet.getInt("type"));
                petsDAOList.add(petsDTO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return petsDAOList;
    }

    @Override
    protected PetsDTO executeSelect(Connection c, int id) {
        String sql = "SELECT * FROM pets WHERE id = ?";
        PetsDTO petsDTO = new PetsDTO();
        try (PreparedStatement preparedStatement = c.prepareStatement(sql)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                petsDTO.setId(resultSet.getInt("id"));
                petsDTO.setName(resultSet.getString("name"));
                petsDTO.setPerson_id(resultSet.getInt("person_id"));
                petsDTO.setBirthdate(resultSet.getDate("birthdate"));
                petsDTO.setGender(resultSet.getString("gender"));
                petsDTO.setType(resultSet.getInt("type"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return petsDTO;
    }

    @Override
    protected void executeInsert(Connection c, PetsDTO object) {
        String sql = "INSERT INTO pets (name, person_id, birthdate, gender, type) VALUES (?, ?, ?, ?, ?) ";
        try (PreparedStatement preparedStatement = c.prepareStatement(sql)) {
            preparedStatement.setString(1, object.getName());
            preparedStatement.setInt(2, object.getPerson_id());
            preparedStatement.setDate(3, object.getBirthdate());
            preparedStatement.setString(4, object.getGender());
            preparedStatement.setInt(5, object.getType());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void executeUpdate(Connection c, PetsDTO object) {
        String sql = "UPDATE pets SET name = ?, person_id = ?, birthdate = ?, gender = ?, type = ? WHERE id = ? ";
        try (PreparedStatement preparedStatement = c.prepareStatement(sql)) {
            preparedStatement.setString(1, object.getName());
            preparedStatement.setInt(2, object.getPerson_id());
            preparedStatement.setDate(3, object.getBirthdate());
            preparedStatement.setString(4, object.getGender());
            preparedStatement.setInt(5, object.getType());
            preparedStatement.setInt(6, object.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void executeDelete(Connection c, PetsDTO object) {
        String sql = "DELETE FROM pets WHERE id = ?";
        try (PreparedStatement preparedStatement = c.prepareStatement(sql)) {
            preparedStatement.setInt(1, object.getId());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
