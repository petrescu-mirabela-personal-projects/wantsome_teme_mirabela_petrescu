package teme.w09.ex4_file_in_folder;

class FileProperty {

    private final boolean fileType;
    private final String fileName;
    private final long fileSize;

    public FileProperty(boolean fileType, String fileName, long fileSize) {
        this.fileType = fileType;
        this.fileName = fileName;
        this.fileSize = fileSize;
    }

    public String getFileName() {
        return fileName;
    }


    @Override
    public String toString() {
        return "FileProperty{" +
                "fileType(Directory=true, File=false)=" + fileType +
                ", fileName='" + fileName + '\'' +
                ", fileSize=" + fileSize +
                '}';
    }
}
