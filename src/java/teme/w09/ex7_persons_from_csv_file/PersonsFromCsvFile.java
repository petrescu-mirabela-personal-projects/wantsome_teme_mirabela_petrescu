package teme.w09.ex7_persons_from_csv_file;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.Collectors;

/*
7. Persons from CSV file
You are given a CSV (Comma Separated Values) file, which contains data about a list of
persons (persons.csv)
It doesn't have a header row, so all rows represent persons and have the same format:
[name],[cnp],[age],[height],[sex]
Field types:
name - a String
cnp - a long number
age, height - a positive integer value
sex - one of these 2 values: 'F'/'M'
Write a program which should:
- load all rows from file, skipping the invalid ones (invalid rows like: having less fields than
expected, or with values of different types than expected -> you could also show an error
message for each ignored row)
- display the number of valid records found
- find the oldest person and display some info about it (name+age)
- display the list of all (valid) persons, sorted ascending by name
- save this sorted list to a new csv file, with same format as initial one (named
'persons_sorted.txt')
Hint : you could organize your code in a few separate classes:
- Person (with all fields for it/equals()/toString()..),
- PersonComparator (for sorting by name),
- PersonWriter and PersonReader (for writing a List<Person> to a CSV file and for loading
such a list from it).
Then manually test the writer and reader classes to make sure they work ok, and then
start using them for rest of assignment..
 */
public class PersonsFromCsvFile {

    public static void main(String[] args) {

        PersonReader personReader = new PersonReader();
        PersonWriter personWriter = new PersonWriter();

        try {
            /*
            load all rows from file, skipping the invalid ones (invalid rows like: having less fields than
            expected, or with values of different types than expected -> you could also show an error
            message for each ignored row)
             */
            String csvFilePath = readFilePath();
            personReader.readPersonFromCsv(csvFilePath);
            System.out.println("\n >>>>>>>> The valid persons from " + " file are: >>>>>>>>  \n" + personReader.getListPersonReader());

            /*
            display the number of valid records found
             */
            System.out.println("\n >>>>>>>> The number of valid records is: " + personReader.getListPersonReader().size());

           /*
           find the oldest person and display some info about it (name+age)
            */
            System.out.println("\n >>>>>>>> The oldest person is: " + findTheOldestPerson(personReader));

            /*
            display the list of all (valid) persons, sorted ascending by name
             */
            List<Person> listValidPersonAscendingByName = findValidPersonsAscendingByName(personReader);
            System.out.println("\n >>>>>>>> The valid persons sorted by name from " + " file are: >>>>>>>>  \n" + listValidPersonAscendingByName);

            /*
            save this sorted list to a new csv file, with same format as initial one (named 'persons_sorted.txt')
             */
            savePersonsSorted(listValidPersonAscendingByName, personWriter, generateFileInfoPath(csvFilePath));

        } catch (FileNotFoundException e) {
            System.out.println("Not a valid csv path");
        }

    }

    /*
Read from user: the path of a base directory and a pattern to search for (a String value)
 */
    private static String readFilePath() {
        System.out.print("Enter the file path (example: D:\\Trainings\\Wantsome\\teme_mirabela_petrescu\\src\\java\\teme\\w09\\ex7_persons_from_csv_file\\persons.csv )");
        Scanner in = new Scanner(System.in); //need this first, to read from input
        return in.next();
    }

    private static Optional<Person> findTheOldestPerson(PersonReader personReader) {
        return personReader.getListPersonReader()
                .stream().min(new PersonComparatorByAge());
    }

    private static List<Person> findValidPersonsAscendingByName(PersonReader personReader) {
        return personReader.getListPersonReader()
                .stream()
                .sorted(new PersonComparatorByName())
                .collect(Collectors.toList());
    }

    private static void savePersonsSorted(List<Person> listPersonsSorted, PersonWriter personWriter, String csvFilePath) {
        try {
            personWriter.writePersonToCsv(listPersonsSorted, csvFilePath);
        } catch (IOException e) {
            System.out.println("Not a valid file");
        }
    }

    private static String generateFileInfoPath(String filePath) {
        return filePath.replace(".csv", "_sorted.txt");
    }
}
