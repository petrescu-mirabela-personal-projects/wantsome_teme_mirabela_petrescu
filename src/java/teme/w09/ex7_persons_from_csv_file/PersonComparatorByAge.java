package teme.w09.ex7_persons_from_csv_file;

import java.util.Comparator;

class PersonComparatorByAge implements Comparator<Person> {

    @Override
    public int compare(Person o1, Person o2) {
        return -Long.compare(o1.getAge(), o2.getAge());
    }
}
