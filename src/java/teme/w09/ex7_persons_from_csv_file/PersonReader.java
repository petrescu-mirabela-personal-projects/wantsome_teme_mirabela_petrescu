package teme.w09.ex7_persons_from_csv_file;

/*
PersonReader for loading such a list from it
*/

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

class PersonReader {
    private final List<Person> listPersonReader = new ArrayList<>();

    public List<Person> getListPersonReader() {
        return listPersonReader;
    }

    @Override
    public String toString() {
        return "PersonReader{" +
                "listPersonReader=" + listPersonReader +
                '}';
    }

    public void readPersonFromCsv(String csvFilePath) throws FileNotFoundException, ArrayIndexOutOfBoundsException {
        File csvFile = new File(csvFilePath);
        int lineCount = 0;
        Scanner fileScanner = new Scanner(csvFile);
        while (fileScanner.hasNext()) {
            String line = fileScanner.nextLine();
            String[] csvLineArray = line.split(",");
            lineCount++;
            try {
                /* PATTERN
                Link: https://www.geeksforgeeks.org/check-if-a-string-contains-only-alphabets-in-java-using-regex/
                Link: https://docs.oracle.com/javase/7/docs/api/java/util/regex/Pattern.html
                Boundary matchers:      ^	The beginning of a line
                                        $	The end of a line
                Greedy quantifiers      X*	X, zero or more times
                 */
                if (csvLineArray[0].replace(" ", "").matches("^[a-zA-Z]*$")) {
                    listPersonReader.add(new Person(csvLineArray[0], Long.valueOf(csvLineArray[1]), Integer.valueOf(csvLineArray[2]), Integer.valueOf(csvLineArray[3]), SEX.valueOf(csvLineArray[4])));
                } else {
                    System.out.println("Line " + lineCount + " => Wrong type of " + csvLineArray[0] + ". It shall be String");
                }
            } catch (NumberFormatException e1) {
                System.out.println("Line " + lineCount + " has wrong type => " + Arrays.toString(csvLineArray));
            } catch (ArrayIndexOutOfBoundsException e2) {
                System.out.println("Line " + lineCount + " has less fields than expected => " + Arrays.toString(csvLineArray));
            } catch (IllegalArgumentException e3) {
                System.out.println("Line " + lineCount + " has wrong type => " + Arrays.toString(csvLineArray));        // SEX enum case
            }
        }
    }
}