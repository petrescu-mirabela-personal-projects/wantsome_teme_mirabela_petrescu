package teme.w09.ex1_Greeting;

/*
1. Greeting with Properties file
Write a greeting app which remembers the user:
- it should display a message like: "Hello, [user name], nice to see you again! (last visit
time: [h:m:s])”
- it will use a properties files (named 'user.properties') to store the username and the time
of last visit
- it will first try to find the file and load the 2 expected keys from it:
- if keys are loaded, just use them
- if file/keys are missing, ask first the user for his name, and use that one (also
saving it to the properties file)
Note: the program should ask the user for the name only once (when missing, and save it to
prop file); but it needs to update the time of the last visit on each run.
 */

import java.io.*;
import java.util.Date;
import java.util.Properties;
import java.util.Scanner;

public class Greeting {

    private static final String fileProperties = "src" + File.separator + "java" + File.separator + "teme" + File.separator + "w09" + File.separator + "ex1_Greeting" + File.separator + "user.properties";

    public static void main(String[] args) {
        try {
            manageProperties(new Date());
        } catch (IOException e) {
            System.out.println("No user.properties file");
        }

    }

    private static void manageProperties(Date date) throws IOException {
        System.out.println("Reading properties from " + fileProperties);
        Properties prop = new Properties();

        try (InputStream in = new FileInputStream(fileProperties)) {
            prop.load(in);
            System.out.println("Loaded properties: " + prop);

            /* it will first try to find the file and load the 2 expected keys from it:
             */
            if (prop.isEmpty()) {
                System.out.println("Writing properties to: " + fileProperties);

                prop.setProperty("time", "" + date);
                System.out.println("Properties after date update: " + prop);

                /* if file/keys are missing, ask first the user for his name, and use that one (also saving it to the properties file)
                 */
                String name = readName(); //calling a method (to read data from console)
                prop.setProperty("username", name);
                System.out.println("It is an empty file -> written properties: " + prop);
            } else {
                if (!prop.getProperty("username").equals("")) {
                    /*
                    if keys are loaded, just use them
                    */
                    System.out.println("Properties before date update: " + prop);

                    /* update the time of the last visit on each run
                     */
                    prop.setProperty("time", "" + date);
                    System.out.println("Properties after date update: " + prop);
                } else if (prop.getProperty("username").equals("")) {
                    System.out.println("Writing properties to: " + fileProperties);
                    /* the program should ask the user for the name only once (when missing, and save it to prop file);
                     */
                    String name = readName(); //calling a method (to read data from console)
                    prop.setProperty("username", name);
                    System.out.println("Properties after name update: " + prop);

                    /* update the time of the last visit on each run
                     */
                    System.out.println("Properties before date update: " + prop);
                    prop.setProperty("time", "" + date);
                    System.out.println("Properties after date update: " + prop);
                }
            }
        }
        try (OutputStream out = new FileOutputStream(fileProperties)) {
            prop.store(out, "i updated the config file");
        }
        /* it should display a message like: "Hello, [user name], nice to see you again! (last visit time: [h:m:s])”
         */
        System.out.println("Hello " + prop.getProperty("username") + ",nice to see you again! (last visit time: " + prop.getProperty("time"));
    }

    private static String readName() {
        System.out.print("Enter your name: ");
        Scanner in = new Scanner(System.in); //need this first, to read from input
        return in.next();
    }
}
