package teme.w09.ex3_folder_contents;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/*
3. Folder contents
Given the path of a directory:
- list its direct descendants (files+dirs):
    - should display one line for each, with the name and size
    - list should be sorted descending by size
    - OPTIONAL: place the directories first and sorted ascending by name (and after them the files descending by size)
- display the total size of all files
 */
public class FolderContents {
    private final List<FileProperty> listFileProperty = new ArrayList<>();
    private final List<File> filesFromDir = new ArrayList<>();

    /*
    Read from user: the path of a base directory and a pattern to search for (a String value)
     */
    private static String readPath() {
        System.out.println("Enter the path of a base directory (example: d:\\Trainings\\Wantsome\\teme_mirabela_petrescu\\curs\\doc\\ )");
        Scanner in = new Scanner(System.in); //need this first, to read from input
        return in.next();
    }

    public static void main(String[] args) {

        String path = readPath();
        FolderContents files = new FolderContents();

        File dir = new File(path);
        if (dir.exists()) {
            System.out.println("\n >>>>>>>>>> list its direct descendants (files+dirs) >>>>>>>>>>>>");
            files.addFiles(dir);
            files.addFileProperty();

            System.out.println("\n >>>>>>>>>> display one line for each, with the name and size >>>>>>>>>>>>");
            System.out.println(files.listFileProperty.toString());

            System.out.println("\n >>>>>>>>>> list should be sorted descending by size >>>>>>>>>>>>");
            files.listDescendingBySize();

            System.out.println("\n >>>>>>>>>> place the directories first and sorted ascending by name (and after them the files descending by size) >>>>>>>>>>>>");
            files.listDirFirstAscendingByTypeByName();

            System.out.println("\n >>>>>>>>>> display the total size of all files >>>>>>>>>>>>\n" + files.totalSizeAllFiles());
        } else {
            System.out.println("\n!!!!!!! Not a valid path");
        }
    }

    /*
    add into a list all the files from the path given as input
     */
    private void addFiles(File dir) {
        File[] files = dir.listFiles();
        for (File fileItem : files != null ? files : new File[0]) {
            filesFromDir.add(fileItem);
            System.out.println(fileItem);
        }
    }

    /*
    add into the list the property of each file
     */
    private void addFileProperty() {
        for (File itemFileOrDir : filesFromDir) {
            if (itemFileOrDir.isDirectory()) {     // directory case
                File[] files = itemFileOrDir.listFiles();
                // calculate total length of each directory (sum of all files length)
                long dirLength = 0;
                for (File itemFile : files != null ? files : new File[0]) {
                    dirLength += itemFile.length();     // total length of each directory
                }
                listFileProperty.add(new FileProperty(itemFileOrDir.isDirectory(), itemFileOrDir.getName(), dirLength));
            }
            if (!itemFileOrDir.isDirectory()) {   // file case
                listFileProperty.add(new FileProperty(itemFileOrDir.isDirectory(), itemFileOrDir.getName(), itemFileOrDir.length()));
            }

        }
    }

    /*
    list should be sorted descending by size
     */
    private void listDescendingBySize() {
        listFileProperty.stream()
                .sorted(new FileComparatorBySize())
                .forEach(System.out::println);
    }

    /*
    place the directories first and sorted ascending by name (and after them the files descending by size)
     */
    private void listDirFirstAscendingByTypeByName() {
        listFileProperty.stream()
                .filter(FileProperty::isFileType)
                .sorted(new FileComparatorByName())
                .forEach(System.out::println);

        listFileProperty.stream()
                .filter(item -> !item.isFileType())
                .sorted(new FileComparatorBySize())
                .forEach(System.out::println);
    }

    /*
    display the total size of all files
     */
    private long totalSizeAllFiles() {
        return listFileProperty.stream()
                .map(FileProperty::getFileSize)
                .reduce(0L, (a, b) -> a + b);
    }
}
