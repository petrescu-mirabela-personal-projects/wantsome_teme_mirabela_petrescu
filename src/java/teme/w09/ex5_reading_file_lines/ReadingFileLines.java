package teme.w09.ex5_reading_file_lines;

/*
5. Reading file lines
Given a file path, read its contents (hint: may use Scanner with .nextLine()) and then:
    ● display first N lines (N read from console), each line being prefixed by its line count
    ● find and display the longest line (including its line number and its length)
    ● display these statistics:
        ○ number of lines, number of words (hint: for splitting a line to words may use
        String.split() method + some regular expression, see RegExExample.java..)
        ○ show the list of all words and the count usage for each (hint: store them in a
        Map<String, Integer>)
        ○ OPTIONAL: top 5 most used words (hint: use a TreeMap<String,Integer> to store
        each word+count, and create a custom comparator which sorts by count - and
        use an instance of this comparator when creating the TreeMap, so its entries will
        be automatically sorted by it..)
        ○ the longest word
    ● write these statistics to a new text file (named like initial file + '_statistics.txt' suffix)
 */

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;
import java.util.stream.Collectors;

public class ReadingFileLines {

    private static final String filePath = ReadingFileLines.readFilePath();
    private static final String fileInfoPath = generateFileInfoPath();
    private final Map<Integer, String> listLineCountString = new HashMap<>();   // Map<line number, line content as a String>
    private final Map<Integer, String[]> listLineCountArrayString = new HashMap<>();   // Map<line number, line content as array of Strings>

    public static void main(String[] args) {

        ReadingFileLines readingFileLines = new ReadingFileLines();
        try {
            readingFileLines.readAllLines();    // read all the lines and write into a map <number line, line content as String>

            try {
                System.out.println("\n >>>>>>>>>> Display first N lines (N read from console), each line being prefixed by its line count >>>>>>>>>>>>");
                readingFileLines.readNLinesStream(readMaxNumberOfLines());
                System.out.println("\n >>>>>>>>>> Find and display the longest line (including its line number and its length) >>>>>>>>>>>>");
                readingFileLines.longestLine();

                System.out.println("\n >>>>>>>>>> STATISTIC Number of lines >>>>>>>>>>>>");
                System.out.println(readingFileLines.statisticNumberOfLines());

                System.out.println("\n >>>>>>>>>> STATISTIC Number of words (all lines) >>>>>>>>>>>>");
                System.out.println(readingFileLines.statisticNumberOfWords());

                System.out.println("\n >>>>>>>>>> STATISTIC Show the list of all words and the count usage for each >>>>>>>>>>>>");
                System.out.println(readingFileLines.statisticListAllWordsCountUsage());

                System.out.println("\n >>>>>>>>>> STATISTIC Top 5 most used words >>>>>>>>>>>>");
                System.out.println(readingFileLines.statisticTop5MostUsedWords());

                System.out.println("\n >>>>>>>>>> STATISTIC The longest word >>>>>>>>>>>>");
                System.out.println("The longest word is " + readingFileLines.statisticTheLongestWord());

                System.out.println("\n >>>>>>>>>> Write these statistics to a new text file (named like initial file + '_statistics.txt' suffix) >>>>>>>>>>>>");
                try {
                    readingFileLines.writeStatisticsInfoTxt();
                } catch (IOException e) {
                    System.out.println("Not a valid info path file => " + fileInfoPath);
                }
            } catch (NumberFormatException e) {
                System.out.println("Not a valid line number");
            }
        } catch (IOException e) {
            System.out.println("No file introduced");
        }

    }

    /*
    display first N lines (N read from console), each line being prefixed by its line count
    */
    private static int readMaxNumberOfLines() throws NumberFormatException {
        System.out.println("Enter the max number of lines to read from the file: ");
        Scanner in = new Scanner(System.in); //need this first, to read from input
        return Integer.valueOf(in.next());
    }

    private static String generateFileInfoPath() {
        File f = new File(filePath);
        return "D:" + File.separator + "Trainings" + File.separator + "Wantsome" + File.separator + "teme_mirabela_petrescu" + File.separator + "src" + File.separator + "java" + File.separator + "teme" + File.separator + "w09" + File.separator + "ex5_reading_file_lines" + File.separator + f.getName().replace(".java", "_statistics.txt");
    }

    /*
    Read from user: the path of a base directory and a pattern to search for (a String value)
     */
    private static String readFilePath() {
        System.out.println("Enter the file path (example: D:\\Trainings\\Wantsome\\teme_mirabela_petrescu\\curs\\src\\java\\rezolvari_teme\\w06_generics\\ex2_media_library\\Library.java )");
        Scanner in = new Scanner(System.in); //need this first, to read from input
        return in.next();
    }

    private void readAllLines() throws IOException {
        File file = new File(filePath);
        Scanner fileScanner = new Scanner(file);
        int lineCount = 0;
        while (fileScanner.hasNext()) {
            String line = fileScanner.nextLine();
            if (!line.isEmpty()) {   // exclude the empty lines
                // Link https://docs.oracle.com/javase/7/docs/api/java/util/regex/Pattern.html
                // .replace("(?m)^\\s*\\r?\\n|\\r?\\n\\s*(?!.*\\r?\\n)", "")
                //String[] lineArray = line.replaceAll("[^A-Za-z0-9]", " ").split("\\s+");
                String[] lineArray = line.replaceAll("\\s+", ".").split("[^A-Za-z0-9]");
                lineCount++;
                listLineCountString.put(lineCount, line);
                listLineCountArrayString.put(lineCount, lineArray);
            }
        }
    }

    /*
    display first N lines (N read from console), each line being prefixed by its line count
     */
    public void readNLines(int maxLines) throws IOException {
        File file = new File(filePath);
        Scanner fileScanner = new Scanner(file);
        System.out.println("First " + maxLines + " lines of " + file.getAbsolutePath() + ":");
        int lineCount = 0;
        while (lineCount < maxLines && fileScanner.hasNext()) {
            String line = fileScanner.nextLine();
            lineCount++;
            System.out.println(lineCount + ": " + line);
        }
    }

    private void readNLinesStream(int maxLines) {
        if (maxLines > 0) {
            listLineCountString.entrySet()
                    .stream()
                    .limit(maxLines < listLineCountString.size() ? maxLines : listLineCountString.size())
                    .forEach(System.out::println);
        } else {
            System.out.println("Not valid line number, <= than 0");
        }
    }

    /*
    find and display the longest line (including its line number and its length)
     */
    private void longestLine() {
        Optional<Map.Entry<Integer, String>> longestLineValue = listLineCountString
                .entrySet()
                .stream()
                .max(Comparator.comparingInt(o -> o.getValue().length()));
        System.out.println(longestLineValue);
        if (longestLineValue.isPresent()) {
            System.out.println("Line length is: " + longestLineValue.get().getValue().length());
        } else {
            System.out.println("No word found");
        }
    }

    /*
    display these statistics:
        ○ number of lines, number of words (hint: for splitting a line to words may use
        String.split() method + some regular expression, see RegExExample.java..)
     */
    private int statisticNumberOfLines() {
        return listLineCountString.size();
    }

    private int statisticNumberOfWords() {
        int countAllWords = 0;
        for (String[] itemOneLine : listLineCountArrayString.values()) {
            countAllWords += itemOneLine.length;
        }
        return countAllWords;
    }

    /*
    show the list of all words and the count usage for each (hint: store them in a Map<String, Integer>)
     */
    private Map<String, Long> statisticListAllWordsCountUsage() {

        // store all the words from the file into a list => further to search the count usage of each word
        List<String> listAllWordsAllLines = new ArrayList<>(allStringFromFile());

        // all can be done in a SINGLE stream pipeline, but with a more complex collect,
        // using .groupingBy() with extra params!
        // see more info:  https://dzone.com/articles/the-ultimate-guide-to-the-java-stream-api-grouping
        return listAllWordsAllLines
                .stream()
                .collect(Collectors.groupingBy(s -> s, Collectors.counting()));
    }

    /*
    OPTIONAL: top 5 most used words (hint: use a TreeMap<String,Integer> to store
    each word+count, and create a custom comparator which sorts by count - and
    use an instance of this comparator when creating the TreeMap, so its entries will
    be automatically sorted by it..)
     */
    private String statisticTop5MostUsedWords() {
        Map<String, Long> listEachWordCount = new TreeMap<>(statisticListAllWordsCountUsage());
        return listEachWordCount.entrySet()
                .stream()
                .sorted((o1, o2) -> -o1.getValue().compareTo(o2.getValue()))
                .limit(6)
                .map(entry -> entry.getKey() + ":" + entry.getValue()) //Stream<String>
                .collect(Collectors.joining(", ")); //String
    }

    private List<String> allStringFromFile() {
        List<String> listAllWordsAllLines = new ArrayList<>();
        // store all the words from the file into a list
        Iterator<String[]> itrValues = listLineCountArrayString.values().iterator();
        while (itrValues.hasNext()) {
            String[] entryValue = itrValues.next();
            listAllWordsAllLines.addAll(Arrays.asList(entryValue));
        }
        System.out.println("All words from the file: " + listAllWordsAllLines);
        return listAllWordsAllLines;
    }

    /*
    the longest word
     */
    private Optional<String> statisticTheLongestWord() {
        // store all the words from the file into a list => further to search the longest word
        List<String> listAllWordsAllLines = new ArrayList<>(allStringFromFile());

        // all can be done in a SINGLE stream pipeline, but with a more complex collect,
        // using .groupingBy() with extra params!
        // see more info:  https://dzone.com/articles/the-ultimate-guide-to-the-java-stream-api-grouping
        return listAllWordsAllLines
                .stream()
                .max(Comparator.comparingInt(String::length));
    }

    /*
    write these statistics to a new text file (named like initial file + '_statistics.txt' suffix)
     */
    private void writeStatisticsInfoTxt() throws IOException {
        String infoToWrite = "\n >>>>>>>>>> STATISTIC Number of lines >>>>>>>>>>>>\n";
        infoToWrite += "" + statisticNumberOfLines() + "\n";

        infoToWrite += "\n >>>>>>>>>> STATISTIC Number of words (all lines) >>>>>>>>>>>>\n";
        infoToWrite += statisticNumberOfWords() + "\n";

        infoToWrite += "\n >>>>>>>>>> STATISTIC Show the list of all words and the count usage for each >>>>>>>>>>>>\n";
        String result1 = statisticListAllWordsCountUsage()
                .entrySet()
                .stream()
                .map(entry -> entry.getKey() + ":" + entry.getValue()) //Stream<String>
                .collect(Collectors.joining(", ")); //String
        infoToWrite += result1 + "\n";

        infoToWrite += "\n >>>>>>>>>> STATISTIC Top 5 most used words >>>>>>>>>>>>\n";
        infoToWrite += statisticTop5MostUsedWords() + "\n";

        infoToWrite += "\n >>>>>>>>>> STATISTIC The longest word >>>>>>>>>>>>\n";
        infoToWrite += statisticTheLongestWord() + "\n";
        writeToFileInfo(infoToWrite);
    }

    private void writeToFileInfo(String text) throws IOException {
        try (OutputStream out = new FileOutputStream(ReadingFileLines.fileInfoPath)) {
            char[] cArray = text.toCharArray();
            for (char textChar : cArray) {
                out.write(textChar);
            }
        }
    }
}
