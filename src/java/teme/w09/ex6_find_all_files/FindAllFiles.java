package teme.w09.ex6_find_all_files;

/*
6. Find all files
Given the path of a base directory, scan all the file tree under it (including all levels) and:
- Display the total number of files and directories under it
- Find and show the top 5 biggest files (display for each the path and size; list should be
sorted desc by size)
- Write the previous displayed info to a new text file (placed under base dir, named
'dir_info.txt')
Hint: this is a good place to use RECURSIVE methods.
In general, your recursive method should receive as input a directory path and return some
value (total files count, or biggest file, or a list of files..) computed for that whole tree; it will also
call itself as needed on the smaller trees under the current dir (with a stop condition when the
received tree has no more levels, etc)
See ListDirectoryTreeExample.java for something similar.
Also, to better understand recursion, read from: /doc/books/thinkjava.pdf, chapters 5.8, 5.9!
 */

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class FindAllFiles {

    private static final String path = FindAllFiles.readPath();
    private final List<File> filesFromPath = new ArrayList<>();
    private final List<FileProperty> listFileProperty = new ArrayList<>();

    public static void main(String[] args) {

        FindAllFiles files = new FindAllFiles();
        File dir = new File(path);
        if (dir.exists()) {     // the file or directory denoted by this abstract pathname exists
            System.out.println("\n >>>>>>>>>> display all files from the path " + path + " >>>>>>>>>>>>");
            files.recursiveSearch(dir);
            files.addFileProperty();

            System.out.println("\n >>>>>>>>>> display total number of files from the path " + path + " >>>>>>>>>>>>");
            files.totalNumberOfFiles();

            System.out.println("\n >>>>>>>>>> directories under the path " + path + " >>>>>>>>>>>>");
            files.directoriesUnderPath();

            System.out.println("\n >>>>>>>>>> The top 5 biggest files (display for each the path and size; " +
                    "list should be sorted desc by size) under the path " + path + " >>>>>>>>>>>>");
            System.out.println(files.top5BiggestFiles());

            try {
                String fileInfoPath = "d:" + File.separator + "Trainings" + File.separator + "Wantsome" + File.separator + "teme_mirabela_petrescu" + File.separator + "src" + File.separator + "java" + File.separator + "teme" + File.separator + "w09" + File.separator + "ex6_find_all_files" + File.separator + "dir_info.txt";
                System.out.println("\n >>>>>>>>>> The top 5 biggest files sorted desc by size) under the path " + path + " are written to file " + fileInfoPath + " >>>>>>>>>>>>");
                files.writeTop5BiggestFilesDirInfoTxt(fileInfoPath);
            } catch (IOException e1) {
                System.out.println("!!!!!! No dir_info.txt file");
            }
        } else {
            System.out.println("!!!!!! Not valid directory (path) name");
        }
    }

    /*
    Read from user: the path of a base directory and a pattern to search for (a String value)
     */
    private static String readPath() {
        System.out.println("Enter the path of a base directory (example: d:\\Trainings\\Wantsome\\teme_mirabela_petrescu\\curs\\doc\\ )");
        System.out.println("Enter the path of a base directory (example: d:\\Trainings\\Wantsome\\teme_mirabela_petrescu\\curs\\src\\java\\demo\\javafx\\ )");
        Scanner in = new Scanner(System.in); //need this first, to read from input
        return in.next();
    }

    /*
    search under that base dir (and all its subfolder) - all the files/directories are listed
     */
    private void recursiveSearch(File dir) {
        File[] filesOrDir = dir.listFiles();
        for (File itemFileOrDir : filesOrDir != null ? filesOrDir : new File[0]) {
            if (itemFileOrDir.isDirectory()) {
                    /*
                    add into a list all the directories from the path given as input
                     */
                filesFromPath.add(itemFileOrDir);
                System.out.println(itemFileOrDir);
                recursiveSearch(itemFileOrDir);
            }
            if (!itemFileOrDir.isDirectory()) {
                    /*
                    add into a list all the files from the path given as input
                     */
                filesFromPath.add(itemFileOrDir);
                System.out.println(itemFileOrDir);
            }
        }
    }

    /*
    add into the list the property of each file
    */
    private void addFileProperty() {
        for (File itemFileOrDir : filesFromPath) {
            if (itemFileOrDir.isDirectory()) {     // directory case
                File[] files = itemFileOrDir.listFiles();
                // calculate total length of each directory
                long dirLength = 0;
                for (File itemFile : files != null ? files : new File[0]) {
                    dirLength += itemFile.length();     // total length of each directory (length sum all of the files under the folder)
                }
                listFileProperty.add(new FileProperty(itemFileOrDir.getPath(), itemFileOrDir.isDirectory(), itemFileOrDir.getName(), dirLength));
            }
            if (!itemFileOrDir.isDirectory()) {   // file case
                listFileProperty.add(new FileProperty(itemFileOrDir.getPath(), itemFileOrDir.isDirectory(), itemFileOrDir.getName(), itemFileOrDir.length()));
            }

        }
    }

    /*
    Display the total number of files and directories under it
     */
    private void totalNumberOfFiles() {
        System.out.println("Total number of files: " + filesFromPath.stream()
                .filter(file -> !file.isDirectory())
                .count());

        System.out.println("Total number of directories: " + filesFromPath.stream()
                .filter(File::isDirectory)
                .count());
    }

    /*
    Display the total number of files and directories under it
    */
    private void directoriesUnderPath() {
        filesFromPath.stream()
                .filter(File::isDirectory)
                .forEach(System.out::println);
    }

    /*
    Find and show the top 5 biggest files (display for each the path and size; list should be sorted desc by size)
    */
    private List<FileProperty> top5BiggestFiles() {
        return listFileProperty.stream()
                .filter(file -> !file.isFileType())     // file type
                .sorted(new FileComparatorBySize())
                .limit(5)
                .collect(Collectors.toList());
    }

    /*
    Write the previous displayed info to a new text file (placed under base dir, named 'dir_info.txt')
     */
    private void writeTop5BiggestFilesDirInfoTxt(String fileInfoPath) throws IOException {
        try (OutputStream out = new FileOutputStream(fileInfoPath)) {
            List<FileProperty> top5FileSize = top5BiggestFiles();
            String text = top5FileSize.toString();
            char[] cArray = text.toCharArray();
            for (char textChar : cArray) {
                out.write(textChar);
            }
        }
    }
}
