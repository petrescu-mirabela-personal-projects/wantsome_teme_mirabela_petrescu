package teme.w02.ex4;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static teme.w02.ex4.Ex4_CheckArrayIsSorted.isSorted;

public class Ex4_CheckArrayIsSorted_Tests {
    @Test
    public void testCheckSorted() {
        assertTrue(isSorted());
        assertTrue(isSorted(1));
        assertTrue(isSorted(1, 2));
        assertTrue(isSorted(1, 4, 6));
        assertTrue(isSorted(1, 2, 2));
        assertFalse(isSorted(3, 2, 1));
        assertFalse(isSorted(1, 2, 4, 3));
    }
}
