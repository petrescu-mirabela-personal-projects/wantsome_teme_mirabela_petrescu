package teme.w02.ex2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static teme.w02.ex2.Ex2_ArrayToString.arrayToString;

public class Ex2_ArrayToString_Tests {
    @Test
    public void testArrayToString() {
        assertEquals("[]", arrayToString(new double[]{}));
        assertEquals("[0.00]", arrayToString(new double[]{0}));
        assertEquals("[1.00, 2.00]", arrayToString(new double[]{1, 2}));
        assertEquals("[1.10, 2.22, 3.33]", arrayToString(new double[]{1.1, 2.2222, 3.3251}));
    }
}
