package teme.w02.ex12;

import java.util.Arrays;

/**
 * Write a function that receives an array and returns a new array sorted by insertion sort.
 * More info: https://en.wikipedia.org/wiki/Insertion_sort
 */
public class Ex12_InsertionSort {

    /**
     * Receives an array and sorts it in place, using Insertion sort algorithm
     */
    static void sort(int[] array) {
        int i = 1, j;   // begin at the left-most element of the array
        int temp;   // temporary variable
        while (i < array.length) {
            temp = array[i];
            // moves array[i]in one go
            j = i - 1;
            while (j >= 0 && array[j] > temp) {
                // performs one assignment in the inner loop body
                // this inner loop shifts the elements to the right to clear a sport for temp = array[i]
                array[j + 1] = array[j];
                j--;
            }
            array[j + 1] = temp;
            i++;
        }
    }

    static void sortSwap(int[] array) {
        int i = 1, j;   // begin at the left-most element of the array
        int temp;   // temporary variable
        while (i < array.length) {
            // run over all the elements, except the first one (it is already sorted))
            j = i;
            while (j > 0 && array[j - 1] > array[j]) {
                // moves array[i] to its correct place
                // swap the values using temp, temporary value
                temp = array[j];
                array[j] = array[j - 1];
                array[j - 1] = temp;
                j--;
            }
            i++;
        }
    }


    //for manual tests
    public static void main(String[] args) {
        sortAndPrint(new int[]{});
        sortAndPrint(new int[]{1});
        sortAndPrint(new int[]{1, 2, 3});
        sortAndPrint(new int[]{1, 3, 2});
        sortAndPrint(new int[]{5, 2, 3, 1, 4});
        sortAndPrint(new int[]{5, 4, 3, 2, 1, 0});
    }

    //helper function for main
    private static void sortAndPrint(int[] arr) {
        sort(arr);
        System.out.println(Arrays.toString(arr));
        sortSwap(arr);
        System.out.println(Arrays.toString(arr));
    }
}
