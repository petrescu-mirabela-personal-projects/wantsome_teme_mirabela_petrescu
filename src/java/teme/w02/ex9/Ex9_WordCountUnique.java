package teme.w02.ex9;

/**
 * Given a phrase of words separated by space, count how many UNIQUE words it contains.
 * Example: "aa bb cc aa bb" => 3 unique words
 * Hint: you will need a way to compare each word with the list of unique words detected up until that moment,
 * in order to decide if to increase unique words counter for this word or not..
 */
public class Ex9_WordCountUnique {

    static int countUniqueWords(String phrase) {

        int countUniqueWord = 0;
        boolean isWordFound;

        /*
         * remove the spaces from the start and end of the String and then split by spaces
         * \\s - means A whitespace character: [ \t\n\x0B\f\r]
         * \\s+ - remove consecutive whitespaces
         */
        String[] phraseArray = phrase.trim().split("\\s+");
        String[] uniqueWordArray = new String[phraseArray.length];

        /*
         * by default the list of unique words has the first word of the phrase of words separated by space, given as input
         * copy in the array the first String from phrase
         */
        if (!phraseArray[0].isEmpty()) {
            uniqueWordArray[0] = phraseArray[0];
            countUniqueWord = 1;
        }
        for (int i = 1; i < phraseArray.length && !phraseArray[0].isEmpty(); i++) {
            isWordFound = false;
            for (int j = 1; j < uniqueWordArray.length; j++) {
                if (!isWordFound && !phraseArray[i].equalsIgnoreCase(uniqueWordArray[j - 1])) {
                    uniqueWordArray[j] = phraseArray[i];
                    countUniqueWord++;
                } else {
                    // word found, not unique
                    isWordFound = true;
                }
            }
        }
        return countUniqueWord;
    }

    public static void main(String[] args) {
        System.out.println(countUniqueWords(" aa  bb AA "));
        System.out.println(countUniqueWords(""));
        System.out.println(countUniqueWords("abc"));
        System.out.println(countUniqueWords("aa bb"));
        System.out.println(countUniqueWords(" aa  bb aa "));
        System.out.println(countUniqueWords(" aa  bb AA "));
    }
}
