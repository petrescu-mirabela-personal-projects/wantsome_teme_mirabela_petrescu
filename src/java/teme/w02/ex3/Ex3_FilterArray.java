package teme.w02.ex3;

import java.util.Arrays;

/**
 * Write a function that takes an array of numbers and returns a new array with only the odd numbers.
 * (hint: test it for a few cases, using Arrays.toString() to display the content of the returned array)
 */
public class Ex3_FilterArray {

    static int[] onlyOdd(int[] array) {

        int[] oddArray = new int[array.length];
        int j = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 1) {
                oddArray[j] = array[i];
                j++;
            }
        }
        return Arrays.copyOf(oddArray, j);
    }

    public static void main(String[] args) {
        System.out.println(
                Arrays.toString(
                        onlyOdd(new int[]{1, 2, 3, 4, 5})));
    }
}
