package teme.w02.ex7;

/**
 * Starting with the solution for Exercise #1, write a more general function that receives
 * a number and a base (between 2..36) and returns a string with the number’s representation in that base.
 * E.g:
 * decimalToBase(10, 2)  -> “1010”
 * decimalToBase(10, 10) -> “10”
 * decimalToBase(10, 8)  -> “12”
 * <p>
 * Note: To support bases higher than 10 you can use letters instead of digits, e.g. for base 16: A = 10, B = 11, ..., F = 15...
 * For example:
 * decimalToBase(47, 16) -> “2F”
 * decimalToBase(1295, 36) -> “ZZ”
 */
public class Ex7_DecimalToBase {

    static String decimalToBase(int number, int base) {

        int[] binaryArray = new int[1000];  // array to store binary number
        // counter for binary array
        int i = 0;
        String binaryString = number == 0 ? "0" : "";    // string to store binary number
        while (number > 0) {
            // storing remainder in binary array
            binaryArray[i++] = number % base;
            number = number / base;
        }
        // read from the last elemen until the first element
        for (int j = i - 1; j >= 0; j--) {
            binaryString += base < 10 ? binaryArray[j] : digitAsString(binaryArray[j]);
        }
        return binaryString;
    }

    /**
     * Helper function, converts a number which should representing a single digit in some base
     * to a single char String representation, like:
     * 1 -> '1'
     * 9 -> '9'
     * 10 -> 'A'
     * 15 -> 'F'
     * ...
     */
    static String digitAsString(int digit) {
        return digit < 10 ?
                String.valueOf(digit) :
                String.valueOf((char) ('A' + (digit - 10)));
    }

    //for manual testing
    public static void main(String[] args) {

        System.out.println(decimalToBase(9, 10));
        System.out.println(decimalToBase(10, 10));

        System.out.println(decimalToBase(0, 2));
        System.out.println(decimalToBase(1, 2));
        System.out.println(decimalToBase(2, 2));
        System.out.println(decimalToBase(10, 2));
        System.out.println(decimalToBase(127, 2));

        System.out.println(decimalToBase(0, 16));
        System.out.println(decimalToBase(15, 16));
        System.out.println(decimalToBase(16, 16));
        System.out.println(decimalToBase(127, 16));
        System.out.println(decimalToBase(255, 16));

        System.out.println(decimalToBase(35, 36));
        System.out.println(decimalToBase(1295, 36));
    }
}
