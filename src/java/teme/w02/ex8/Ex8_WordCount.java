package teme.w02.ex8;

/**
 * Given a phrase (as a String value), count how many words it contains. Words are considered to be separated only by space (one or more spaces)
 * Example: "Aa bb cc dd" => 4 words
 * Hint: you may use the split() method present on String to separate the words (search online info about using it)
 * (other useful String methods to check: trim, isEmpty)
 * Optional: read about regular expressions (in Java), and find a way to call String.split(..)
 * so that it will directly eliminate multiple spaces from words (so it will not generate parts
 * which are formed just of spaces), and see if you can get a one-line solution using this..
 */
public class Ex8_WordCount {

    static int countWords(String phrase) {

        String[] phraseSplitArray;
        int countWords = 0;
        // remove the spaces from the start and end of the String and then split by spaces
        phraseSplitArray = phrase.trim().split(" ");
        for (String item : phraseSplitArray) {
            // ignore the vector elements which are empty string
            if (!item.isEmpty()) {
                countWords++;
            }
        }
        return countWords;
    }

    public static void main(String[] args) {
        System.out.println(countWords(""));
        System.out.println(countWords("abc"));
        System.out.println(countWords("aa bb"));
        System.out.println(countWords(" aa  bb  "));
    }
}
