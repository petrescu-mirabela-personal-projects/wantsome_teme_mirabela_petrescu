package teme.w02.ex10;

import java.util.Arrays;

/**
 * Given a phrase of words separated by space, write a method which returns the list of unique words
 * (case-insensitive) and for each one number of times it appears.
 * The returned value should be a string, with this format: “<word1>=<count1> <word2>=<count2> ...”
 * Example: "Aa bb cc aa dd BB dd" => "aa=3 bb=2 cc=1 dd=2”
 * <p>
 * Hint: you'll need to keep somehow a list of pairs of 2 separate values {word + counter};
 * for this, you may consider using 2 arrays updated/iterated in parallel - 1st one for the words,
 * and 2nd one for the counters (so a word is stored at some index i in 1st array,
 * and its counter is stored at same index i but in 2nd array)
 */
public class Ex10_WordsWithCount {

    static String countWords(String phrase) {

        /*
         * remove the spaces from the start and end of the String and then split by spaces
         * \\s - means A whitespace character: [ \t\n\x0B\f\r]
         * \\s+ - remove consecutive whitespaces
         */
        String[] phraseArray = phrase.trim().split("\\s+");

        int[] uniqueCountArray = new int[phraseArray.length];
        Arrays.fill(uniqueCountArray, 0);    // fill the frequency array with 0


        String uniqueStringCount = "";

        int countUniqueWord;    //Variable for getting Repeated word count

        for (int i = 0; !phraseArray[0].isEmpty() && i < phraseArray.length; i++)        //Outer loop for Comparison
        {
            countUniqueWord = 1;
            for (int j = i + 1; j < phraseArray.length; j++)    //Inner loop for Comparison
            {

                if (phraseArray[i].equalsIgnoreCase(phraseArray[j]))    //Checking for both strings are equal
                {
                    countUniqueWord++;                //if equal increment the count
                    phraseArray[j] = "0";            //Replace repeated words by zero
                }
            }
            if (!phraseArray[i].equals("0")) {
                uniqueStringCount += phraseArray[i] + "=" + countUniqueWord + " ";  // Adding the word along with count
            }
        }

        return uniqueStringCount.trim();
    }

    public static void main(String[] args) {
        System.out.println(countWords(""));
        System.out.println(countWords("   "));
        System.out.println(countWords("aa bb aa"));
        System.out.println(countWords("  Aa  bb  aa cc BB  "));
    }
}
