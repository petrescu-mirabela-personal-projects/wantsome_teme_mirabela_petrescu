package teme.w02.ex14;

/**
 * Write a function that sorts an array of numbers using the merge sort algorithm.
 * Merge Sort is a kind of Divide and Conquer algorithm in computer programming.
 */

// Link: https://www.programiz.com/dsa/merge-sort

public class Ex14_MergeSort {

    /*
     * The MergeSort function repeatedly divides the array into two halves until we reach a stage where we try
     * to perform MergeSort on a subarray of size 1 i.e. p == r.
     */
    static void mergeSort(int[] arr, int p, int r) {
        if (p < r) {
            int q = (p + r) / 2;
            mergeSort(arr, p, q);
            mergeSort(arr, q + 1, r);
            merge(arr, p, q, r);

        }
    }

    static void merge(int A[], int p, int q, int r) {
        // L is the copy of A[p..q]
        // M is the copy of A[q+1..r]
        int n1 = q - p + 1;
        int n2 = r - q;
        int i, j, k;

        int[] L = new int[n1];
        int[] M = new int[n2];

        // Step1: Create duplicate copies of sub-arrays to be sorted
        for (i = 0; i < n1; i++)
            L[i] = A[p + i];
        for (j = 0; j < n2; j++)
            M[j] = A[q + 1 + j];

        // Step2: Maintain current index of sub-arrays and main array
        i = 0;
        j = 0;
        k = p;

        // Step 3: Until we reach end of either L or M, pick larger among elements L and M and place them in the correct position at A[p..r]
        while (i < n1 && j < n2) {
            if (L[i] <= M[j]) {
                A[k++] = L[i++];
            } else {
                A[k++] = M[j++];
            }
        }

        // Step 4: When we run out of elements in either L or M, pick up the remaining elements and put in A[p..r]
        while (i < n1) {
            A[k++] = L[i++];
        }
        while (j < n2) {
            A[k++] = M[j++];
        }
    }

    //for manual tests
    public static void main(String[] args) {
        int arr1[] = {};
        mergeSort(arr1, 0, arr1.length - 1);
        printArray(arr1);

        int arr2[] = {3, 6, 1, 5, 12, 0, 23, 4, 55};
        mergeSort(arr2, 0, arr2.length - 1);
        printArray(arr2);

    }

    static void printArray(int arr[]) {
        int n = arr.length;
        for (int i = 0; i < n; ++i)
            System.out.print(arr[i] + " ");
        System.out.println();
    }

}
