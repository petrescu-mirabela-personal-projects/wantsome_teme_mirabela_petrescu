package teme.w06.ex0;

import java.util.Iterator;

class SquaresIterable implements Iterable<Long> {

    private final long maxValue;

    SquaresIterable(long maxValue) {
        this.maxValue = maxValue;
    }

    @Override
    public Iterator<Long> iterator() {
        return new SquaresIterator(maxValue);
    }
}

class SquaresIterator implements Iterator<Long> {

    private final long maxValue;
    private long number = 0;

    SquaresIterator(long maxValue) {
        this.maxValue = maxValue;
    }

    @Override
    public boolean hasNext() {
        long nextValue = (number + 1) * (number + 1);
        return nextValue <= maxValue;
    }

    @Override
    public Long next() {
        number++;
        return number * number;
    }
}
