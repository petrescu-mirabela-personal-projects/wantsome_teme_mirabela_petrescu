package teme.w06.ex0;

/*
c) Comparing Persons :
iii) Implement a separate comparator which will order persons by cnp
(ascending), and test it. Try to test it by adding same persons to a new sorted
set, but which uses by default this specific comparator to keep the set sorted,
and the just iterate over that set and print them.
 */

import java.util.Comparator;

class PersonComparatorByCnp implements Comparator<Person> {

    @Override
    public int compare(Person o1, Person o2) {
        return Integer.compare(o1.getCnp(), o2.getCnp());  // ascending order by cnp
    }
}
