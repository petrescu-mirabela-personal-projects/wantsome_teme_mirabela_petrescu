package teme.w06.ex2;

import java.util.Objects;

public class Mp3 extends MediaEntity {
    private final String singer;
    private final String album;

    public Mp3(MediaEntityType type, String title, int noOfDownloads, String singer, String album) {
        super(type, title, noOfDownloads);
        this.singer = singer;
        this.album = album;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Mp3)) return false;
        Mp3 mp3 = (Mp3) o;
        return Objects.equals(singer, mp3.singer) &&
                Objects.equals(album, mp3.album);
    }

    @Override
    public String toString() {
        return super.toString() +
                "singer='" + singer + '\'' +
                ", album='" + album + '\'' +
                '}';
    }
}
