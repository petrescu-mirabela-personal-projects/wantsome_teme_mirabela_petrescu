package teme.w06.ex2;

public class Video extends MediaEntity {
    private final int durationMin;
    private final boolean fullHD;

    public Video(MediaEntityType type, String title, int noOfDownloads, int duration, boolean fullHD) {
        super(type, title, noOfDownloads);
        this.durationMin = duration;
        this.fullHD = fullHD;
    }

    @Override
    public String toString() {
        return super.toString() +
                "duration=" + durationMin +
                ", fullHD=" + fullHD +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Video)) return false;
        Video video = (Video) o;
        return durationMin == video.durationMin &&
                fullHD == video.fullHD;
    }
}
