package teme.w06.ex2;

import java.util.Comparator;

class LibraryComparatorByNoOfDownloads implements Comparator<MediaEntity> {

    public LibraryComparatorByNoOfDownloads() {
    }

    @Override
    public int compare(MediaEntity o1, MediaEntity o2) {
        int noOfDownloadsResult = -(o1.getNoOfDownloads()).compareTo(o2.getNoOfDownloads()); // descending order by number of downloads
        if (noOfDownloadsResult == 0) {    // if the noOfDownloads is the same
            // order the title
            return o1.getTitle().compareTo(o2.getTitle());  // ascending order by the title name
        }
        return noOfDownloadsResult;
    }
}
