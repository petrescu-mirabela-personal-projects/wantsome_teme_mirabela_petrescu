package teme.w06.ex2;

import java.util.Objects;

public class Book extends MediaEntity {
    private final String author;
    private final String publisher;

    public Book(MediaEntityType type, String title, int noOfDownloads, String author, String publisher) {
        super(type, title, noOfDownloads);
        this.author = author;
        this.publisher = publisher;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Book)) return false;
        Book book = (Book) o;
        return Objects.equals(author, book.author) &&
                Objects.equals(publisher, book.publisher);
    }

    @Override
    public String toString() {
        return super.toString() +
                "author='" + author + '\'' +
                ", publisher='" + publisher + '\'' +
                '}';
    }
}
