package teme.w06.ex2;

import java.util.Objects;

enum MediaEntityType {
    BOOK,
    MP3,
    VIDEO
}

class MediaEntity {
    private MediaEntityType type;    // MediaEntityType
    private String title;            // String
    private Integer noOfDownloads;   // Integer

    public MediaEntity() {
    }

    MediaEntity(MediaEntityType type, String title, Integer noOfDownloads) {
        this.type = type;
        this.title = title;
        this.noOfDownloads = noOfDownloads;
    }

    public MediaEntityType getType() {
        return type;
    }

    public String getTitle() {
        return title;
    }

    public Integer getNoOfDownloads() {
        return noOfDownloads;
    }

    public void setNoOfDownloads(Integer noOfDownloads) {
        this.noOfDownloads = noOfDownloads;
    }

    @Override
    public String toString() {
        return "MediaEntity{" +
                "type=" + type +
                ", title='" + title + '\'' +
                ", noOfDownloads=" + noOfDownloads +
                ", ";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MediaEntity)) return false;
        MediaEntity that = (MediaEntity) o;
        return Objects.equals(noOfDownloads, that.noOfDownloads) &&
                type == that.type &&
                Objects.equals(title, that.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, title, noOfDownloads);
    }
}
