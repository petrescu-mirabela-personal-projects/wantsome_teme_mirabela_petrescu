package teme.exercitii_extra.ex12_fractional_numbers;

import java.util.Objects;

/*
12) Fractional numbers
Write a class Fractional to represent fractional numbers, like: 1/2, 1/3, -5/4. In
general, a fractional number has 2 parts of type integer: a numerator and a
denominator.
- add the required fields, and some getters (no setters, fields should be
immutable)
- add the needed constructors, with the needed validations (pay attention to
x/0 case!)
- Add a static factory method named f() which receives 2 values
(numerator+denominator) and builds and returns a new instance of
Fractional (will be easier to call in expressions than using ‘new
Fractional()’ each time)
- override toString - should show values like: “(1/2)" , “(5/4)”
- override equals(), so it will consider the numerical value of the fraction; for
example: 1/3 == 2/6, 6/2==3,...
(hint: simply trying to translate the values to an equivalent value of type
float/double may not work, due to limits of binary representation: 1/3 can
never be stored exactly; so you may have to find another way to check
that 2 fractions are equal…)
- Add methods for the arithmetic operators: add, multiply; the methods
should accept 2 parameters of type Fractional, compute the result of
operation and return it as a new Fractional value
- Optional: overload the add,multiply methods with versions which
work also with int (accepting all 4 combinations of int and Fractional
types for the 2 input params)
- Use this class to compute the value of expressions like this:
f(1,3) .multiply( f(6,1) ) .equals( f(2,1)) => should return true
Optional:
- Add a method to get the normalized value, like for example: 2/4 =>
normalized= 1/2; 6/3 => normalized= 2 (or 2/1); hint: you will need to
implement an algorithm for GCD (greatest common denominator)
https://en.wikipedia.org/wiki/Euclidean_algorithm#Implementations
- Modify your constructor so it will call the normalization method first, so for
fractions like 3/6 will transform them to 1/2 from the start
Exemple:
x: (1/2)
y: (2/3)
(1/2)==(2/3) ? : false
(2/3)==(4/6) ? : true
(1/2) + (2/3) = (7/6)
(1/2) * (2/3) = (2/6)
(1/3)*(6/1) == (2/1) ? : true
 */
public class Fractional {

    private int numerator;
    private int denominator;

    /*
    - add the needed constructors, with the needed validations (pay attention to
    x/0 case!)
     */
    public Fractional(int numerator, int denominator) {
        this.numerator = numerator / gcd(numerator, denominator);
        if (denominator > 0) {
            this.denominator = denominator / gcd(numerator, denominator);
        }
    }

    public Fractional() {
        this(0, 1);
    }

    public static void main(String[] args) {


        System.out.println("(1/2) => " + new Fractional(1, 2));
        System.out.println("(2/3) => " + new Fractional(2, 3));
        System.out.println("(6/3) => " + new Fractional(6, 3));

        // shall return false
        System.out.println("(1/2) == (2/3) => " + new Fractional(1, 2)
                .equals(new Fractional(2, 3)));

        // shall return true
        System.out.println("(2/3) == (4/6) => " + new Fractional(2, 3)
                .equals(new Fractional(4, 6)));


        System.out.println("(1/2) + (2/3) = " + new Fractional(1, 2)
                .add(new Fractional(2, 3)));

        System.out.println("(1/2) * (2/3) = " + new Fractional(1, 2)
                .multiply(new Fractional(2, 3)));

        System.out.println("(1/3) * (6/1) = " + new Fractional(1, 3)
                .multiply(new Fractional(6, 1)));

        // shall return true
        System.out.println("(1/3) * (6/1) == (2/1) => " + new Fractional(1, 3)
                .multiply(new Fractional(6, 1))
                .equals(new Fractional(2, 1)));

    }

    public int getNumerator() {
        return numerator;
    }

    public int getDenominator() {
        return denominator;
    }

    /*
    - override equals(), so it will consider the numerical value of the fraction; for
    example: 1/3 == 2/6, 6/2==3,...
     */

    /*
    - override toString - should show values like: “(1/2)" , “(5/4)”
     */
    @Override
    public String toString() {
        return numerator +
                "/" + denominator;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Fractional)) return false;
        Fractional that = (Fractional) o;
        return numerator == that.numerator &&
                denominator == that.denominator;
    }

    @Override
    public int hashCode() {
        return Objects.hash(numerator, denominator);
    }

    /*
            - Add a static factory method named f() which receives 2 values
            (numerator+denominator) and builds and returns a new instance of
            Fractional (will be easier to call in expressions than using ‘new
            Fractional()’ each time)
             */
    public Fractional f(int numerator, int denominator) {

        return new Fractional(numerator, denominator);
    }

    /*
    - Add methods for the arithmetic operators: add, multiply; the methods
    should accept 2 parameters of type Fractional, compute the result of
    operation and return it as a new Fractional value
    - Optional: overload the add,multiply methods with versions which
    work also with int (accepting all 4 combinations of int and Fractional
    types for the 2 input params)
     */
    public Fractional add(Fractional n1) {

        return f((numerator * n1.denominator + n1.numerator * denominator), (denominator * n1.denominator));
    }

    public Fractional multiply(Fractional n1) {

        return f(numerator * n1.numerator, denominator * n1.denominator);
    }

    /*
    Add a method to get the normalized value, like for example: 2/4 =>
    normalized= 1/2; 6/3 => normalized= 2 (or 2/1); hint: you will need to
    implement an algorithm for GCD (greatest common denominator)
    https://en.wikipedia.org/wiki/Euclidean_algorithm#Implementations
     */
    public int gcd(int numerator, int denominator) {
        if (denominator == 0) {
            return numerator;
        }
        return gcd(denominator, numerator % denominator);
    }
}
