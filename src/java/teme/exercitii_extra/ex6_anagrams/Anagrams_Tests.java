package teme.exercitii_extra.ex6_anagrams;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class Anagrams_Tests {

    @Test
    public void testAnagrams() {

        Anagrams anagramsI = new Anagrams();
        assertTrue(anagramsI.anagramsCheck("binary", "brainy"));
        assertTrue(anagramsI.anagramsCheck("Listen", "Silent"));
        assertTrue(anagramsI.anagramsCheck("anagram", "nagaram"));
        assertFalse(anagramsI.anagramsCheck("anagram", "angrm"));

        assertTrue(anagramsI.anagramsCheckStream("binary", "brainy"));
        assertTrue(anagramsI.anagramsCheckStream("Listen", "Silent"));
        assertTrue(anagramsI.anagramsCheckStream("anagram", "nagaram"));
        assertFalse(anagramsI.anagramsCheckStream("anagram", "angrm"));

    }
}