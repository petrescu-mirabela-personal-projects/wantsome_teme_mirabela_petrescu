package teme.exercitii_extra.ex6_anagrams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/*
6) Anagrams
Check if 2 strings are anagrams (have exactly same letters, including duplicates,
but in a different order, and upper/lower case doesn’t matter)
Example:
'binary' and 'brainy' are anagrams? : true
'Listen' and 'Silent' are anagrams? : true
'anagram' and 'nagaram' are anagrams? : true
'anagram' and 'angrm' are anagrams? : false
 */
public class Anagrams {

    private static <T> List<T> difference(List<T> set1, List<T> set2) {
        List<T> result = new ArrayList<>();
        for (T itemSet1 : set1) {
            if (!set2.contains(itemSet1)) {
                result.add(itemSet1);
            }
        }
        return result;
    }

    private static <T> List<T> differenceStream(List<T> set1, List<T> set2) {
        List<T> set3 = set1.stream()
                .filter(s -> !set2.contains(s))
                .collect(Collectors.toList());
        return set3;
    }

    public static void main(String[] args) {

        Anagrams anagramsI = new Anagrams();
        System.out.println("binary and brainy are anagram: " + anagramsI.anagramsCheck("binary", "brainy"));
        System.out.println("binary and brainy are anagram: " + anagramsI.anagramsCheckStream("binary", "brainy"));

        System.out.println();
        System.out.println("Listen and brainy are Silent: " + anagramsI.anagramsCheck("Listen", "Silent"));
        System.out.println("Listen and brainy are Silent: " + anagramsI.anagramsCheckStream("Listen", "Silent"));

        System.out.println();
        System.out.println("anagram and brainy are nagaram: " + anagramsI.anagramsCheck("anagram", "nagaram"));
        System.out.println("anagram and brainy are nagaram: " + anagramsI.anagramsCheckStream("anagram", "nagaram"));

        System.out.println();
        System.out.println("anagram and angrm are anagram: " + anagramsI.anagramsCheck("anagram", "angrm"));
        System.out.println("anagram and angrm are anagram: " + anagramsI.anagramsCheckStream("anagram", "angrm"));
    }

    public boolean anagramsCheck(String s1, String s2) {
        List<String> setString1 = new ArrayList<>(Arrays.asList(s1.toLowerCase().replaceAll(" ", "").split("")));
        List<String> setString2 = new ArrayList<>(Arrays.asList(s2.toLowerCase().replaceAll(" ", "").split("")));

        // if the size is the same AND
        // if there are the same letters (duplicates are not considered)
        return setString1.size() == setString2.size() && difference(setString1, setString2).isEmpty();
    }

    public boolean anagramsCheckStream(String s1, String s2) {
        List<String> setString1 = new ArrayList<>(Arrays.asList(s1.toLowerCase().replaceAll(" ", "").split("")));
        List<String> setString2 = new ArrayList<>(Arrays.asList(s2.toLowerCase().replaceAll(" ", "").split("")));

        // if the size is the same AND
        // if there are the same letters (duplicates are not considered)
        return setString1.size() == setString2.size() && differenceStream(setString1, setString2).isEmpty();
    }
}
