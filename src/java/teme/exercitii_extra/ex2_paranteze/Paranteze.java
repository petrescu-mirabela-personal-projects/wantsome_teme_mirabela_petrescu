package teme.exercitii_extra.ex2_paranteze;

/*
2) Paranteze
Scrieti o methoda care primeste un text (ca o valoare tip String) si verifica daca
parantezele cuprinse in ea sunt corect si complet deschise si inchise.
(adica: toate parantezele deschise trebuie sa fie si inchise pana la final; si o
paranteza inchisa poate fi folosita doar daca mai avem una deschisa fara
pereche inaintea ei..)
Exemple:
"(2*(3+5)-a)" => true
"((2*a+b)" => false
")(ceva" => false
"altceva bla" => true
 */

public class Paranteze {

    private static boolean parenthesisStream(String text) {
        return text
                .chars()
                .mapToDouble(i -> i == '(' ? 1 : i == ')' ? -1 : 0)
                .reduce(0, (a, b) -> a == 0 && b == -1 ? Double.NaN : a + b) == 0;
    }

    public static void main(String[] args) {

        Paranteze paranteze = new Paranteze();
        System.out.println(paranteze.parenthesis("(2*(3+5)-a)"));
        System.out.println(paranteze.parenthesisStream("(2*(3+5)-a)"));

        System.out.println();
        System.out.println(paranteze.parenthesis("((2*a+b)"));
        System.out.println(paranteze.parenthesisStream("((2*a+b)"));

        System.out.println();
        System.out.println(paranteze.parenthesis(")(ceva"));
        System.out.println(paranteze.parenthesisStream(")(ceva"));

        System.out.println();
        System.out.println(paranteze.parenthesis("altceva bla"));
        System.out.println(paranteze.parenthesisStream("altceva bla"));
    }

    public boolean parenthesis(String text) {
        String[] stringText = text.replaceAll(" ", "").split("");
        int countAccolade = 0;
        for (String item : stringText) {
            if (item.equals("(")) {
                countAccolade++;
            } else if (item.equals(")") && countAccolade > 0) {   // the text shall have the ( before (
                countAccolade--;
            }
        }
        return (countAccolade == 0);    // the number of ( equals to the number of )
    }
}
