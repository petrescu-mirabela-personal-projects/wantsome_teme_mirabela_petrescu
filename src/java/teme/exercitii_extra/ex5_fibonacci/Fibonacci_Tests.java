package teme.exercitii_extra.ex5_fibonacci;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Fibonacci_Tests {

    @Test
    public void testFibonacci() {

        Fibonacci fibonacciI = new Fibonacci();

        assertEquals(1, fibonacciI.fibonacci(1));
        assertEquals(89, fibonacciI.fibonacci(10));
        assertEquals(10946, fibonacciI.fibonacci(20));

        assertEquals("1 1 2 3 5 8 13 21 34 55 89 144 233 377 610 987 1597 2584 4181 6765 10946 17711 28657 46368 75025 121393 196418 317811 514229 832040".trim(), fibonacciI.fibonacciPrint(30));
    }
}