package teme.exercitii_extra.ex5_fibonacci;

/*
5) Fibonacci
Write a method which computes the numbers of Fibonacci's sequence
This sequence is defined recursively in mathematics, like this:
F(0) = 1, F(1) = 1,
F(N) = F(N-1) + F(N-2)
(more info: https://en.wikipedia.org/wiki/Fibonacci_number )

Example:
fibonacci(1) = 1
fibonacci(10) = 89
fibonacci(20) = 10946
first 30 Fibonacci numbers: 1 1 2 3 5 8 13 21 34 55 89 144 233 377 610
987 1597 2584 4181 6765 10946 17711 28657 46368 75025 121393
196418 317811 514229 832040
 */
public class Fibonacci {

    public static void main(String[] args) {

        Fibonacci fibonacciI = new Fibonacci();

        System.out.print(fibonacciI.fibonacci(1));     // shall print 1
        System.out.println();
        System.out.print(fibonacciI.fibonacci(10));    // shall print 89
        System.out.println();
        System.out.print(fibonacciI.fibonacci(20));    // shall print 10946
        System.out.println();
        System.out.println(fibonacciI.fibonacci(30));    // shall print
        System.out.print(fibonacciI.fibonacciPrint(30));    // shall print
    }

    /*
    a) Write a method fibonacci which receives as input param a number N and
    returns the value of fibonacci(N). Test it for N=1, 10, 20
     */
    public int fibonacci(int n) {
        int result = 0;
        if (n == 0 || n == 1) {
            result = 1;
        }
        if (n > 1) {
            result = fibonacci(n - 1) + fibonacci(n - 2);
        }
        return result;
    }

    /*
    b) Use it to print the first 30 values of fibonacci sequence.
    Hint:
    - Try to write your method recursively (is actually shorter/easier than
    iterative for this case, you just directly translate the math definition)
    - For the recursive method: think about how optimal it is - for example,
    when computing F(5), how many times it recomputes the value of F(3)?
    - Optional: can you write a more optimal version, in which for F(N) it
    computes all values for F(N-1)..F(2) only once?
     */
    public String fibonacciPrint(int n) {
        StringBuilder printFibonacci = new StringBuilder();
        //int fibonacciItem = 0;
        int i = 0;
        while (i < n) {
            //fibonacciItem += fibonacci(i);
            printFibonacci.append(" ").append(fibonacci(i));
            i++;
        }
        return printFibonacci.toString().trim();
    }
}
