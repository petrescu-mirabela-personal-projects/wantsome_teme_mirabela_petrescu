package teme.exercitii_extra.ex8_distinct_list_values;

import java.util.*;
import java.util.stream.Collectors;

/*
8) Distinct list values
Write a method named distinct () which:
- receives as input parameter a List<String> with some values
- returns a List<String> in which all duplicate values are removed (the
remaining unique items should still be in same order as in the initial list).
Optional : think about at least 2 different ways to solve it. Which one is more
efficient? (regarding execution time, memory)
 */
public class DistinctListValues {

    public static void main(String[] args) {

        long startTime1 = System.nanoTime();     // nano instead milliseconds to be more precise
        long beforeUsedMem1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        DistinctListValues distinctListValuesI1 = new DistinctListValues();
        System.out.println(distinctListValuesI1.distinct1(newSet("t", "e", "t", "d", "c", "a", "b", "a", "c", "b")));
        long afterUsedMem1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        long stopTime1 = System.nanoTime();
        long actualTime1 = stopTime1 - startTime1;
        long actualMemUsed1 = afterUsedMem1 - beforeUsedMem1;
        System.out.println("Execution time: " + actualTime1);
        System.out.println("Memory used: " + actualMemUsed1);

        long startTime2 = System.nanoTime();     // nano instead milliseconds to be more precise
        long beforeUsedMem2 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        DistinctListValues distinctListValuesI2 = new DistinctListValues();
        System.out.println(distinctListValuesI2.distinct2(newSet("t", "e", "t", "d", "c", "a", "b", "a", "c", "b")));
        long afterUsedMem2 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        long stopTime2 = System.nanoTime();
        long actualTime2 = stopTime2 - startTime2;
        long actualMemUsed2 = afterUsedMem2 - beforeUsedMem2;
        System.out.println("Execution time: " + actualTime2);
        System.out.println("Memory used: " + actualMemUsed2);

        long startTime3 = System.nanoTime();     // nano instead milliseconds to be more precise
        long beforeUsedMem3 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        DistinctListValues distinctListValuesI3 = new DistinctListValues();
        System.out.println(distinctListValuesI3.distinctStream(newSet("t", "e", "t", "d", "c", "a", "b", "a", "c", "b")));
        long afterUsedMem3 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        long stopTime3 = System.nanoTime();
        long actualTime3 = stopTime3 - startTime3;
        long actualMemUsed3 = afterUsedMem3 - beforeUsedMem3;
        System.out.println("Execution time: " + actualTime3);
        System.out.println("Memory used: " + actualMemUsed3);
    }

    //helper method for easy building a new Array with some given elements (of a custom type)
    @SafeVarargs
    private static <E> ArrayList<E> newSet(E... elements) {
        return new ArrayList<>(Arrays.asList(elements));
    }

    // less efficient regarding execution time, same memory usage
    private List<String> distinct1(List<String> valuesList) {
        List<String> distinctList = new ArrayList<>();
        Iterator<String> itr = valuesList.iterator();
        while (itr.hasNext()) {
            String value = itr.next();
            itr.remove();
            if (!distinctList.contains(value)) {
                distinctList.add(value);
            }
        }
        return distinctList;
    }

    // more efficient regarding execution time, same memory usage
    private List<String> distinct2(List<String> valuesList) {
        return new ArrayList<>(new LinkedHashSet<>(valuesList));
    }

    // stream implementation
    // higher execution time
    private List<String> distinctStream(List<String> valuesList) {
        return valuesList.stream()
                .distinct()
                .collect(Collectors.toList());
    }
}
