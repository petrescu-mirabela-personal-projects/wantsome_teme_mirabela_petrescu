package teme.exercitii_extra.ex1_fizz_buzz;

/*
1) FizzBuzz
We are given an array of some random integer numbers. We say that a number
is:
- 'fizz' if it can be divided by 3
- 'buzz' if it can be divided by 5
- 'fizzbuzz' if it can be divided by both 3 and 5.
Write a method that goes through the array and prints for each number: either
fizz, buzz or fizzbuz (if number is of that type), or the number itself otherwise.
Hint: after first solving it to work correctly, try then to optimize your code by
avoiding any repetitions and unnecessary code (you should arrive to a quite
short/simple solution)
Example:
For array: { 1, 2, 3, 7, 8, 5, 30, 4, 10}
It should print: 1 2 fizz 7 8 buzz fizbuzz 4 buzz
 */
public class FizzBuzz {

    // optimised code
    private static void printOptimised(int[] arr) {
        for (int item : arr) {
            if ((item % 15) == 0)
                System.out.print("fizzbuzz ");
            else if ((item % 5) == 0)
                System.out.print("buzz ");
            else if ((item % 3) == 0)
                System.out.print("fizz ");
            else
                System.out.print(item + " ");
        }
    }

    // not optimised code
    private static void printNotOptimised(int[] arr) {
        for (int item : arr) {
            if (item % 3 == 0 && item % 5 == 0) {
                System.out.print("fizzbuzz ");
            } else if (item % 3 == 0) {
                System.out.print("fizz ");
            } else if (item % 5 == 0) {
                System.out.print("buzz ");
            } else {
                System.out.print(item + " ");
            }
        }
    }

    public static void main(String[] args) {

        printNotOptimised(new int[]{1, 2, 3, 7, 8, 5, 30, 4, 10});
        System.out.println("\n");
        printOptimised(new int[]{1, 2, 3, 7, 8, 5, 30, 4, 10});
    }

}
