package teme.exercitii_extra.ex10_parcurgere_matrice;

/*
10) Parcurgere matrice
Scrieti o metoda care primeste un matrice de dimensiune NxN (un array
bidimensional de valori int) umpluta cu diferite valori, si tipareste pe cate un rand:
- Lista elementele de pe diagonala principala
- Lista elementelor de pe diagonala secundara
- Suma totala a elementelor din matrice
- Suma elementelor de pe marginea matricii (tot perimetrul)

Exemple:
Matrice:
1 2 3 4
5 6 7 8
9 10 11 12
13 14 15 16
diagonala 1: 1 6 11 16
diagonala 2: 4 7 10 13
suma toate: 136
suma perimetru: 102
 */
public class ParcurgereMatrice {

    static void parcurgeteMatrice(int mat[][]) {

        int sumaToate = 0;

        System.out.print("diagonala 1: ");
        for (int i = 0; i < mat.length; i++) {
            System.out.print(mat[i][i] + " ");
        }

        System.out.println();

        System.out.print("diagonala 2: ");
        for (int i = 0; i < mat.length; i++) {
            System.out.print(mat[i][mat.length - 1 - i] + " ");
        }

        System.out.println();
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat.length; j++) {
                sumaToate += mat[i][j];
            }
        }
        System.out.println("suma toate: " + sumaToate);

        int sumaPerimetru = sumaToate;
        for (int i = 1; i <= 2; i++) {
            for (int j = 1; j <= 2; j++) {
                sumaPerimetru -= mat[i][j];
            }
        }
        System.out.println("suma perimetru: " + sumaPerimetru);
    }

    public static void main(String[] args) {

        int mat[][] = {{1, 2, 3, 4},
                {5, 6, 7, 8},
                {9, 10, 11, 12},
                {13, 14, 15, 16}};

        parcurgeteMatrice(mat);
    }
}
