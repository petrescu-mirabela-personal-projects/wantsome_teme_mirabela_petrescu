package teme.exercitii_extra.ex9_reverse_list;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/*
9) Reverse list
Given a List<Integer> with some values, reverse it.
Can you solve it with in-place operations, without creating a new list? (do you
need any extra memory in this case?)
Note : you should not use Collections.reverse (or look at its implementation), but
write your own solution.
 */
public class ReverseList {

    public static void main(String[] args) {

        ReverseList reverseListI = new ReverseList();
        System.out.println(reverseListI.reverseList(newList(1, 2, 3, 4, 5, 6)));
        System.out.println(reverseListI.reverseList(newList(1, 2, 3, 4, 5, 6, 7)));
        System.out.println(reverseListI.reverseList(newList(1, 2, 3, 4, 5, 6, 7, 8)));
        System.out.println(reverseListI.reverseList(newList(1, 2, 3, 4, 5, 6, 7, 8, 9)));

        System.out.println(reverseListI.reverseList1(newList(1, 2, 3, 4, 5, 6)));
        System.out.println(reverseListI.reverseList1(newList(1, 2, 3, 4, 5, 6, 7)));
        System.out.println(reverseListI.reverseList1(newList(1, 2, 3, 4, 5, 6, 7, 8)));
        System.out.println(reverseListI.reverseList1(newList(1, 2, 3, 4, 5, 6, 7, 8, 9)));
    }

    //helper method for easy building a new Array with some given elements (of a custom type)
    @SafeVarargs
    private static <E> ArrayList<E> newList(E... elements) {
        return new ArrayList<>(Arrays.asList(elements));
    }

    private List<Integer> reverseList(List<Integer> list) {
        int size = list.size();
        for (int i = 0; i < size / 2; i++) {        // read the half of the list
            int value = list.get(size - 1 - i);     // keep the values of the last half elements (one by one)
            list.set(size - 1 - i, list.get(i));    // replace the elements from the last half (one by one) with the values of the first half
            list.set(i, value);                     // replace the elements of the first half (one by one) with the local variable value

        }
        return list;
    }

    // using swap method
    private List<Integer> reverseList1(List<Integer> list) {
        int size = list.size();
        for (int i = 0; i < size / 2; i++) {        // read the half of the list
            Collections.swap(list, i, size - 1 - i);

        }
        return list;
    }
}