package teme.w13.ex6_producer_consumer;

/*
Problem 6: Producer-Consumer
Consider having a class called StringsGenerator , that generates random strings using the
following algorithm:
    ● Generate a random integer between 3 and 13; this will be the length of the word
    ● Generate the string, by randomly generate each letter (in ‘a’-’z’ interval)
Write a StringsProcessor interface with the following implementations:
    ● StringsPrinter , prints a string to the console
    ● VowelsCounter , prints to the console the number of vowels in a word
    ● StringsLength , prints to the console the length of a string
    ● VowelsRemover , prints to the console the string that results from removing all vowels
Then write a StringsProducer thread that randomly generates 100 string values, and for
each of them it also chooses randomly one of the strings processing algorithms described
above, to be applied on that string later in consumer.
Finally write a StringsConsumer thread that receives the above objects (string value +
processing algorithm) and applies the algorithm to the string, and prints the result.
The communication between the two threads should be done via an ArrayBlockingQueue
instance. Use the put() and take() methods (and possibly peek() )
Optional:
- For a cleaner end of the program:
- after generating all values, put an extra special value on the queue (a “poison”
item) which will signal to the consumer threads, when they detect it, that they
should end too
- In main thread, after starting the new threads (1producer+2consumers), wait until
all new threads completed, before ending main thread too
- Make your solution work with 2 or more consumer threads; and for clearer output, print
from the consumers also the thread id, when processing a value from queue
- questions here: how many changes did you have to make to support this? What
about having 2 producers + 3 consumers? Did you need any changes also to the
consumer code handling the “poison” value? (for all consumer threads to finish)
 */

// Link: https://www.geeksforgeeks.org/arrayblockingqueue-put-method-in-java/
// Link: https://www.geeksforgeeks.org/arrayblockingqueue-take-method-in-java/
// Link: https://javarevisited.blogspot.com/2012/02/producer-consumer-design-pattern-with.html
// Link: https://stackoverflow.com/questions/25393938/multiple-producer-multiple-consumer-multithreading-java

import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class ProducerConsumer {

    private static final int CAPACITY = 10;

    public static void main(String[] args) {

        //  Creating shared object
        BlockingQueue<WordAlgorithm> queue =
                new ArrayBlockingQueue<>(ProducerConsumer.CAPACITY);

        int poisonRandomIndex = new Random().nextInt(ProducerConsumer.CAPACITY);
        System.out.println("poison index is: " + poisonRandomIndex);

        Producer producer1 = new Producer(queue, poisonRandomIndex, CAPACITY);
        Producer producer2 = new Producer(queue, poisonRandomIndex, CAPACITY);
        Consumer consumer1 = new Consumer(queue, CAPACITY);
        Consumer consumer2 = new Consumer(queue, CAPACITY);

        //Creating Producer and Consumer Thread
        // The two threads will access the same queue, in order to test its blocking capabilities.
        Thread producerThread1 = new Thread(producer1, "Producer1");
        Thread producerThread2 = new Thread(producer2, "Producer1");
        Thread consumerThread1 = new Thread(consumer1, "Consumer1");
        Thread consumerThread2 = new Thread(consumer2, "Consumer2");

        //Starting producer and Consumer thread
        producerThread1.start();
        producerThread2.start();
        consumerThread1.start();
        consumerThread2.start();

        /*
        In main thread, after starting the new threads (1producer+2consumers),
        wait until all new threads completed, before ending main thread too
         */
//        try {
//            producerThread1.join();
//            producerThread2.join();
//            consumerThread1.join();
//            consumerThread2.join();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        System.out.println("[Main] thread finished");

    }
}

class Producer extends Thread implements StringsProcessor {

    private BlockingQueue<WordAlgorithm> queue;
    private int poisonRandomIndex;
    private int capacity;

    public Producer(BlockingQueue<WordAlgorithm> queue, int poisonRandomIndex, int capacity) {
        this.queue = queue;
        this.poisonRandomIndex = poisonRandomIndex;
        this.capacity = capacity;
    }

    public void run() {
        //    synchronized (queue) {
        int processingAlgorithm;
        String wordGenerated;
        for (int i = 0; i < capacity; i++) {
//            synchronized (Producer.class) {
            processingAlgorithm = new Random().nextInt(4);
            if (i != poisonRandomIndex) {
                wordGenerated = StringsGenerator.generateString();
            } else {
                wordGenerated = "poison";
            }
            WordAlgorithm wordAlgorithm = new WordAlgorithm(wordGenerated, processingAlgorithm);
            System.out.println(" >>> Producer " + this.getName() + " >>> ");
            try {
                System.out.println("Trying to add to queue: String " + wordGenerated + " and the result is " + wordAlgorithm);
                queue.put(wordAlgorithm);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
//            }
            //     }
        }
    }

    @Override
    public String toString() {
        return "Producer{" +
                "queue=" + queue +
                '}';
    }
}

class Consumer extends Thread implements StringsProcessor {

    private static boolean isPoison;        // static to be shared between all the consumer threads
    private BlockingQueue<WordAlgorithm> queue;
    private int capacity;

    public Consumer(BlockingQueue<WordAlgorithm> queue, int capacity) {
        this.queue = queue;
        this.capacity = capacity;
    }

    public void run() {
        //synchronized (queue) {
            /*
            after generating all values, put an extra special value on the queue (a “poison”
            item) which will signal to the consumer threads, when they detect it, that they should end too
             */
        while (queue.remainingCapacity() < capacity && !isPoison) {
            //while(true){
            System.out.println(" ### Consumer " + this.getName() + " ### ");
            try {
                WordAlgorithm wordAlgorithm = queue.take();
                System.out.println("Trying to get from producer (queue): " + wordAlgorithm.getWord());
                System.out.println(" Queue size: " + queue.size() +
                        ", remaining capacity: " + queue.remainingCapacity());
                if (wordAlgorithm.getWord().equals("poison")) {
                    isPoison = true;
                    break;
                }

                switch (wordAlgorithm.getAlgorithm()) {
                    case 0:
                        StringsProcessor.super.StringsPrinter(wordAlgorithm.getWord());
                        break;
                    case 1:
                        StringsProcessor.super.StringsLength(wordAlgorithm.getWord());
                        break;
                    case 2:
                        StringsProcessor.super.VowelsCounter(wordAlgorithm.getWord());
                        break;
                    case 3:
                        StringsProcessor.super.VowelsRemover(wordAlgorithm.getWord());
                        break;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //    }
        }
    }
}
