package teme.w13.ex6_producer_consumer;

/*
Consider having a class called StringsGenerator , that generates random strings using the
following algorithm:
    ● Generate a random integer between 3 and 13; this will be the length of the word
    ● Generate the string, by randomly generate each letter (in ‘a’-’z’ interval)
 */

import java.util.Random;

public class StringsGenerator {

    /*
    Generate a random integer between 3 and 13; this will be the length of the word
     */
    public static int generateRandomNumber(int min, int max) {
        return new Random().nextInt(max - min + 1) + min;
    }

    /*
    Generate the string, by randomly generate each letter (in ‘a’-’z’ interval)
     */
    public static String generateString() {
        int length = generateRandomNumber(3, 13);
        String wordGenerated = "";
        int i = 0;
        while (i < length) {
            wordGenerated += (char) generateRandomNumber(97, 122);
            i++;
        }
        return wordGenerated;
    }

}
