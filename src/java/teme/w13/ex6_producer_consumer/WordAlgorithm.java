package teme.w13.ex6_producer_consumer;

public class WordAlgorithm {

    private String word;
    private int algorithm;

    public WordAlgorithm(String word, int algorithm) {
        this.word = word;
        this.algorithm = algorithm;
    }

    public String getWord() {
        return word;
    }

    public int getAlgorithm() {
        return algorithm;
    }

    @Override
    public String toString() {
        return "WordAlgorithm{" +
                "word='" + word + '\'' +
                ", algorithm=" + algorithm +
                '}';
    }
}
