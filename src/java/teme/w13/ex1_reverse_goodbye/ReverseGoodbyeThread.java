package teme.w13.ex1_reverse_goodbye;

/*
Problem 1: Reverse goodbye
Write a program that creates a thread (let's call it Thread 1). Thread 1 creates another thread
(Thread 2); Thread 2 creates Thread 3, and so on, up to Thread 10.
Each thread should print "Hello from thread <num>" and then “Goodbye from thread <num>”,
but you should structure your program such that the threads print their greetings in this nested
order:
Hello from thread 1
Hello from thread 2
...
Hello from thread 10
Goodbye from thread 10
...
Goodbye from thread 1
Note that the requirement is to actually create/run this code on multiple parallel threads! (not a
single main one).
Also, you should not use Thread.sleep() in your threads (would be too easy a solution :) and
also the program would run slower than necessary...)
Hint : read about .join() method of Thread class...
For this exercise, try to create the threads by extending the Thread class.
Optional : once you solve it, can you think of a second different solution for it?...
 */

public class ReverseGoodbyeThread extends Thread {

    private int counter;

    ReverseGoodbyeThread(int counter) {
        this.counter = counter;
    }

    @Override
    public void run() {
        System.out.println("Hello from thread: " + counter);
        if (counter < 10) {
            Thread tChild = new ReverseGoodbyeThread(counter + 1);
            tChild.start();
            try {
                tChild.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Goodbye from thread: " + counter);
    }
}
