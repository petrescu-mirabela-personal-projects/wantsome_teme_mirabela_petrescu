package teme.w13.ex2_shared_resource;


public class SharedResourceThreadChild extends SharedResourceThread {

    @Override
    public void run() {
        System.out.println("  Thread 2 started ...");
        System.out.println("  Thread 2 initial Common Var is: " + SharedResourceThread.commonVar);

        int initValue = SharedResourceThread.commonVar;
        while (initValue == SharedResourceThread.commonVar) {
            System.out.println("  Thread 2 value still unchanged, sleeping a little");
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("  Thread 2: VALUE CHANGED ! " + initValue + ", new " + SharedResourceThread.commonVar);
        System.out.println(".. Thread 2 ended");
    }
}
