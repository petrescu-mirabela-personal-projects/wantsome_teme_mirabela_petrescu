package teme.w05.ex3;

import java.util.*;

public class PersonsRegistryImpl implements PersonsRegistry {

    Map<Integer, Person> personsRegistry = new HashMap<>();

    /* Tries to adds a new person to the registry, throws exception if a person with same CNP is already registered.
     */
    @Override
    public void register(Person p) throws DuplicateCnpException {
        /* throws is a keyword in Java which is used in the signature of method to indicate that this method might throw one of the listed type exceptions.
        The caller to these methods has to handle the exception using a try-catch block.
         */
        if (!personsRegistry.containsKey(p.getCnp())) {     // if the cnp is not in the registry
            personsRegistry.put(p.getCnp(), p);
        } else {
            throw new DuplicateCnpException("Registering a second person with same cnp should have failed!" +
                    personsRegistry.get(p.getCnp()).toString());
        }
    }

    /* Finds a person by cnp and removes it from registry.
    If person is not found, will still work (no errors, and does nothing)
     */
    @Override
    public void unregister(int cnp) {
        if (personsRegistry.containsKey(cnp)) {     // if the key cnp is found
            personsRegistry.remove(cnp);            // remove the entire element of the map
        }
    }

    /* Get the number of currently registered persons.
     */
    @Override
    public int count() {
        return personsRegistry.size();
    }

    /* Get the list of cnp values of all persons.
     */
    @Override
    public Set<Integer> cnps() {
        return personsRegistry.keySet();
    }

    /* Get the list of unique names of all persons.
     */
    @Override
    public Set<String> uniqueNames() {
        Set<String> uniqueNamesList = new HashSet<>();
        for (int keyCNP : personsRegistry.keySet()) {
            uniqueNamesList.add(personsRegistry.get(keyCNP).getName());
        }
        return uniqueNamesList;
    }

    /* Find a person by cnp; returns null if no person found.
     */
    @Override
    public Person findByCnp(int cnp) {
        // return the value for the specified key (if found)
        if (personsRegistry.containsKey(cnp)) {
            return personsRegistry.get(cnp);
        } else {
            return null;
        }
    }

    /* Find the persons with a specified name (may be zero, one or more)
       Comparing person name with specified name should be case insensitive.
    */
    @Override
    public Set<Person> findByName(String name) {
        Set<Person> persons = new LinkedHashSet<>();
        for (int keyCNP : personsRegistry.keySet()) {
            if (personsRegistry.get(keyCNP).getName().equalsIgnoreCase(name)) {
                persons.add(personsRegistry.get(keyCNP));
            }
        }
        return persons;
    }

    /* Get the average age for all persons (or 0 if it cannot be computed)
     */
    @Override
    public double averageAge() {
        double averageAllAge = 0;
        int count;
        for (int keyCNP : personsRegistry.keySet()) {
            averageAllAge += personsRegistry.get(keyCNP).getAge();
        }
        count = personsRegistry.keySet().size();
        averageAllAge /= count;
        return count > 0 ? averageAllAge : 0;
    }

    /* Get the percent (value between 0-100) of adults (persons with age>=18)
       from the number of all persons (or 0 as default if it cannot be computed)
    */
    @Override
    public double adultsPercentage() {
        double adultsCount = 0;
        for (int keyCNP : personsRegistry.keySet()) {
            if (personsRegistry.get(keyCNP).getAge() >= 18) {
                adultsCount++;
            }
        }
        return adultsCount > 0 ? adultsCount * 100 / personsRegistry.size() : 0;
    }

    /* Get the percent (value between 0-100) of adults who voted from the number of all adult persons (age>=18)
     */
    @Override
    public double adultsWhoVotedPercentage() {
        double adultsWhoVotedCount = 0;
        for (int keyCNP : personsRegistry.keySet()) {
            if (personsRegistry.get(keyCNP).isHasVoted()) {
                adultsWhoVotedCount++;
            }
        }
        return adultsWhoVotedCount > 0 ? adultsWhoVotedCount * 100 / (adultsPercentage() * personsRegistry.size() / 100) : 0;
    }
}
