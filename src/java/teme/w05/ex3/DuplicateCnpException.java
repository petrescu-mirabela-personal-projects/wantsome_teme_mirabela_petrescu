package teme.w05.ex3;

/*
To create a custom exception, we have to extend the java.lang.Exception class.
 */

// Link: https://www.baeldung.com/java-new-custom-exception

public class DuplicateCnpException extends Exception {

    public DuplicateCnpException(String errorMessage) {
        // we also have to provide a constructor that takes a String as the error message and called the parent class constructor
        super(errorMessage);
    }
}
