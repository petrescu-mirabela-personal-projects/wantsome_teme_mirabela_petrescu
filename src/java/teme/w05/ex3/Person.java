package teme.w05.ex3;

/*
Create a class Person having these properties:
- cnp - an integer number (optional: validate that it’s >0)
- name - a string (optioan: validate it’s not empty)
- age - an integer number (optional: validate that it’s >0)
- hasVoted - a boolean, set to true if person has voted in last elections
 */

public class Person {
    private int cnp;
    private String name;
    private int age;
    private boolean hasVoted;

    public Person(int cnp, String name, int age, boolean hasVoted) {
        if (cnp > 0 && age > 0) {
            this.cnp = cnp;
            this.age = age;
        }
        if (!name.isEmpty()) {
            this.name = name;
        }
        this.hasVoted = hasVoted;
    }

    public int getCnp() {
        return cnp;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public boolean isHasVoted() {
        return hasVoted;
    }

    @Override
    public String toString() {
        return "Person{" +
                "cnp=" + cnp +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", hasVoted=" + hasVoted +
                '}';
    }
}
