package teme.w05.ex1;

import java.util.*;

/* Create a BuildingRegistry class, which contains some (static) methods which perform
these tasks using a list of buildings received as a parameter:
● return the average price for each building category .
● return the average price for each neighborhood .
● return the number of categories (of the actual buildings, not all possible
categories)
● return the list of unique neighborhoods (hint: use a Set)
*/

public class BuildingRegistry {

    /* return the average price for each building category .
     */
    public static double averagePriceEachBuilding(List<Building> buildings, Category category) {
        double averagePrice = 0;
        int count = 0;
        for (int i = 0; i < buildings.size(); i++) {
            if (buildings.get(i).getCategory().equals(category)) {
                averagePrice += buildings.get(i).getPrice();
                count++;
            }
        }
        averagePrice /= count;
        return averagePrice;
    }

    /* return the average price for each neighborhood.
     */
    public static double averagePriceEachNeighborhood(List<Building> buildings, String neighborhood) {
        double averagePrice = 0;
        int count = 0;
        for (int i = 0; i < buildings.size(); i++) {
            if (buildings.get(i).getNeighborhood().equals(neighborhood)) {
                averagePrice += buildings.get(i).getPrice();
                count++;
            }
        }
        averagePrice /= count;
        return averagePrice;
    }

    /* return the number of categories (of the actual buildings, not all possible categories)
     */
    public static int numberCategories(List<Building> buildings) {

        Set<Category> setCategory = new HashSet<>();
        for (Building itemBuilding : buildings) {
            setCategory.add(itemBuilding.getCategory());
        }
        return setCategory.size();
    }

    /* return the list of unique neighborhoods (hint: use a Set)
     */
    public static Set<String> uniqueNeighborhoods(List<Building> buildings) {

        Set<String> setNeighborhoods = new HashSet<>();
        for (Building itemBuilding : buildings) {
            setNeighborhoods.add(itemBuilding.getNeighborhood());
        }
        return setNeighborhoods;
    }

    public static void main(String[] args) {

        Building buidling01 = new Building("Main", Category.RESIDENTIAL, 1700, "Copou");
        Building buidling02 = new Building("Principal", Category.RESIDENTIAL, 1200, "Copou");
        Building buidling03 = new Building("Second", Category.OFFICE, 1800, "Alexandru");
        Building buidling04 = new Building("None", Category.HOSPITAL, 500, "Gara");
        Building buidling05 = new Building("Other", Category.RESIDENTIAL, 1300, "Tudor");

        // listBuilding.add(1, buidling01);
        List<Building> listBuilding = new ArrayList<Building>(Arrays.asList(buidling01, buidling02, buidling03, buidling04, buidling05));

        System.out.println(averagePriceEachBuilding(listBuilding, Category.RESIDENTIAL));
        System.out.println(averagePriceEachNeighborhood(listBuilding, "Copou"));
        System.out.println(numberCategories(listBuilding));
        System.out.println(uniqueNeighborhoods(listBuilding));

    }
}
