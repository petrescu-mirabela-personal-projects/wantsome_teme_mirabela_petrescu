package teme.w08.ex0;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class WarmUp {

    /*
    a. Given a list of Strings, write a method that returns a list of all strings
    that start with the letter 'a' (lower case) and have exactly 3 letters.
     */

    private static void pointA() {
        List<String> list = Arrays.asList("aaa", "xc", "werrr", "asd", "");

        List<String> result = list
                .stream()
                .filter(s -> s.length() == 3 && s.startsWith("a"))
                .collect(toList());
        System.out.println(result);
    }

    /*
    b. Write a method that returns a comma separated string based on a
    given list of integers. Each element should be preceded by the letter
    'e' if the number is even, and preceded by the letter 'o' if the number
    is odd. For example, if the input list is (3,44), the output should be
    'o3,e44'.
     */
    private static void pointB() {
        List<Integer> intList = Arrays.asList(3, 44, 5, 2);

        String joined = intList
                .stream()
                .map(i -> (i % 2 == 0 ? "e" : "o") + i)
                .collect(Collectors.joining(", ", "{", "}"));

        System.out.println("joined: " + joined);
    }

    /*
    c. Given a string, count the occurrences of each letter (hint: see
    Collectors.groupingBy, Collectors.counting..)
     */
    private static void pointC() {
        String phrase = "aaaazzzeqre";
        String[] letters = phrase.split("");

        //Solution1: with 2 separate operations: a simple grouping...
        Map<String, List<String>> grouped =
                Arrays.stream(letters)
                        .collect(Collectors.groupingBy(s -> s));
        System.out.println(grouped);

        //...then another map() operation, on map entries, to replace the groups of letters with only the group size
        String counts = grouped  //Map<String, List<String>>
                .entrySet() //Set<Map.Entry<String, List<String>>>
                .stream()   //Stream<Map.Entry<String, List<String>>>
                .map(entry -> entry.getKey() + ":" + entry.getValue().size()) //Stream<String>
                .collect(Collectors.joining(", ")); //String

        System.out.println(counts); //=> "q:1, a:4, r:1, e:2, z:3"


        //Solution2: all can be done in a SINGLE stream pipeline, but with a more complex collect,
        // using .groupingBy() with extra params!
        //see more info:  https://dzone.com/articles/the-ultimate-guide-to-the-java-stream-api-grouping
        Map<String, Long> groupSizes =
                Arrays
                        .stream(letters)
                        .collect(Collectors.groupingBy(s -> s, Collectors.counting()));
        System.out.println(groupSizes);
    }

    /*
    d. Print the first 10 powers of 3 which are greater than 1000 (hint: use
    IntStream.iterate(), limit(), filter(), boxed()..)
    Output should be like: 2187, 6561, ...
     */
    private static void pointD() {
        Stream<Integer> st = Stream.iterate(1, n -> n * 3)
                .filter(m -> m > 1000)
                .limit(10);

        List<Integer> list = st.collect(toList());
        System.out.println(list);
    }

    /*
    e. Print all the square numbers (of form n^2) between 1000 and 10000
    (hint: use IntStream.range(), map(), boxed()..)
    Output should be like: 1024, 1089, 1156, ....
     */
    private static void pointE() {
        IntStream.rangeClosed(1, 100)
                .map(n -> n * n)
                .filter(i -> i > 1000 && i < 10000)
                .forEach(System.out::println);
    }

    /*
    f. Solve again the problems from Week 5 (Collections), but using
    stream operations whenever possible - the problems:
    - “Warm-up” (counting words)
    - “Buildings Registry”
    - “Persons Registry”.
     */

    public static void main(String[] args) {

        pointA();
        pointB();
        pointC();
        pointD();
        pointE();
    }
}
