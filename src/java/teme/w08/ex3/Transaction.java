package teme.w08.ex3;

import java.util.Objects;

class Transaction {
    private final int year;
    private final int value;
    private final Trader trader;

    public Transaction(int year, int value, Trader trader) {
        this.year = year;
        this.value = value;
        this.trader = trader;
    }

    public int getYear() {
        return year;
    }

    public int getValue() {
        return value;
    }

    public Trader getTrader() {
        return trader;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Transaction)) return false;
        Transaction that = (Transaction) o;
        return year == that.year &&
                value == that.value &&
                Objects.equals(trader, that.trader);
    }

    @Override
    public int hashCode() {
        return Objects.hash(year, value, trader);
    }
}
