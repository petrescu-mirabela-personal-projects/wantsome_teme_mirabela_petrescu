package teme.w08.ex3;

import java.util.*;
import java.util.function.Predicate;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

public class TraderTransaction {

    List<Transaction> listTransaction = new ArrayList<>();

    public static void main(String[] args) {

        TraderTransaction traderTransaction = new TraderTransaction();

        Transaction t1 = new Transaction(2000, 123, new Trader("Popescu", "Iasi"));
        Transaction t2 = new Transaction(2000, 154, new Trader("Cazacu", "Bacau"));
        Transaction t3 = new Transaction(2011, 500, new Trader("Pintilie", "Milan"));
        Transaction t4 = new Transaction(2000, 100, new Trader("Rotaru", "Cambridge"));
        Transaction t5 = new Transaction(2010, 124, new Trader("Pricopi", "Cluj"));
        Transaction t6 = new Transaction(2011, 90, new Trader("Vasiliu", "Cambridge"));
        Transaction t7 = new Transaction(1999, 170, new Trader("Paun", "Constanta"));
        Transaction t8 = new Transaction(2001, 186, new Trader("Radacina", "Cambridge"));
        Transaction t9 = new Transaction(2011, 30, new Trader("Iliescu", "Bucuresti"));
        Transaction t10 = new Transaction(2011, 16, new Trader("Constantinescu", "Cluj"));

        traderTransaction.listTransaction = newSet(t1, t2, t3, t4, t5, t6, t7, t8, t9, t10);

        System.out.println("\nAll transactions from 2011 sorted by value (small to high) => " + traderTransaction.findTransactionFrom2011());
        System.out.println("\nAll transactions from 2011 sorted by value (small to high) - using lambda => " + traderTransaction.findTransactionFrom2011Lambda());
        System.out.println("\nAll transactions from 2011 sorted by value (small to high) - using Predicate => " + traderTransaction.findTransactionFrom2011Predicate());

        System.out.println("\nAll the unique cities where traders work => " + traderTransaction.uniqueCities());
        System.out.println("\nAll the unique cities where traders work - using Set => " + traderTransaction.uniqueCitiesSet());

        System.out.println("\nAll traders from Cambridge, sorted by name (descending) => " + traderTransaction.tradersFromCambridge());

        System.out.println("\nAll traders’ names sorted alphabetically => " + traderTransaction.allTradersName());

        System.out.println("\nAre any traders from Milan ? => " + traderTransaction.anyTraderFromMilan());

        System.out.println("\nBefore update all transactions so that the traders from Milan are moved to Cambridge");
        System.out.println(traderTransaction.listTransaction);
        traderTransaction.tradersMilanToCambridge();
        System.out.println("After update all transactions so that the traders from Milan are moved to Cambridge");
        System.out.println(traderTransaction.listTransaction);

        System.out.println("\nHighest transaction value is: " + traderTransaction.highestTrasactionValue());

        System.out.println("\nLowest transaction value is: " + traderTransaction.lowestTrasactionValue());
    }

    //helper method for easy building a new Array with some given elements (of a custom type)
    @SafeVarargs
    private static <E> List<E> newSet(E... elements) {
        return new ArrayList<>(Arrays.asList(elements));
    }

    /*
        Find all transactions from 2011 and sort them by value (small to high)
         */
    public List<Transaction> findTransactionFrom2011() {
        return listTransaction.stream()
                .filter(j -> j.getYear() == 2011)
                .sorted(new Comparator<Transaction>() {
                    @Override
                    public int compare(Transaction o1, Transaction o2) {
                        return Integer.compare(o1.getValue(), o2.getValue());
                    }
                })
                .collect(toList());
    }

    public List<Transaction> findTransactionFrom2011Lambda() {
        return listTransaction.stream()
                .filter(j -> j.getYear() == 2011)
                .sorted(Comparator.comparingInt(Transaction::getValue))
                .collect(toList());
    }

    // using Predicate - Find all transactions from 2011 and sort them by value (small to high)
    public List<Transaction> findTransactionFrom2011Predicate() {
        Predicate<Transaction> predicate = s -> s.getYear() == 2011;
        return listTransaction.stream()
                .filter(predicate)
                .sorted(Comparator.comparingInt(Transaction::getValue))
                .collect(toList());
    }

    /*
    What are all the unique cities where traders work?
     */
    public List<String> uniqueCities() {
        return listTransaction.stream()
                .map(s -> s.getTrader().getCity())
                .distinct()
                .sorted()
                .collect(toList());
    }

    public Set<String> uniqueCitiesSet() {
        return listTransaction.stream()
                .map(s -> s.getTrader().getCity())
                .collect(toSet());
    }

    /*
    Find all traders from Cambridge and sort them by name (descending)
     */
    public List<Transaction> tradersFromCambridge() {
        return listTransaction.stream()
                .filter(s -> s.getTrader().getCity().equals("Cambridge"))
                .sorted((o1, o2) -> -o1.getTrader().getName().compareTo(o2.getTrader().getName()))
                .collect(toList());
    }

    /*
    Return a string of all traders’ names sorted alphabetically and separated by comma
     */
    public List<String> allTradersName() {
        return listTransaction.stream()
                .map(s -> s.getTrader().getName())
                .distinct()
                .sorted()
                .collect(toList());
    }

    /*
    Determine if there are any traders from Milan
     */
    public boolean anyTraderFromMilan() {
        return listTransaction.stream()
                .anyMatch(s -> s.getTrader().getCity().equals("Milan"));
    }

    /*
    Update all transactions so that the traders from Milan are moved to Cambridge
     */
    public void tradersMilanToCambridge() {
        listTransaction.stream()
                .filter(s -> s.getTrader().getCity().equals("Milan"))
                .forEach(s -> s.getTrader().setCity("Cambridge"));
    }

    /*
    What’s the highest value in all transactions?
     */
    public Optional<Integer> highestTrasactionValue() {
        return listTransaction.stream()
                .map(Transaction::getValue)
                .reduce((s1, s2) -> s1 > s2 ? s1 : s2);
    }

    /*
    What’s the transaction with the lowest value?
     */
    public Optional<Integer> lowestTrasactionValue() {
        return listTransaction.stream()
                .map(Transaction::getValue)
                .reduce((s1, s2) -> s1 < s2 ? s1 : s2);
    }
}
