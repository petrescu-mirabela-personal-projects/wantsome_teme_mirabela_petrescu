package teme.w03.ex2;

/*
---------------------------------
Ex2. Modelling a room of persons
---------------------------------

Create a class Room, which will model a room in which multiple persons can be present at the same time:

a) the room has a field named capacity (showing the maximum number of person which can be in the room at the same time)
   - this property is mandatory and should be always required when a new Room instance is built (hint: need to handle it in a constructor)
   - once a room is built, the capacity cannot be changed, but it should be readable from outside (hint: need a getter, but no setter)

b) the room should be able to store multiple Person objects (up to its max capacity)
   hint: you’ll need a field holding an array of Person objects (initialized also in constructor, based on the given room capacity)

c) the room should have these methods defined:
  - int getCount() - return the number of persons currently in the room

  - void printAll() - should print the room’s capacity, the number of persons currently in the room, and the details
    of each person (using the toString() method of Person, printed one per row)

  - void enter(Person person) -> should add the given person to the room, if there is still space left
    (current number of persons < max capacity), or else print an error message and ignore this person
    hint: you may need to have a separate field to count the actual number of persons in the room,
          personsCount, which is different/lower than max room capacity
    optional: add a validation so we don’t accept in the room 2 persons with the exact same name
              (in that case may just print an error message and ignore the person with duplicate name)

  - boolean isPresent(String personName) -> should return true only if a person with the specified name is currently in the room

  - String getOldest() -> returns the name and age of the oldest person in the room, as a single String value
    (format: “<name>(<birthYear>)”), or empty string if no person found (question: can this ever happen? when?..)

  - String[] getNames(String hairColor) -> returns an array with the names of all persons in room
    which have the hairColor property equal to the given color

  - void exit(String personName) -> search and remove from room the person(s) which have the specified name
    hint1: once a person is found and should be removed, you need to update the array so all persons coming after it
           should be moved one position lower (towards the beginning of the array); also, the current persons counter
           needs to be updated after this
    hint2: you may want to define a helper method int indexOf(String personName) which searches for and returns
           the index of the person in room or -1 if not found; this would have some code very similar to one in isPresent(),
           so you should avoid duplication and make isPresent() use this helper method;
           you can then easily use same helper method for this exit() method
*/

import teme.w03.ex1.Person;

import java.util.Arrays;

public class Room {

    private int capacity;
    private Person[] persons;
    private int personsCount;

    public Room(int capacity) {
        this.capacity = capacity;
        persons = new Person[capacity]; // the room should be able to store multiple Person objects (up to its max capacity)
    }

    //--- this is here just for manual testing ---//
    public static void main(String[] args) {
        //Room r = ...
    }

    public Person[] getPersons() {
        return persons;
    }

    public void setPersons(Person[] persons) {
        this.persons = persons;
    }

    public int getCapacity() {
        return capacity;
    }

    /**
     * return the number of persons currently in the room
     */
    int getCount() {
        return personsCount;
    }

    /**
     * should print the room’s capacity, the number of persons currently in the room, and the details
     * of each person (using the toString() method of Person, printed one per row)
     */
    void printAll() {
        System.out.println("Room capacity is " + capacity);
        System.out.println("Number of persons in the room is " + personsCount);
        if (personsCount > 0) {
            for (int i = 0; i < persons.length; i++) {
                persons[i].toString();
            }
        }
    }

    /**
     * should add the given person to the room, if there is still space left
     * (current number of persons < max capacity), or else print an error message and ignore this person
     * hint: you may need to have a separate field to count the actual number of persons in the room,
     * personsCount, which is different/lower than max room capacity
     * optional: add a validation so we don’t accept in the room 2 persons with the exact same name
     * (in that case may just print an error message and ignore the person with duplicate name)
     */
    void enter(Person person) {
        if (personsCount == 0) {
            persons[personsCount++] = person;
        } else if (personsCount < capacity) {
            /** // >>>>>>> without isPresent function call
             int count = 0;
             for (int i = 0; i < personsCount; i++) {
             if (persons[i++].getName().equals(person.getName())) {
             count++;
             }
             }
             if (count == 0) {
             persons[personsCount++] = person;
             }*/
            if (!isPresent(person.getName())) {
                persons[personsCount++] = person;
            } else {
                System.out.println("Error: Person name already exist");
            }
        } else {
            System.out.println("Error: Not enough capacity");
        }
    }

    /**
     * should return true only if a person with the specified name is currently in the room
     */
    boolean isPresent(String personName) {
/*      // Version code without indexOf function call
        int count = 0;
        for (int i = 0; i < personsCount; i++) {
            if (persons[i++].getName().equalsIgnoreCase(personName)) {
                count++;
            }
        }
        return (count != 0);*/

        return indexOf(personName).length > 0;
    }

    /**
     * returns the name and age of the oldest person in the room, as a single String value
     * (format: “<name>(<birthYear>)”), or empty string if no person found (question: can this ever happen? when?..)
     */
    String getOldest() {
        int oldestYear;
        String oldestPerson = "";
        if (personsCount > 0) {
            oldestYear = persons[0].getBirthYear();
            oldestPerson = persons[0].getName() + "(" + oldestYear + ")";    // consider only the first person
            for (int i = 1; i < personsCount; i++) {
                if (persons[i].getBirthYear() < oldestYear) {
                    oldestPerson = persons[i].getName() + "(" + persons[i].getBirthYear() + ")";
                }
            }
        }

        return oldestPerson;
    }

    /**
     * returns an array with the names of all persons in room which have the hairColor property equal to the given color
     */
    String[] getNames(String hairColor) {
        String[] personsHairColor = new String[personsCount]; // empty string array
        int k = 0;
        if (personsCount > 0) {
            for (int i = 0; i < personsCount; i++) {
                if (persons[i].getHairColor().equalsIgnoreCase(hairColor)) {
                    personsHairColor[k++] = persons[i].getName();
                }
            }
        }
        return Arrays.copyOf(personsHairColor, k);
    }

    /**
     * search and remove from room the person(s) which have the specified name
     * hint1: once a person is found and should be removed, you need to update the array so all persons coming after it
     * should be moved one position lower (towards the beginning of the array); also, the current persons counter
     * needs to be updated after this
     * hint2: you may want to define a helper method int indexOf(String personName) which searches for and returns
     * the index of the person in room or -1 if not found; this would have some code very similar to one in isPresent(),
     * so you should avoid duplication and make isPresent() use this helper method;
     * you can then easily use same helper method for this exit() method
     */
    void exit(String personName) {

        if (personsCount > 1) {
            for (int i = 1; i < personsCount; i++) {
                if (persons[i - 1].getName().equalsIgnoreCase(personName)) {
                    int j = i;
                    while (j < personsCount) {
                        persons[j - 1] = persons[j];
                        j++;
                    }
                    personsCount--;
                    persons = Arrays.copyOf(persons, personsCount);
                }
            }
        } else {
            personsCount = 0;
        }

    }

    private int[] indexOf(String personName) {
        int[] indexArray = new int[personsCount];
        int i, k = 0;
        if (personsCount == 0) {
            indexArray = new int[]{};
        }
        for (i = 0; i < personsCount; i++) {
            if (persons[i++].getName().equalsIgnoreCase(personName)) {
                indexArray[k++] = i;
            }
        }
        return Arrays.copyOf(indexArray, k);
    }

}
