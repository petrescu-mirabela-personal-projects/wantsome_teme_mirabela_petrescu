package teme.w03.ex7;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Unit tests for MyArrayList class (should compile and pass all tests after you complete that one first)
 */
public class MyLinkedListTests {

    @Test
    public void testAddNodeLastPosition() {
        /* add a new element into the list - at the end */
        MyLinkedList listLastPos = new MyLinkedList();

        listLastPos.add(1);
        assertEquals("MyLinkedList{head=Node{dataNode=1, linkNode=null}}", listLastPos.toString());

        listLastPos.add(2);
        assertEquals("MyLinkedList{head=Node{dataNode=1, linkNode=Node{dataNode=2, linkNode=null}}}", listLastPos.toString());

        listLastPos.add(3);
        assertEquals("MyLinkedList{head=Node{dataNode=1, linkNode=Node{dataNode=2, linkNode=Node{dataNode=3, linkNode=null}}}}", listLastPos.toString());
    }

    @Test
    public void testAddNodeFirstPosition() {
        /* add a new element into the list - first position in the list */
        MyLinkedList listFirstPos = new MyLinkedList();

        listFirstPos.addNodeFirstPosition(1);
        assertEquals("MyLinkedList{head=Node{dataNode=1, linkNode=null}}", listFirstPos.toString());

        listFirstPos.addNodeFirstPosition(2);
        assertEquals("MyLinkedList{head=Node{dataNode=2, linkNode=Node{dataNode=1, linkNode=null}}}", listFirstPos.toString());

        listFirstPos.addNodeFirstPosition(3);
        assertEquals("MyLinkedList{head=Node{dataNode=3, linkNode=Node{dataNode=2, linkNode=Node{dataNode=1, linkNode=null}}}}", listFirstPos.toString());
    }

    @Test
    public void testAddNodeIndexPosition() {
        /* add a new element into the list - first position in the list */
        MyLinkedList listIndexPos = new MyLinkedList();

        listIndexPos.addNodeFirstPosition(1);
        listIndexPos.addNodeFirstPosition(2);
        listIndexPos.addNodeFirstPosition(3);
        assertEquals("MyLinkedList{head=Node{dataNode=3, linkNode=Node{dataNode=2, linkNode=Node{dataNode=1, linkNode=null}}}}", listIndexPos.toString());

        /* add a new Node with data 5 into the list - after Node with data 2 */
        listIndexPos.add(2, 5);
        assertEquals("MyLinkedList{head=Node{dataNode=3, linkNode=Node{dataNode=2, linkNode=Node{dataNode=5, linkNode=Node{dataNode=1, linkNode=null}}}}}", listIndexPos.toString());

        /* add a new Node with data 9 into the list - after Node with data 3 */
        listIndexPos.add(3, 9);
        assertEquals("MyLinkedList{head=Node{dataNode=3, linkNode=Node{dataNode=9, linkNode=Node{dataNode=2, linkNode=Node{dataNode=5, linkNode=Node{dataNode=1, linkNode=null}}}}}}", listIndexPos.toString());

        /* add a new Node with data 9 into the list - after Node with data 3 */
        listIndexPos.add(1, 10);
        assertEquals("MyLinkedList{head=Node{dataNode=3, linkNode=Node{dataNode=9, linkNode=Node{dataNode=2, linkNode=Node{dataNode=5, linkNode=Node{dataNode=1, linkNode=Node{dataNode=10, linkNode=null}}}}}}}", listIndexPos.toString());

    }

    @Test
    public void testIterateList() {
        /* add a new element into the list - first position in the list */
        MyLinkedList listIndexPos = new MyLinkedList();

        listIndexPos.addNodeFirstPosition(1);
        listIndexPos.addNodeFirstPosition(2);
        listIndexPos.addNodeFirstPosition(3);
        assertEquals("MyLinkedList{head=Node{dataNode=3, linkNode=Node{dataNode=2, linkNode=Node{dataNode=1, linkNode=null}}}}", listIndexPos.toString());

        /* add a new Node with data 5 into the list - after Node with data 2 */
        listIndexPos.add(2, 5);
        assertEquals("MyLinkedList{head=Node{dataNode=3, linkNode=Node{dataNode=2, linkNode=Node{dataNode=5, linkNode=Node{dataNode=1, linkNode=null}}}}}", listIndexPos.toString());

        /* add a new Node with data 9 into the list - after Node with data 3 */
        listIndexPos.add(3, 9);
        assertEquals("MyLinkedList{head=Node{dataNode=3, linkNode=Node{dataNode=9, linkNode=Node{dataNode=2, linkNode=Node{dataNode=5, linkNode=Node{dataNode=1, linkNode=null}}}}}}", listIndexPos.toString());

        /* iterate till the end of the list */
        listIndexPos.iterateList();
        assertEquals("MyLinkedList{head=Node{dataNode=3, linkNode=Node{dataNode=9, linkNode=Node{dataNode=2, linkNode=Node{dataNode=5, linkNode=Node{dataNode=1, linkNode=null}}}}}}", listIndexPos.toString());

        /* iterate an empty list */
        MyLinkedList listIndexPosEmpty = new MyLinkedList();
        listIndexPosEmpty.iterateList();
        assertEquals("MyLinkedList{head=null}", listIndexPosEmpty.toString());
    }

    @Test
    public void testRemoveNodeIndexPosition() {
        /* add a new element into the list - first position in the list */
        MyLinkedList listIndexPos = new MyLinkedList();

        listIndexPos.addNodeFirstPosition(1);
        listIndexPos.addNodeFirstPosition(2);
        listIndexPos.addNodeFirstPosition(3);
        assertEquals("MyLinkedList{head=Node{dataNode=3, linkNode=Node{dataNode=2, linkNode=Node{dataNode=1, linkNode=null}}}}", listIndexPos.toString());

        /* add a new Node with data 5 into the list - after Node with data 2 */
        listIndexPos.add(2, 5);
        assertEquals("MyLinkedList{head=Node{dataNode=3, linkNode=Node{dataNode=2, linkNode=Node{dataNode=5, linkNode=Node{dataNode=1, linkNode=null}}}}}", listIndexPos.toString());

        /* add a new Node with data 9 into the list - after Node with data 3 */
        listIndexPos.add(3, 9);
        assertEquals("MyLinkedList{head=Node{dataNode=3, linkNode=Node{dataNode=9, linkNode=Node{dataNode=2, linkNode=Node{dataNode=5, linkNode=Node{dataNode=1, linkNode=null}}}}}}", listIndexPos.toString());

        /* remove Node data 2 - last Node of the Linked List */
        listIndexPos.remove(2);
        assertEquals("MyLinkedList{head=Node{dataNode=3, linkNode=Node{dataNode=9, linkNode=Node{dataNode=5, linkNode=Node{dataNode=1, linkNode=null}}}}}", listIndexPos.toString());
        listIndexPos.remove(1);
        assertEquals("MyLinkedList{head=Node{dataNode=3, linkNode=Node{dataNode=9, linkNode=Node{dataNode=5, linkNode=null}}}}", listIndexPos.toString());

        /* remove a node which is not in the linked list */
        listIndexPos.remove(50);
        assertEquals("MyLinkedList{head=Node{dataNode=3, linkNode=Node{dataNode=9, linkNode=Node{dataNode=5, linkNode=null}}}}", listIndexPos.toString());
    }


    @Test
    public void testRemoveNodeLastPosition() {
        /* add a new element into the list - first position in the list */
        MyLinkedList listIndexPos = new MyLinkedList();
        listIndexPos.addNodeFirstPosition(1);
        listIndexPos.addNodeFirstPosition(2);
        listIndexPos.addNodeFirstPosition(3);
        assertEquals("MyLinkedList{head=Node{dataNode=3, linkNode=Node{dataNode=2, linkNode=Node{dataNode=1, linkNode=null}}}}", listIndexPos.toString());
        /* add a new Node with data 5 into the list - after Node with data 2 */
        listIndexPos.add(2, 5);
        assertEquals("MyLinkedList{head=Node{dataNode=3, linkNode=Node{dataNode=2, linkNode=Node{dataNode=5, linkNode=Node{dataNode=1, linkNode=null}}}}}", listIndexPos.toString());
        /* add a new Node with data 9 into the list - after Node with data 3 */
        listIndexPos.add(3, 9);
        assertEquals("MyLinkedList{head=Node{dataNode=3, linkNode=Node{dataNode=9, linkNode=Node{dataNode=2, linkNode=Node{dataNode=5, linkNode=Node{dataNode=1, linkNode=null}}}}}}", listIndexPos.toString());
        listIndexPos.iterateList();
        assertEquals("MyLinkedList{head=Node{dataNode=3, linkNode=Node{dataNode=9, linkNode=Node{dataNode=2, linkNode=Node{dataNode=5, linkNode=Node{dataNode=1, linkNode=null}}}}}}", listIndexPos.toString());

        /* remove Node data 2 - last Node of the Linked List */
        listIndexPos.remove(2);
        assertEquals("MyLinkedList{head=Node{dataNode=3, linkNode=Node{dataNode=9, linkNode=Node{dataNode=5, linkNode=Node{dataNode=1, linkNode=null}}}}}", listIndexPos.toString());

        /* remove Node data 1 - last Node of the Linked List */
        listIndexPos.remove(1);
        assertEquals("MyLinkedList{head=Node{dataNode=3, linkNode=Node{dataNode=9, linkNode=Node{dataNode=5, linkNode=null}}}}", listIndexPos.toString());

        listIndexPos.remove();
        assertEquals("MyLinkedList{head=Node{dataNode=3, linkNode=Node{dataNode=9, linkNode=null}}}", listIndexPos.toString());

        /* remove an empty linked list */
        MyLinkedList listLastPosEmpty = new MyLinkedList();
        listLastPosEmpty.removeNodeFirstPosition();
        assertEquals("MyLinkedList{head=null}", listLastPosEmpty.toString());
    }

    @Test
    public void testRemoveNodeFirstPosition() {
        /* add a new element into the list - first position in the list */
        MyLinkedList listIndexPos = new MyLinkedList();

        listIndexPos.addNodeFirstPosition(1);
        listIndexPos.addNodeFirstPosition(2);
        listIndexPos.addNodeFirstPosition(3);
        assertEquals("MyLinkedList{head=Node{dataNode=3, linkNode=Node{dataNode=2, linkNode=Node{dataNode=1, linkNode=null}}}}", listIndexPos.toString());

        /* add a new Node with data 5 into the list - after Node with data 2 */
        listIndexPos.add(2, 5);
        assertEquals("MyLinkedList{head=Node{dataNode=3, linkNode=Node{dataNode=2, linkNode=Node{dataNode=5, linkNode=Node{dataNode=1, linkNode=null}}}}}", listIndexPos.toString());

        /* add a new Node with data 9 into the list - after Node with data 3 */
        listIndexPos.add(3, 9);
        assertEquals("MyLinkedList{head=Node{dataNode=3, linkNode=Node{dataNode=9, linkNode=Node{dataNode=2, linkNode=Node{dataNode=5, linkNode=Node{dataNode=1, linkNode=null}}}}}}", listIndexPos.toString());

        /* remove Node data 2 - last Node of the Linked List */
        listIndexPos.remove(2);
        assertEquals("MyLinkedList{head=Node{dataNode=3, linkNode=Node{dataNode=9, linkNode=Node{dataNode=5, linkNode=Node{dataNode=1, linkNode=null}}}}}", listIndexPos.toString());

        /* remove Node data 1 - last Node of the Linked List */
        listIndexPos.remove(1);
        assertEquals("MyLinkedList{head=Node{dataNode=3, linkNode=Node{dataNode=9, linkNode=Node{dataNode=5, linkNode=null}}}}", listIndexPos.toString());

        listIndexPos.remove();
        assertEquals("MyLinkedList{head=Node{dataNode=3, linkNode=Node{dataNode=9, linkNode=null}}}", listIndexPos.toString());

        listIndexPos.removeNodeFirstPosition();
        assertEquals("MyLinkedList{head=Node{dataNode=9, linkNode=null}}", listIndexPos.toString());

        /* remove an empty linked list */
        MyLinkedList listFirstPosEmpty = new MyLinkedList();
        listFirstPosEmpty.removeNodeFirstPosition();
        assertEquals("MyLinkedList{head=null}", listFirstPosEmpty.toString());

    }

    @Test
    public void testUpdateNodeData() {
        /* add a new element into the list - first position in the list */
        MyLinkedList listIndexPos = new MyLinkedList();

        listIndexPos.addNodeFirstPosition(1);
        listIndexPos.addNodeFirstPosition(2);
        listIndexPos.addNodeFirstPosition(3);
        assertEquals("MyLinkedList{head=Node{dataNode=3, linkNode=Node{dataNode=2, linkNode=Node{dataNode=1, linkNode=null}}}}", listIndexPos.toString());

        /* add a new Node with data 5 into the list - after Node with data 2 */
        listIndexPos.add(2, 5);
        assertEquals("MyLinkedList{head=Node{dataNode=3, linkNode=Node{dataNode=2, linkNode=Node{dataNode=5, linkNode=Node{dataNode=1, linkNode=null}}}}}", listIndexPos.toString());

        /* add a new Node with data 9 into the list - after Node with data 3 */
        listIndexPos.add(3, 9);
        assertEquals("MyLinkedList{head=Node{dataNode=3, linkNode=Node{dataNode=9, linkNode=Node{dataNode=2, linkNode=Node{dataNode=5, linkNode=Node{dataNode=1, linkNode=null}}}}}}", listIndexPos.toString());

        /* update the Node which has data 9 with the new data 10 */
        listIndexPos.updateNodeData(9, 10);
        assertEquals("MyLinkedList{head=Node{dataNode=3, linkNode=Node{dataNode=10, linkNode=Node{dataNode=2, linkNode=Node{dataNode=5, linkNode=Node{dataNode=1, linkNode=null}}}}}}", listIndexPos.toString());

        /* update the Node which has data 3 with the new data 21 - first Node in the Linked List */
        listIndexPos.updateNodeData(3, 21);
        assertEquals("MyLinkedList{head=Node{dataNode=21, linkNode=Node{dataNode=10, linkNode=Node{dataNode=2, linkNode=Node{dataNode=5, linkNode=Node{dataNode=1, linkNode=null}}}}}}", listIndexPos.toString());

        /* update the Node which has data 1 with the new data 101 - last Node in the Linked List */
        listIndexPos.updateNodeData(1, 101);
        assertEquals("MyLinkedList{head=Node{dataNode=21, linkNode=Node{dataNode=10, linkNode=Node{dataNode=2, linkNode=Node{dataNode=5, linkNode=Node{dataNode=101, linkNode=null}}}}}}", listIndexPos.toString());
    }

}
