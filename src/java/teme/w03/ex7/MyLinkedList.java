package teme.w03.ex7;

/**
 * A linked list is a linear collection of data elements, whose order is not given by their physical
 * placement in memory. Instead, each element points to the next.
 * It is a data structure consisting of a collection of nodes which together represent a sequence .
 * In its most basic form, each node contains: data , and a reference (in other words, a link) to the
 * next node in the sequence.
 */

public class MyLinkedList {

    private Node head;           // reference to the first node

    public static void main(String[] args) {
        MyLinkedList listLastPos = new MyLinkedList();

        /* add a new element into the list - at the end */
        listLastPos.add(1);
        listLastPos.add(2);
        listLastPos.add(3);
        System.out.println("addNodeLastPosition call: " + listLastPos.toString());

        /* add a new element into the list - first position in the list */
        MyLinkedList listFirstPos = new MyLinkedList();

        listFirstPos.addNodeFirstPosition(1);
        listFirstPos.addNodeFirstPosition(2);
        listFirstPos.addNodeFirstPosition(3);
        System.out.println("addNodeFirstPosition call: " + listFirstPos.toString());

        listFirstPos.add(2, 5);
        System.out.println("addNodeIndexPosition call: " + listFirstPos.toString());

        listFirstPos.add(3, 9);
        System.out.println("addNodeIndexPosition call: " + listFirstPos.toString());

        listFirstPos.iterateList();
        System.out.println("iterateList call: " + listFirstPos.toString());

        listFirstPos.remove(2);
        System.out.println("removeNodeIndexPosition call: " + listFirstPos.toString());

        listFirstPos.remove();
        System.out.println("removeNodeLastPosition call: " + listFirstPos.toString());

        listFirstPos.removeNodeFirstPosition();
        System.out.println("removeNodeFirstPosition call: " + listFirstPos.toString());

        listFirstPos.updateNodeData(9, 10);
        System.out.println("updateNodeData call: " + listFirstPos.toString());
    }

    @Override
    public String toString() {
        return "MyLinkedList{" +
                "head=" + head +
                '}';
    }

    /*
    ---------------------------------------------------------------------------------------------------
    ----------------------------------------------- ADD CASE ------------------------------------------
    ---------------------------------------------------------------------------------------------------
    */

    /* iterate over the elements */
    public void iterateList() {
        if (head != null) { // the Linked List shall not be empty
            /*
             A Linked List is typically represented by the head of it, we have to traverse the list till end
              */
            Node last = head;   // the last Node is the item which will iterate over all the items Node of the linked list
            while (last.getLinkNode() != null) {
                last = last.getLinkNode();
            }
        }
    }

    /* add a new element in the last position of the list */
    public void add(int data) {
        Node newNode = new Node(data);
        // there is no Node (empty list)
        if (head == null) {
            head = new Node(data);
            return;
        } else {
            /*
             A Linked List is typically represented by the head of it,
             we have to traverse the list till end and then change the next of last node to new node
              */
            Node last = head;   // the last Node is the item which will iterate over all the items Node of the linked list
            while (last.getLinkNode() != null) {
                // the new list will contain the link content of the previous list item
                last = last.getLinkNode();
            }
            newNode.setLinkNode(null);  // this node will be the last node, so the link will be null
            last.setLinkNode(newNode);  // change the link of the last node
            return;
        }
    }

    /* add a new element after a specified Node of the list */
    public void add(int dataBefore, int data) {
        Node newNode = new Node(data);
        // there is no Node (empty list)
        if (head == null) {
            head = new Node(data);
            return;
        } else {
            /*
             A Linked List is typically represented by the head of it,
             we have to traverse the list till end and add the new Node data after a predefined Node introduced as method parameter
              */
            Node last = head;   // the last Node is the item which will iterate over all the items Node of the linked list
            boolean isFound = false;
            while (last.getLinkNode() != null) {
                // the new list will contain the link content of the previous list item
                if (dataBefore == last.getDataNode()) {
                    Node tmp = last.getLinkNode();  // temporary Node where the link of current last link information is stored
                    newNode.setLinkNode(tmp);       // newNode will be linked to the previous temporary link Node info
                    last.setLinkNode(newNode);
                    isFound = true;
                }
                last = last.getLinkNode();
            }
            if (!isFound && last.getLinkNode() == null)   // in case the Node before the the New added Node is the last element of the Linked List
            {
                last.setLinkNode(newNode);
            }
            return;
        }
    }


    /*
    ---------------------------------------------------------------------------------------------------
    -------------------------------------------- REMOVE CASE ------------------------------------------
    ---------------------------------------------------------------------------------------------------
     */

    /* add a new element in the first position of the list */
    public void addNodeFirstPosition(int data) {
        Node newNode = new Node(data);
        // there is no Node (empty list)
        if (head == null) {
            head = new Node(data);
            return;
        } else {
            /*
             A Linked List is typically represented by the head of it
              */
            newNode.setLinkNode(head);  // this node will be the first node, so the link will be the previous head Node
            head = newNode;             // head Node will have the link of the new newNode Node
            return;
        }
    }

    /* remove an existing element */
    public void removeNodeFirstPosition() {
        if (head != null) { // the Linked List shall not be empty
            /*
             A Linked List is typically represented by the head of it,
             we have to traverse the list till end and remove the first Node
              */
            Node last = head;   // the last Node is the item which will iterate over all the items Node of the linked list
            head = last.getLinkNode();
        }
    }

    /* remove an existing element using its data information */
    public void remove(int data) {
        // there is no Node (empty list)
        if (head != null) {
            /*
             A Linked List is typically represented by the head of it,
             we have to traverse the list till end and remove the Node using the Node data introduced as method parameter
              */
            Node last = head;   // the last Node is the item which will iterate over all the items Node of the linked list
            while (last.getLinkNode() != null) {
                Node tmp = last.getLinkNode();      // tmp = link content of the Node which shall be deleted
                if (tmp.getDataNode() == data && tmp.getLinkNode() != null) {    // read the Node data to check if it is the data to be deleted (not the last node)
                    Node tmp1 = tmp.getLinkNode();  // store into a temporary node the link of the Node which shall be deleted
                    last.setLinkNode(tmp1);         // Node link (Node before the Node to be deleted) = Node link (after the Node to be deleted)
                    last = last.getLinkNode();
                } else if (tmp.getDataNode() == data && tmp.getLinkNode() == null) {    // if the last Node has link null (last element of the linked list)
                    last.setLinkNode(null);         // Node link (Node before the Node to be deleted) = Node link (after the Node to be deleted)
                } else {
                    last = last.getLinkNode();
                }
            }
        }
    }

    /* remove the last element */
    public void remove() {
        if (head != null) {     // the Linked List shall not be empty
            /*
             A Linked List is typically represented by the head of it,
             we have to traverse the list till end and remove the last Node from the Linked List
              */
            Node last = head;   // the last Node is the item which will iterate over all the items Node of the linked list
            while (last.getLinkNode() != null) {
                Node tmp = last.getLinkNode();      // tmp = link content of the next Node
                if (tmp.getLinkNode() == null) {    // if the last Node has link null (last element of the linked list)
                    last.setLinkNode(null);         // Node link (Node before the Node to be deleted) = Node link (after the Node to be deleted)
                } else {
                    last = last.getLinkNode();
                }
            }
        }
    }

    /* update the data stored in an element */
    public void updateNodeData(int currentDataNode, int newDataNode) {
        boolean isFound = false;
        // if the Linked list is not empty
        if (head != null) {
            /*
             A Linked List is typically represented by the head of it,
             we have to traverse the list till end and update de Node data after a predefined Node and data introduced as method parameter
              */
            Node last = head;   // the last Node is the item which will iterate over all the items Node of the linked list
            while (last.getLinkNode() != null) {
                if (last.getDataNode() == currentDataNode) {     // if the current Node data equal to the Node data introduced as method parameter
                    last.setDataNode(newDataNode);     // set current Node data to data introduced as method parameter
                    isFound = true;
                }
                last = last.getLinkNode();
            }
            if (!isFound && last.getDataNode() == currentDataNode)   // in case the Node data to be replaced is the last element of the Linked List
            {
                last.setDataNode(newDataNode);
            }
        }
    }
}
