package teme.w03.ex6;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Unit tests for MyArrayList class (should compile and pass all tests after you complete that one first)
 */
public class MyArrayListTests {

    @Test
    public void testAdd() {
        MyArrayList ar1 = new MyArrayList(new int[]{1, 2, 3, 4, 5, 6});
        ar1.add(7);
        assertEquals(new MyArrayList(new int[]{1, 2, 3, 4, 5, 6, 7}).toString(), ar1.toString());

        MyArrayList ar2 = new MyArrayList(new int[]{});
        ar2.add(12);
        assertEquals(new MyArrayList(new int[]{12}).toString(), ar2.toString());

    }

    @Test
    public void testRemove() {
        MyArrayList ar1 = new MyArrayList(new int[]{1, 2, 3, 4, 5, 6});
        ar1.remove();
        assertEquals(new MyArrayList(new int[]{1, 2, 3, 4, 5}).toString(), ar1.toString());

        MyArrayList ar2 = new MyArrayList(new int[]{1});
        ar2.remove();
        assertEquals(new MyArrayList(new int[]{}).toString(), ar2.toString());

        MyArrayList ar3 = new MyArrayList(new int[]{});
        ar3.remove();
        assertEquals(new MyArrayList(new int[]{}).toString(), ar3.toString());
    }

    @Test
    public void testSize() {
        MyArrayList ar1 = new MyArrayList(new int[]{1, 2, 3, 4, 5, 6});
        assertEquals(6, ar1.size());

        MyArrayList ar2 = new MyArrayList(new int[]{});
        assertEquals(0, ar2.size());

        MyArrayList ar3 = new MyArrayList(new int[]{0});
        assertEquals(1, ar3.size());
    }

    @Test
    public void testGet() {
        MyArrayList ar1 = new MyArrayList(new int[]{1, 2, 3, 4, 5, 6});
        assertEquals(-1, ar1.get(-1));
        assertEquals(1, ar1.get(0));
        assertEquals(2, ar1.get(1));
        assertEquals(6, ar1.get(5));
        assertEquals(-1, ar1.get(6));

        MyArrayList ar2 = new MyArrayList(new int[]{});
        assertEquals(-1, ar2.get(0));
    }

    @Test
    public void testRemoveIndex() {
        MyArrayList ar1 = new MyArrayList(new int[]{1, 2, 3, 4, 5, 6});
        ar1.removeIndex(1);
        assertEquals(new MyArrayList(new int[]{1, 3, 4, 5, 6}).toString(), ar1.toString());
        ar1.removeIndex(0);
        assertEquals(new MyArrayList(new int[]{3, 4, 5, 6}).toString(), ar1.toString());
        ar1.removeIndex(-1);
        assertEquals(new MyArrayList(new int[]{3, 4, 5, 6}).toString(), ar1.toString());
        ar1.removeIndex(10);
        assertEquals(new MyArrayList(new int[]{3, 4, 5, 6}).toString(), ar1.toString());
        MyArrayList ar2 = new MyArrayList(new int[]{});
        ar2.removeIndex(10);
        assertEquals(new MyArrayList(new int[]{}).toString(), ar2.toString());
    }

    @Test
    public void testAddIndex() {
        MyArrayList ar1 = new MyArrayList(new int[]{1, 2, 3, 4, 5, 6});
        ar1.addIndex(12, 1);
        assertEquals(new MyArrayList(new int[]{1, 12, 2, 3, 4, 5, 6}).toString(), ar1.toString());
        ar1.addIndex(9, 0);
        assertEquals(new MyArrayList(new int[]{9, 1, 12, 2, 3, 4, 5, 6}).toString(), ar1.toString());
        ar1.addIndex(10, -1);
        assertEquals(new MyArrayList(new int[]{9, 1, 12, 2, 3, 4, 5, 6}).toString(), ar1.toString());
        ar1.addIndex(20, 10);
        assertEquals(new MyArrayList(new int[]{9, 1, 12, 2, 3, 4, 5, 6}).toString(), ar1.toString());
        MyArrayList ar2 = new MyArrayList(new int[]{});
        ar2.addIndex(13, 10);
        assertEquals(new MyArrayList(new int[]{}).toString(), ar2.toString());
    }

}
