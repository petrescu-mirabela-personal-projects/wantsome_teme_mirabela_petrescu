package teme.w04.ex4;

public class Node {
    private int dataNode;
    private Node linkNode;

    /* each node has a data and no link --> considered the last node in the LinkedList */
    public Node(int dataNode) {
        this.dataNode = dataNode;
        linkNode = null;
    }

//    @Override
//    public String toString() {
//        return "Node{" +
//                "dataNode=" + dataNode +
//                ", linkNode=" + linkNode +
//                '}';
//    }

    @Override
    public String toString() {
        return (dataNode + " ").trim();
    }

    public Node getLinkNode() {
        return linkNode;
    }

    public void setLinkNode(Node linkNode) {
        this.linkNode = linkNode;
    }

    public int getDataNode() {
        return dataNode;
    }

    public void setDataNode(int dataNode) {
        this.dataNode = dataNode;
    }
}
