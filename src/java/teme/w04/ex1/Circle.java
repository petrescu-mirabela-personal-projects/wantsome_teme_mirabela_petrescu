package teme.w04.ex1;

    /*
    a) Define a Circle class (fields: centerX/Y,radius; methods: area(), length(), toString(), getters)
    - hint: circle area = PI*R^2, length = 2*PI*R ; may use Math.PI, Math.pow.. if needed
    - question: can you make the fields final? (can you think of any benefits it might bring?) What about the methods and/or the class itself?
     */

class Circle {      // if class is declared final, it is not wouldn't allow to extend this class
    private final double centerX;   // visible in Circle class and its subclass CylinderH
    private final double centerY;   // visible in Circle class and its subclass CylinderH
    private final double radius;    // visible in Circle class and its subclass CylinderH

    public Circle(double centerX, double centerY, double radius) {
        this.centerX = centerX;
        this.centerY = centerY;
        this.radius = radius;
    }

    public double getCenterX() {
        return centerX;
    }

    public double getCenterY() {
        return centerY;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "centerX=" + centerX +
                ", centerY=" + centerY +
                ", radius=" + radius +
                '}';
    }

    public double getRadius() {
        return radius;
    }

    // final class - it will not be overridden by its subclass
    public double area() {
        return Math.PI * Math.pow(radius, 2);
    }

    // final class - it will not be overridden by its subclass
    public double length() {
        return 2 * Math.PI * radius;
    }
}
