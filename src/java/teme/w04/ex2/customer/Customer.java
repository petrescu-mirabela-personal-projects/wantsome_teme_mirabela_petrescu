package teme.w04.ex2.customer;

public class Customer extends Address {
    private final String firstName;
    private final String lastName;
    private final String cnp;

    private Customer(String firstName, String lastName, String cnp, String street, int streetNumber, String town) {
        super(street, streetNumber, town);
        this.firstName = firstName;
        this.lastName = lastName;
        this.cnp = cnp;
    }

    public Customer(String firstName, String lastName, String cnp, Address address) {
        this(firstName, lastName, cnp, address.street, address.streetNumber, address.town);
    }

    @Override
    public String toString() {
        return "---- Customer info ---- \n{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", cnp='" + cnp + '\'' +
                ", " + super.toString();
    }
}
