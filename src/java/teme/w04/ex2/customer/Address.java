package teme.w04.ex2.customer;

public class Address {
    String street;
    int streetNumber;
    String town;

    Address() {
    }

    public Address(String street, int streetNumber, String town) {
        this.street = street;
        this.streetNumber = streetNumber;
        this.town = town;
    }

    @Override
    public String toString() {
        return "Customer address{" +
                "street='" + street + '\'' +
                ", streetNumber=" + streetNumber +
                ", town='" + town + '\'' +
                '}';
    }
}
