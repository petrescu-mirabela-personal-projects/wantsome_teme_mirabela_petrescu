package teme.w04.ex2.product;

public class Product {
    private final int id;
    private final String name;
    private final String type;    // ex. clothing, electronics
    private final double price;
    private final String color;

    public Product(int id, String name, String type, double price, String color) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.price = price;
        this.color = color;
    }

    public double getPrice() {
        return price;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "\nProduct{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", price=" + price +
                ", color='" + color + '\'' +
                '}';
    }
}
