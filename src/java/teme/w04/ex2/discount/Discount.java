package teme.w04.ex2.discount;

public interface Discount {
    // Discount can be also an abstract class
    // FixedDiscount and PercentageDiscount will extend only the Discount class
    // and not the Product class because in this case these 2 classes returns only the discount, not the price after the discount

    /* method shall return the price (total price) after the discount is applied
    method parameter totalPrice is the total price before the discount is applied
     */
    double priceWithDiscount(double totalPrice);

}
