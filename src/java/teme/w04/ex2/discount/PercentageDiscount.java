package teme.w04.ex2.discount;

import java.util.Objects;

/*
    Use inheritance to somehow define/support these two different discount types.
 */
public class PercentageDiscount implements Discount {
    private final double percent;    // what percentage of the price should be discounted (e.g 4% = 0.04)

    public PercentageDiscount(double percent) {
        this.percent = percent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PercentageDiscount)) return false;
        PercentageDiscount that = (PercentageDiscount) o;
        return Double.compare(that.percent, percent) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(percent);
    }

    @Override
    public String toString() {
        return "PercentageDiscount{" +
                "percent = " + percent +
                '}';
    }

    /* method shall return the price (total price) after the discount is applied
    method parameter totalPrice is the total price before the discount is applied
     */
    @Override
    public double priceWithDiscount(double totalPriceBeforeDiscount) {
        double totalPriceWithDiscount = 0;
        //if (new PercentageDiscount(percent).equals(percent)) {
        if (percent != 0) {
            totalPriceWithDiscount = totalPriceBeforeDiscount * percent / 100;
        }
        return totalPriceWithDiscount;
    }
}
