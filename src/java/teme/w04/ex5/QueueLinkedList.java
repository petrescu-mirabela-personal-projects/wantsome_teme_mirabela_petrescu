package teme.w04.ex5;

/*
     Based on the solved problem from previous week for MyLinkedList, implement these classes
     based/derived from that class (somehow - inherit from it if possible, or make that one more
     general, or just copy part of the code, etc..):
     Doubly linked list :
         ● each element has two links - one to the next and one to the previous node
     Stack - a linked list with the following restrictions:
         ● adding new nodes only at the head of the list
         ● remove nodes only from the head of the list
         ● can access only the element from the head of the list
     Queue - a linked list with the following restrictions:
         ● adding new nodes only at the tail (end) of the list
         ● remove nodes only from the tail of the list
         ● can access only the element from the tail of the list
     Implement these specializations of a linked list in such a way that you take advantage of OOP
     features like inheritance and polymorphism.
 */

public class QueueLinkedList implements Tail {

    private Node tail;           // reference to the last node

    public QueueLinkedList() {
        tail = null;
    }

    @Override
    public String toString() {
        Node node = tail;
        int[] listArray = new int[countNodes()];
        // the result shall be printed in a  reverse order because we start to read the data from the tail
        int i = countNodes() - 1;
        StringBuilder str = new StringBuilder();
        while (node != null) {
            listArray[i--] = node.getDataNode();
            node = node.getPrevLinkNode();  // read from the right (from the tail) to the left side
        }
        for (int item : listArray) {
            str.append(" ").append(item);
        }
        return str.toString().trim();
    }

    /**
     * Counts the number of nodes in the list.
     */
    private int countNodes() {
        Node cn = tail;
        int count = 0;
        while (cn != null) {
            count++;
            cn = cn.getPrevLinkNode();
        }
        return count;
    }

    /* iterate over the elements
     can access only the element from the tail of the list */
    @Override
    public void iterateList() {
        Node currentNode = tail;
        // there is no Node (empty list)
        // iterate until the last node --> currentNode != null is false
        while (currentNode != null) {
            currentNode = currentNode.getPrevLinkNode();    /* iterate over the elements */
        }
    }

    /*
    ---------------------------------------------------------------------------------------------------
    ----------------------------------------------- ADD CASE ------------------------------------------
    ---------------------------------------------------------------------------------------------------
    */

    /* adding new nodes only at the tail (end) of the list */
    @Override
    public void addNodeLastPosition(int data) {
        Node newNode = new Node(tail, data, null);
        // there is no Node (empty list)
        if (tail == null) {
            tail = newNode;
        } else {
            /*
             A Queue Linked List is typically represented by the tail of it
              */
            tail.setNextLinkNode(newNode);  // last node in the list will have the link to the new introduced Node
            tail = newNode;     // the new tail of the list is now the newNode,(where its next Link is null from the instantiation)
        }
    }



    /*
    ---------------------------------------------------------------------------------------------------
    -------------------------------------------- REMOVE CASE ------------------------------------------
    ---------------------------------------------------------------------------------------------------
     */


    /* remove nodes only from the tail of the list */
    @Override
    public void removeNodeLastPosition() {
        if (tail != null) {     // the Linked List shall not be empty
            if (tail.getPrevLinkNode() != null) {
                tail = tail.getPrevLinkNode();    // tail will have the content of the tail prev link
                tail.setNextLinkNode(null);     // next link of the new tail will be now null, since will become the new tail
            } else {
                tail = null;    // the initial list had only one node
            }
        }
    }

    /* update the data stored in an element */
    public void updateNodeData(int data) {
        Node currentNode = tail;
        // there is no Node (empty list)
        if (currentNode != null) {
            currentNode.setDataNode(data);
        }
    }
}

