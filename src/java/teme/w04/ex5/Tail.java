package teme.w04.ex5;

interface Tail {
    String toString();

    void iterateList();

    void addNodeLastPosition(int data);

    void removeNodeLastPosition();
}
