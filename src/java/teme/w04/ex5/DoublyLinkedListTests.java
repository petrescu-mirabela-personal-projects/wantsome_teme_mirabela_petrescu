package teme.w04.ex5;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Unit tests for MyArrayList class (should compile and pass all tests after you complete that one first)
 */
public class DoublyLinkedListTests {

    @Test
    public void testAddNodeLastPosition() {
        /* add a new element into the list - at the end */
        DoublyLinkedList listLastPos = new DoublyLinkedList();

        listLastPos.addNodeLastPosition(1);
        assertEquals("1", listLastPos.toString());

        listLastPos.addNodeLastPosition(2);
        assertEquals("1 2", listLastPos.toString());

        listLastPos.addNodeLastPosition(3);
        assertEquals("1 2 3", listLastPos.toString());
    }

    @Test
    public void testAddNodeLastPosition02() {
        /* add a new element into the list - at the end */
        QueueLinkedList listLastPos = new QueueLinkedList();

        listLastPos.addNodeLastPosition(1);
        assertEquals("1", listLastPos.toString());

        listLastPos.addNodeLastPosition(2);
        assertEquals("1 2", listLastPos.toString());

        listLastPos.addNodeLastPosition(3);
        assertEquals("1 2 3", listLastPos.toString());
    }

    @Test
    public void testAddNodeFirstPosition() {
        /* add a new element into the list - first position in the list */
        DoublyLinkedList listFirstPos = new DoublyLinkedList();

        listFirstPos.addNodeFirstPosition(1);
        assertEquals("1", listFirstPos.toString());

        listFirstPos.addNodeFirstPosition(2);
        assertEquals("2 1", listFirstPos.toString());

        listFirstPos.addNodeFirstPosition(3);
        assertEquals("3 2 1", listFirstPos.toString());
    }

    @Test
    public void testAddNodeFirstPosition02() {
        /* add a new element into the list - first position in the list */
        StackLinkedList listFirstPos = new StackLinkedList();

        listFirstPos.addNodeFirstPosition(1);
        assertEquals("1", listFirstPos.toString());

        listFirstPos.addNodeFirstPosition(2);
        assertEquals("2 1", listFirstPos.toString());

        listFirstPos.addNodeFirstPosition(3);
        assertEquals("3 2 1", listFirstPos.toString());
    }

    @Test
    public void testAddNodeAfterNodeData() {
        /* add a new element into the list - first position in the list */
        DoublyLinkedList listIndexPos = new DoublyLinkedList();

        listIndexPos.addNodeFirstPosition(1);
        listIndexPos.addNodeFirstPosition(2);
        listIndexPos.addNodeFirstPosition(3);
        assertEquals("3 2 1", listIndexPos.toString());

        /* add a new Node with data 5 into the list - after Node with data 3 */
        listIndexPos.addNodeAfterNodeData(new Node(3), 5);
        assertEquals("3 5 2 1", listIndexPos.toString());

        /* add a new Node with data 9 into the list - after Node with data 1 */
        listIndexPos.addNodeAfterNodeData(new Node(1), 9);
        assertEquals("3 5 2 1 9", listIndexPos.toString());

        /* add a new Node with data 9 into the list - after Node with data 3 */
        listIndexPos.addNodeAfterNodeData(new Node(5), 9);
        assertEquals("3 5 9 2 1 9", listIndexPos.toString());
    }

    @Test
    public void testIterateList() {
        /* add a new element into the list - first position in the list */
        DoublyLinkedList listIndexPos = new DoublyLinkedList();

        listIndexPos.addNodeFirstPosition(1);
        listIndexPos.addNodeFirstPosition(2);
        listIndexPos.addNodeFirstPosition(3);
        assertEquals("3 2 1", listIndexPos.toString());

        /* iterate till the end of the list */
        listIndexPos.iterateList();
        assertEquals("3 2 1", listIndexPos.toString());

        /* iterate an empty list */
        DoublyLinkedList listIndexPosEmpty = new DoublyLinkedList();
        listIndexPosEmpty.iterateList();
        assertEquals("", listIndexPosEmpty.toString());
    }

    @Test
    public void testRemoveNodeData() {
        /* add a new element into the list - first position in the list */
        DoublyLinkedList listIndexPos = new DoublyLinkedList();

        listIndexPos.addNodeFirstPosition(1);
        listIndexPos.addNodeFirstPosition(2);
        listIndexPos.addNodeFirstPosition(3);
        assertEquals("3 2 1", listIndexPos.toString());

        listIndexPos.removeNodeData(2);
        assertEquals("3 1", listIndexPos.toString());
        listIndexPos.removeNodeData(1);
        assertEquals("3", listIndexPos.toString());

        /* remove a node which is not in the linked list */
        listIndexPos.removeNodeData(50);
        assertEquals("3", listIndexPos.toString());

        listIndexPos.removeNodeData(3);
        assertEquals("", listIndexPos.toString());
    }


    @Test
    public void testRemoveNodeLastPosition() {
        /* add a new element into the list - first position in the list */
        DoublyLinkedList listLastPos = new DoublyLinkedList();
        listLastPos.addNodeFirstPosition(1);
        listLastPos.addNodeFirstPosition(2);
        listLastPos.addNodeFirstPosition(3);
        assertEquals("3 2 1", listLastPos.toString());

        listLastPos.removeNodeLastPosition();
        assertEquals("3 2", listLastPos.toString());

        listLastPos.removeNodeLastPosition();
        assertEquals("3", listLastPos.toString());

        listLastPos.removeNodeLastPosition();
        assertEquals("", listLastPos.toString());

        /* remove an empty linked list */
        listLastPos.removeNodeFirstPosition();
        assertEquals("", listLastPos.toString());
    }

    @Test
    public void testRemoveNodeLastPosition02() {
        /* add a new element into the list - first position in the list */
        QueueLinkedList listLastPos = new QueueLinkedList();
        listLastPos.addNodeLastPosition(1);
        listLastPos.addNodeLastPosition(2);
        listLastPos.addNodeLastPosition(3);
        assertEquals("1 2 3", listLastPos.toString());

        listLastPos.removeNodeLastPosition();
        assertEquals("1 2", listLastPos.toString());

        listLastPos.removeNodeLastPosition();
        assertEquals("1", listLastPos.toString());

        listLastPos.removeNodeLastPosition();
        assertEquals("", listLastPos.toString());

        /* remove an empty linked list */
        listLastPos.removeNodeLastPosition();
        assertEquals("", listLastPos.toString());
    }

    @Test
    public void testRemoveNodeFirstPosition() {
        /* add a new element into the list - first position in the list */
        DoublyLinkedList listFirstPosEmpty = new DoublyLinkedList();

        listFirstPosEmpty.addNodeFirstPosition(1);
        listFirstPosEmpty.addNodeFirstPosition(2);
        listFirstPosEmpty.addNodeFirstPosition(3);
        assertEquals("3 2 1", listFirstPosEmpty.toString());

        listFirstPosEmpty.removeNodeFirstPosition();
        assertEquals("2 1", listFirstPosEmpty.toString());

        listFirstPosEmpty.removeNodeFirstPosition();
        assertEquals("1", listFirstPosEmpty.toString());

        listFirstPosEmpty.removeNodeFirstPosition();
        assertEquals("", listFirstPosEmpty.toString());

        /* remove an empty linked list */
        listFirstPosEmpty.removeNodeFirstPosition();
        assertEquals("", listFirstPosEmpty.toString());

    }

    @Test
    public void testUpdateNodeData() {
        /* add a new element into the list - first position in the list */
        DoublyLinkedList listIndexPos = new DoublyLinkedList();

        listIndexPos.addNodeFirstPosition(1);
        listIndexPos.addNodeFirstPosition(2);
        listIndexPos.addNodeFirstPosition(3);
        assertEquals("3 2 1", listIndexPos.toString());

        /* update the Node which has data 2 with the new data 10 */
        listIndexPos.updateNodeData(2, 10);
        assertEquals("3 10 1", listIndexPos.toString());

        /* update the Node which has data 3 with the new data 21 - first Node in the Linked List */
        listIndexPos.updateNodeData(3, 21);
        assertEquals("21 10 1", listIndexPos.toString());

        /* update the Node which has data 1 with the new data 101 - last Node in the Linked List */
        listIndexPos.updateNodeData(1, 101);
        assertEquals("21 10 101", listIndexPos.toString());
    }

    /* Test the Head interface */
    @Test
    public void testHeadInterface() {
        runTestHead(new DoublyLinkedList());
        runTestHead(new StackLinkedList());
    }

    private void runTestHead(Head head) {
        head.addNodeFirstPosition(1);
        head.addNodeFirstPosition(2);
        head.addNodeFirstPosition(3);
        assertEquals("3 2 1", head.toString());
        head.removeNodeFirstPosition();
        assertEquals("2 1", head.toString());
        head.removeNodeFirstPosition();
        assertEquals("1", head.toString());
        head.removeNodeFirstPosition();
        assertEquals("", head.toString());
        head.removeNodeFirstPosition();
        assertEquals("", head.toString());
    }

    /* Test the Tail interface */
    @Test
    public void testTailInterface() {
        runTestTail(new DoublyLinkedList());
        runTestTail(new QueueLinkedList());
    }

    private void runTestTail(Tail tail) {
        tail.addNodeLastPosition(1);
        assertEquals("1", tail.toString());
        tail.addNodeLastPosition(2);
        assertEquals("1 2", tail.toString());
        tail.addNodeLastPosition(3);
        assertEquals("1 2 3", tail.toString());


        tail.removeNodeLastPosition();
        assertEquals("1 2", tail.toString());
        tail.removeNodeLastPosition();
        assertEquals("1", tail.toString());
        tail.removeNodeLastPosition();
        assertEquals("", tail.toString());
        tail.removeNodeLastPosition();
        assertEquals("", tail.toString());
    }


}
