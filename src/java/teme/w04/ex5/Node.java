package teme.w04.ex5;

public class Node {
    private int dataNode;
    private Node prevLinkNode;
    private Node nextLinkNode;

    public Node() {
        this(null, 0, null);
    }

    public Node(Node prevLinkNode, int dataNode, Node nextLinkNode) {
        this.dataNode = dataNode;
        this.prevLinkNode = prevLinkNode;
        this.nextLinkNode = nextLinkNode;
    }

    /* each node has a data and no link --> considered the last node in the LinkedList */
    public Node(int dataNode) {
        this(null, dataNode, null);
    }

    public int getDataNode() {
        return dataNode;
    }

    public void setDataNode(int dataNode) {
        this.dataNode = dataNode;
    }

    public Node getPrevLinkNode() {
        return prevLinkNode;
    }

    public void setPrevLinkNode(Node prevLinkNode) {
        this.prevLinkNode = prevLinkNode;
    }

    public Node getNextLinkNode() {
        return nextLinkNode;
    }

    public void setNextLinkNode(Node nextLinkNode) {
        this.nextLinkNode = nextLinkNode;
    }

    @Override
    public String toString() {
        return dataNode + "";
    }
}

