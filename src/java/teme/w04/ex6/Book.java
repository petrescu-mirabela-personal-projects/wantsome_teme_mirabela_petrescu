package teme.w04.ex6;

/* A class called Book is designed as shown in the class diagram. It contains:
Four private instance variables: name (String), author (of the class Author you have just created, assume that each book has one and only one author), price (double), and qtyInStock (int);
 */
public class Book {
    private final String name;
    private final Author author;
    private final double price;
    private int qtyInStock = 0;

    private Book(String name, Author author, double price) {
        this.name = name;
        this.author = author;
        this.price = price;
    }

    private Book(String name, Author author, double price, int qtyInStock) {
        this(name, author, price);
        this.qtyInStock = qtyInStock;
    }

    public static void main(String[] args) {
        Author anAuthor = new Author("Tan Ah Teck", "ahteck@somewhere.com", 'm');
        System.out.println(anAuthor);   // call toString()
        anAuthor.setEmail("paul@nowhere.com");
        System.out.println(anAuthor);


        Book aBook = new Book("Java for dummy", anAuthor, 19.95, 1000);
        // Printing the name and email of the author from a Book instance.
        System.out.println();
        System.out.println();
        System.out.println("--------------------------------------------------------------------------------------- ");
        System.out.println("------------ Printing the name and email of the author from a Book instance ---------- ");
        System.out.println("Name of the author from a book instance: " + aBook.getAuthor().getName());
        System.out.println("Email of the author from a book instance: " + aBook.getAuthor().getEmail());

        System.out.println("------------ Printing the name, email and gender of the author from a Book instance - using defined methods ---------- ");
        System.out.println("Name of the author from a book instance: " + aBook.getAuthorName());
        System.out.println("Gender of the author from a book instance: " + aBook.getAuthorGender());
        System.out.println("Email of the author from a book instance: " + aBook.getAuthorEmail());


        // Use an anonymous instance of Author
        System.out.println();
        System.out.println();
        System.out.println("-------------------------------------------------------------------------------------- ");
        System.out.println("------------ Anonymous instance of Author -------------------------------------------- ");
        System.out.println("-------------------------------------------------------------------------------------- ");
        Book anotherBook = new Book("more Java for dummy", new Author("Petrescu Mirabela", "abcd@somewhere1.com", 'f'), 32.14, 412);

        // Printing the name and email of the author from a Book instance.
        System.out.println("------------ Printing the name and email of the author from a Book instance ---------- ");
        System.out.println("Name of the author from a book instance: " + anotherBook.getAuthor().getName());
        System.out.println("Email of the author from a book instance: " + anotherBook.getAuthor().getEmail());

        System.out.println("------------ Printing the name, email and gender of the author from a Book instance - using defined methods ---------- ");
        System.out.println("Name of the author from a book instance: " + anotherBook.getAuthorName());
        System.out.println("Gender of the author from a book instance: " + anotherBook.getAuthorGender());
        System.out.println("Email of the author from a book instance: " + anotherBook.getAuthorEmail());

    }

    private Author getAuthor() {
        return author;
    }

    @Override
    public String toString() {
        return "Book{" +
                "name='" + name + '\'' +
                ", author=" + author +
                ", price=" + price +
                ", qtyInStock=" + qtyInStock +
                '}';
    }

    /*
    Introduce new methods called getAuthorName(), getAuthorEmail(), getAuthorGender()
    in the Book class to return the name, email and gender of the author of the book. For example,
     */
    private String getAuthorName() {
        return getAuthor().getName();
    }

    private String getAuthorEmail() {
        return getAuthor().getEmail();
    }

    private char getAuthorGender() {
        return getAuthor().getGender();
    }
}
