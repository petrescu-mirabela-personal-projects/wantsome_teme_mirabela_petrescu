package teme.w04.ex3;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Unit tests for Complex class (should compile and pass all tests after you complete that first)
 */
public class Complex_Tests {

    @Test
    public void testComplexOperations() {

        //create some instances of Complex class (using the static .complex() method)
        Complex c0 = Complex.complex(0, 0);
        Complex c1 = Complex.complex(1, 2);
        Complex c2 = Complex.complex(3, 4);

        //check some expected results
        assertEquals(Complex.complex(-1, -2), c1.negate());
        assertEquals(Complex.complex(4, 6), c1.add(c2));
        assertEquals(Complex.complex(-5, 10), c1.multiply(c2));

        //check some generic arithmetic rules
        assertEquals(c1.add(c2), c2.add(c1));           //add is commutative
        assertEquals(c1.multiply(c2), c2.multiply(c1)); //multiply is commutative
        assertEquals(c1, c1.add(c2).add(c2.negate()));  //adding a number then adding its negate should cancel out
        assertEquals(c1, c1.negate().negate());         //double negation cancels out
        assertEquals(c0, c0.multiply(c2));              //multiply anything by (0,0) results in (0,0)
    }
}
