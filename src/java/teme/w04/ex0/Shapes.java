package teme.w04.ex0;

import static java.lang.Math.sqrt;

// VARIANTA MODIFICATA, CU FIELDUL CENTER (SI GETTER/SETTER) PUSE IN SHAPE DIRECT!
interface WithArea {
    double computeArea();

    default boolean isLargerThan(WithArea other) {
        return this.computeArea() > other.computeArea();
    }
}

interface WithGrowableArea extends WithArea {

    void grow(double factor);

    default void growUntilLargerThan(double targetArea, double factor) {
        while (computeArea() < targetArea) {
            grow(factor);
        }
    }
}


//================ SHAPES ================//
/*
a) Create an abstract class Shape, with methods:
        - double computeArea()
        - boolean isLargerThan(Shape other) - should return true if area of this > area of other
        - q: should you / can you mark these methods as abstract? what about private? final?
*/

/**
 * VARIANTA LUCRATa IN CLASA (bazata pe solutie Tudor)
 * CU TRIANGLE, SHAPE MODIFICAT SA CUPRINDA CENTRU, MAI MULTE INTERFETE DE BAZA, ETC!
 */

/*
Ex0: Warm-up: Shapes

You are given a Point class, (with 2 coordinates, can compute distance to other point)
Try to complete the steps below in such a way to reuse the Point class and avoid any code repetition

a) Create an abstract class Shape, with methods:
    - double computeArea()
    - boolean isLargerThan(Shape other) - should return true if area of this > area of other
  - q: should you / can you mark these methods as abstract? what about private? final?

b) Create a CircleShape class, extending Shape, which represents a circle of certain radius
  - also override toString() for it, for convenience
  - q: what fields should it have? constructors? any getters needed? other methods?
       what can/should we use as access level for each?
       could or should we make the class abstract? what about final?..

- Create a RectangleShape class, extending Shape, which represents a rectangle of specific height and width
  - same questions as for CircleShape
  - q: do you notice any code duplication already? (isLargerThan?) can we avoid that in anyway?

c) We want to be able to resize our 2 shapes.
   - create a WithGrowableArea interface, with one method:
     - void grow(double factor) - when called, it should resize current shape by this factor (size = size * factor)
   - change CircleShape and RectangleShape so they support the methods of the new interface

d) We want to position our shapes in 2D space, be able to move them and measure distances between them.
   - create an interface Movable, with methods:
     - Point getCenter();
     - void setCenter(Point center);
     - void move(double deltaX, double deltaY) - should move the center point of the shape adding deltaX/Y to its current x/y
     - double distanceTo(Movable other) - the distance between the center of this shape and center of other

   - create 2 new classes, named Circle/Rectangle, which are similar to CircleShape/RectangleShape,
     but also support the actions from Movable interface
     - how can you reuse existing classes/interfaces to minimize the amount of new code to write?
     - did you need to change anything in existing classes/interface? (how much, and why)
     - is there still any code duplication between circle/rectangle classes? can it be avoided?

e) Create a new class Square, which is similar to Rectangle, but always has the width the same as height
   - how can you reuse existing classes/interfaces to minimize the amount of code needed for the new class?
     how many methods do you still need to write in Rectangle class?
   - which of the existing classes/methods can we make final now? (respond before trying it in code)
     and would we have any reasons/benefits to do this?

f) We would like to add a new method: void growUntilBiggerThan(double targetArea, double factor),
   which repeatedly grows a shape (by the given factor), until its area is bigger than targetArea.
   Where (and how) should we add this method, so it can be used by concrete shape classes (Square,Circle,..),
   without repeating its implementation more than once?.. (do we need to change our class hierarchy? how?)

g) If we would need to add a new Elipse shape in the future, where would you include it
   in this class hierarchy? (should it be above/below Circle/CircleShape? why?)
   (note: an Elipse is basically an elongate shape, and unlike a circle it has 2 differet radiuses - see: https://www.math24.net/circle-ellipse/)

h) Would it be a good idea to make Point itself movable (remove final, add setters or a move() method)?
   Could we then remove the setCenter() from Movable? Can this cause any possible problems later?
   (think about 2 shapes build using same center point, them moved.. what would the distance between them be?)

i) Could we avoid somehow the need to repeat the declaration of the 'center' field (and setter/getter for it) in each Shape?..
   (hint: think about role/content of the Movable interfaces, Shape class)

j) Optional: draw an UML class diagram for all these classes/interfaces, either on paper or using an app.
  (UML info: https://medium.com/@smagid_allThings/uml-class-diagrams-tutorial-step-by-step-520fd83b300b,
   online tools: https://www.draw.io/, https://online.visual-paradigm.com/solutions/free-class-diagram-tool/, https://creately.com ,
   offline: https://wiki.gnome.org/Apps/Dia)
*/

public class Shapes {
    public static void main(String[] args) {

        Point p1 = new Point(1, 2);
        Point p2 = new Point(3, 4);
        System.out.println("p1: " + p1 + ", p2: " + p2 + ", dist: " + p1.distanceTo(p2));

        //--- Uncomment after solving a) b) ---//

        CircleShape cs = new CircleShape(5);
        RectangleShape rs = new RectangleShape(6, 8);
        System.out.println("\nCircleShape cs: " + cs);
        System.out.println("RectangleShape rs: " + rs);
        System.out.println("cs area: " + cs.computeArea());
        System.out.println("rs area: " + rs.computeArea());
        System.out.println("cs > rs ?: " + cs.isLargerThan(rs));
        System.out.println("rs > cs ?: " + rs.isLargerThan(cs));


        //--- Uncomment after solving c) ---//

        cs.grow(1.1);
        rs.grow(1.25);
        System.out.println("cs after grow 1.1x : cs: " + cs + ", area: " + cs.computeArea());
        System.out.println("rs after grow 1.25x : rs: " + rs + ", area: " + rs.computeArea());

        //--- Uncomment after solving d) ---//

        Circle c = new Circle(5, p1);
        Rectangle r = new Rectangle(6, 8, p1);

        System.out.println("\nCircle c: " + c);
        System.out.println("Rectangle r: " + r);
        System.out.println("c center: " + c.getCenter());
        System.out.println("c-r distance: " + c.distanceTo(r));
        System.out.println("r-c distance: " + r.distanceTo(c));
        c.move(1, 2);
        r.move(3, 4);
        System.out.println("\nafter move: ");
        System.out.println("c: " + c);
        System.out.println("r: " + r);
        System.out.println("c-r distance: " + c.distanceTo(r));
        System.out.println("r-c distance: " + r.distanceTo(c));


        //--- Uncomment after solving e) ---//

        Square s = new Square(7, p1);
        System.out.println("\nSquare: s: " + s + ", area: " + s.computeArea() + ", center: " + s.getCenter());
        System.out.println("s-r distance: " + s.distanceTo(r));
        s.move(0.5, 0.5);
        System.out.println("after move s: " + s);


        //--- Uncomment after solving f) ---//

        System.out.println("\ns > c ? " + s.isLargerThan(c) + " (s: " + s + ", s.area: " + s.computeArea() + "; c: " + c + ", c.area: " + c.computeArea() + ")");
        System.out.println("\nMaking the square bigger than circle:");
        s.growUntilLargerThan(c.computeArea(), 1.1);
        System.out.println("after: s > c ? " + s.isLargerThan(c) + " (s: " + s + ", s.area: " + s.computeArea() + "; c: " + c + ", c.area: " + c.computeArea() + ")");
        System.out.println("\nNow making the circle bigger again:");
        c.growUntilLargerThan(s.computeArea(), 1.1);
        System.out.println("after: c > s ? " + c.isLargerThan(s) + " (s: " + s + ", s.area: " + s.computeArea() + "; c: " + c + ", c.area: " + c.computeArea() + ")");

    }
}

//================ POINT ================//
class Point {
    private final double x, y;

    Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double distanceTo(Point other) {
        return sqrt(Math.pow(x - other.x, 2) + Math.pow(y - other.y, 2));
    }

    @Override
    public String toString() {
        return "Point{" + "x=" + x + ", y=" + y + '}';
    }
}

abstract class Shape implements WithGrowableArea {
    Point center;

    Point getCenter() {
        return center;
    }

    void setCenter(Point center) {
        this.center = center;
    }
}

/*
c) We want to be able to resize our 2 shapes.
        - create a WithGrowableArea interface, with one method:
        - void grow(double factor) - when called, it should resize current shape by this factor (size = size * factor)
        - change CircleShape and RectangleShape so they support the methods of the new interface
*/



/*
b) Create a CircleShape class, extending Shape, which represents a circle of certain radius
        - also override toString() for it, for convenience
        - q: what fields should it have? constructors? any getters needed? other methods?
        what can/should we use as access level for each?
        could or should we make the class abstract? what about final?..

        - Create a RectangleShape class, extending Shape, which represents a rectangle of specific height and width
        - same questions as for CircleShape
        - q: do you notice any code duplication already? (isLargerThan?) can we avoid that in anyway?
*/

class CircleShape extends MovableShape implements WithGrowableArea {
    private double radius;

    CircleShape(double radius) {
        this.radius = radius;
    }

    @Override
    public double computeArea() {
        return Math.PI * radius * radius;
    }

    @Override
    public String toString() {
        return "CircleShape{" +
                "radius=" + radius +
                '}';
    }

    @Override
    public void grow(double factor) {
        radius *= factor;
    }
}

class RectangleShape extends MovableShape implements WithGrowableArea {
    private double width;
    private double height;

    RectangleShape(double width, double height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public double computeArea() {
        return width * height;
    }

    @Override
    public String toString() {
        return "RectangleShape{" +
                "width=" + width +
                ", height=" + height +
                '}';
    }

    @Override
    public void grow(double factor) {
        width *= factor;
        height *= factor;
    }
}

/*
d) We want to position our shapes in 2D space, be able to move them and measure distances between them.
        - create an interface Movable, with methods:
        - Point getCenter();
        - void setCenter(Point center);
        - void move(double deltaX, double deltaY) - should move the center point of the shape adding deltaX/Y to its current x/y
        - double distanceTo(Movable other) - the distance between the center of this shape and center of other

        - create 2 new classes, named Circle/Rectangle, which are similar to CircleShape/RectangleShape,
        but also support the actions from Movable interface
     - how can you reuse existing classes/interfaces to minimize the amount of new code to write?
             - did you need to change anything in existing classes/interface? (how much, and why)
        - is there still any code duplication between circle/rectangle classes? can it be avoided?
*/

abstract class MovableShape extends Shape {

    void move(double deltaX, double deltaY) {
        setCenter(new Point(getCenter().getX() + deltaX, getCenter().getY() + deltaY));
    }

    double distanceTo(Shape other) {
        return getCenter().distanceTo(other.getCenter());
    }
}

final class Circle extends CircleShape {

    Circle(double radius, Point center) {
        super(radius);
        this.center = center;
    }

    @Override
    public Point getCenter() {
        return center;
    }

    @Override
    public void setCenter(Point center) {
        this.center = center;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "circle=" + super.toString() +
                ", center=" + center +
                '}';
    }
}

class Rectangle extends RectangleShape {

    Rectangle(double width, double height, Point center) {
        super(width, height);
        this.center = center;
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "rectangle=" + super.toString() +
                ", center=" + center +
                '}';
    }
}

/*
e) Create a new class Square, which is similar to Rectangle, but always has the width the same as height
   - how can you reuse existing classes/interfaces to minimize the amount of code needed for the new class?
        how many methods do you still need to write in Rectangle class?
        - which of the existing classes/methods can we make final now? (respond before trying it in code)
        and would we have any reasons/benefits to do this?
*/

class Square extends Rectangle {

    Square(double side, Point center) {
        super(side, side, center);
    }
}

class Triangle extends Shape implements WithGrowableArea {
    private double side;

    Triangle(double side, Point center) {
        this.side = side;
        this.center = center;
    }

    @Override
    public double computeArea() {
        return 0; //TODO!!!
    }

    @Override
    public void grow(double factor) {
        side *= factor;
    }

    @Override
    public String toString() {
        return "Triangle{" +
                "side=" + side +
                ", center=" + center +
                '}';
    }
}