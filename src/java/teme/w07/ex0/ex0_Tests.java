package teme.w07.ex0;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static teme.w07.ex0.Ex0.toPositiveInt;

public class ex0_Tests {
    private final static double DELTA = 0.001; //precision used for comparing doubles

    //helper method for easy building a new Array with some given elements (of a custom type)
    private static <E> ArrayList<E> newList(E... elements) {
        return new ArrayList<>(Arrays.asList(elements));
    }

    @Test
    public void testEx0() {
        try {
            toPositiveInt("-1");
            fail("Negative number");
        } catch (NotANumberException | NegativeNumberException e) {
            //ok, expected to get this exception when test works well (so just 'bury' it)
        }
        try {
            assertEquals(10, toPositiveInt("10"), DELTA);
        } catch (NotANumberException | NegativeNumberException e) {
            fail("Not a positive number");
        }
        try {
            toPositiveInt("ana");
            fail("Not a number exception");
        } catch (NotANumberException | NegativeNumberException e) {
//ok, expected to get this exception when test works well (so just 'bury' it)
        }

        List<String> stringValues = new ArrayList<>();
        List<Integer> intValues = toPositiveInt(stringValues);
        assertTrue(intValues.isEmpty());
        assertEquals(0, intValues.size());

        stringValues.add("lucian");
        intValues = toPositiveInt(stringValues);
        assertEquals(0, intValues.size());

        stringValues.add("-1");
        intValues = toPositiveInt(stringValues);
        assertEquals(0, intValues.size());

        stringValues.add("1234");
        intValues = toPositiveInt(stringValues);
        assertEquals(newList(1234), intValues);

    }
}