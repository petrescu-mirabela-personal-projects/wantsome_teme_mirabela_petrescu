package teme.w07.ex3;

/*
d. Create a class named Print that implements the Function interface, and its
implementation of evaluate() method simply prints out the give int value, and then also
returns it (as is, no changes).
 */
public class Print implements Function {
    @Override
    public int evaluate(int value) {
        System.out.println("Value = " + value);
        return value;
    }
}
