package teme.w07.ex3;

import org.junit.Test;

import static org.junit.Assert.*;

/*
f. Optional: write a JUnit test to test the functionality of Compute and Half classes
- Question: can you write a similar test for the Print class? Is it easier or harder to
do than for Half class? (why?)
 */
public class HalfAndPrint_Tests {

    @Test
    public void testHalfAndPrint() {
        int[] arr1 = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        Compute compute = new Compute();
        Print print = new Print();
        Half half = new Half();

        assertTrue(print instanceof Function);
        assertTrue(half instanceof Function);
        assertFalse(compute instanceof Function);

        assertArrayEquals(arr1, compute.compute(arr1, print));  // test the Print class of the initial array

        // After half
        // it is harder that to test the Print class
        // because print receives an int value and we have to go through all the initial array to have the half values
        int[] arr2 = compute.compute(arr1, half);
        int[] arr3 = new int[arr1.length];  // array which shall contain the values of arr1 divided by 2
        for (int i = 0; i < arr1.length; i++) {
            arr3[i] = arr1[i] / 2;
        }
        assertArrayEquals(arr3, arr2);  // test the Half class
        assertArrayEquals(arr3, compute.compute(arr3, print));  // test the Print of the Half class result
        assertArrayEquals(arr2, compute.compute(arr2, print));  // test the Print class

    }

}