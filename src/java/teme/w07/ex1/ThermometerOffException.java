package teme.w07.ex1;

class ThermometerOffException extends Exception {
    ThermometerOffException() {
        super("Thermometer is off");
    }
}
