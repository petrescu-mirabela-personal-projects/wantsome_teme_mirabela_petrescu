package teme.w07.ex1;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AverageSensor implements Sensor {
    /* . Contains multiple instances of sensors
     */
    List<Sensor> sensorList = new ArrayList<>();

    public AverageSensor() {
    }

    public AverageSensor(List<Sensor> sensorList) {
        this.sensorList = sensorList;
    }

    @Override
    public String toString() {
        return sensorList + "";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AverageSensor)) return false;
        AverageSensor that = (AverageSensor) o;
        return Objects.equals(sensorList, that.sensorList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sensorList);
    }

    /* b. Has an additional method addSensor which adds a new sensor
     */
    public void addSensor(Sensor sensor) {
        sensorList.add(sensor);
    }

    /* c. The average sensor is on when all of it’s sensors are on
     */
    @Override
    public boolean isOn() {
        for (Sensor sensorItem : sensorList) {
            if (!sensorItem.isOn()) {
                return false;
            }
        }
        return true;
    }

    /* d. When the average sensor switched on all of its sensors are switched on if not already on
     */
    @Override
    public void on() {
        for (Sensor sensorItem : sensorList) {
            if (!sensorItem.isOn()) {
                sensorItem.on();
            }
        }
    }

    /* e. When the average sensor is switched off some of its sensors are switched off (at least one)
     */
    @Override
    public void off() {
        for (Sensor sensorItem : sensorList) {
            if (sensorItem.isOn()) {
                sensorItem.off();
            }
        }
    }

    /* f. The measure method will return an average of the measured values of all its registered sensors
    g. If the measure method is called when the sensor is off or when there are no sensors registered to the average sensor throw an exception
     */
    @Override
    public int measure() throws ThermometerOffException, EmptyAverageSensorException {
        if (!sensorList.isEmpty()) {
            if (isOn()) {
                int averageMeasure = 0;
                for (Sensor sensorItem : sensorList) {
                    averageMeasure += sensorItem.measure();
                }
                return averageMeasure / sensorList.size();
            }
            throw new ThermometerOffException();
        }
        throw new EmptyAverageSensorException();
    }
}
