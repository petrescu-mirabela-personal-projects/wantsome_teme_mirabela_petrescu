package teme.w07.ex1;

class EmptyAverageSensorException extends Exception {
    public EmptyAverageSensorException() {
        super("Empty average sensor list");
    }
}
