package teme.w07.ex1;

import java.util.Random;

public class ThermometerSensor implements Sensor {
    private int thermometerSensorTemp;
    private boolean state = false;  // After creation the thermometer is off

    public ThermometerSensor() {
    }

    @Override
    public String toString() {
        return "ThermometerSensor{" +
                "thermometerSensorTemp=" + thermometerSensorTemp +
                '}';
    }

    @Override
    public boolean isOn() {
        return state;
    }

    @Override
    public void on() {
        state = true;
    }

    @Override
    public void off() {
        state = false;
    }

    @Override
    public int measure() throws ThermometerOffException {
        if (state) {
        /* In order to generate a random number between -30 and 30 we create an object of java.util.Random class and call its nextInt method with 61 as argument.
        This will generate a number between 0 and 60 and add -30 to the result which will make the range of the generated value as -30 to 30.
        */
            Random randomGenerator = new Random();
            int randomInt = randomGenerator.nextInt(61) - 30;
            return thermometerSensorTemp = randomInt;
        }
        throw new ThermometerOffException();
    }
}
