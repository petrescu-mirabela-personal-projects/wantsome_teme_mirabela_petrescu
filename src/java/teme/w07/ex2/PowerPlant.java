package teme.w07.ex2;

/*
1. A PowerPlant class that:
a. Receives as parameter in the constructor an int that signifies the desired output of the nuclear plant.
b. Has a method that returns the desired output
c. Has a method soundAlert() which prints an alarm message to console
 */

class PowerPlant {
    private final int desiredOutputNuclearPlant;  // the desired output of the nuclear plant

    public PowerPlant(int desiredOutputNuclearPlant) {
        this.desiredOutputNuclearPlant = desiredOutputNuclearPlant;
    }

    /* returns the desired output
     */
    public int desiredOutput() {
        return desiredOutputNuclearPlant;
    }

    /* prints an alarm message to console
     */
    public void soundAlert() {
        System.out.println("\n>>>>>> Alarm message >>>>>>\n");
    }
}
