package teme.w07.ex2;

import org.junit.Test;

import static org.junit.Assert.*;

public class PlantController_Tests {

    /* e. Has a run() method that runs constantly and checks whether the reactor needs
    adjustment and calls the adjust method. If the reactor goes critical call the sound
    alarm method and shutdown the reactor.
     */
    @Test
    public void testRunPlantController() {
        System.out.println("\n>>>>> testRunPlantController test ");
        /* Write a test class which creates a power plant with desired output is 120, using an reactor for
        which the critical threshold is 150. The test should also create a plant controller, on which it
        should call the run() method.
        */
        PowerPlant powerPlant = new PowerPlant(120);
        Reactor reactor = new Reactor(150);
        PlantController plantController = new PlantController(powerPlant, reactor);

        assertEquals(120, powerPlant.desiredOutput());
        assertEquals(150, reactor.getPowerLevelReactorCritical());

        /* Run the test a few times, and check that only one of these 2 valid scenarios happen:
        - The power plant reaches a stable output (with difference between it and the desired
        output being less than 10 units), and alarm is not sounded / the power remains running
        - OR: after a few increases, the reactor goes critical; then the alarm should be sounded
        and the reactor should be shutdown, and after a few decrease steps it should reach
        current output of 0.
        */
        assertEquals(120, powerPlant.desiredOutput());
        assertEquals(150, reactor.getPowerLevelReactorCritical());
        assertEquals(0, reactor.currentPowerLevel());   // power output level at which the reactor goes critical, starts with a power output of 0.
        assertTrue(plantController.needAdjustment());
        assertEquals(0, plantController.getState());    // adjust method run once (Level 0)
        plantController.run();
        assertTrue(plantController.getState() != 0);    // at least one state was changed
        System.out.println("testRunPlantController call - catch");
        assertTrue(reactor.currentPowerLevel() < reactor.getPowerLevelReactorCritical());

        /* The power plant reaches a stable output (with difference between it and the desired
        output being less than 10 units), and alarm is not sounded / the power remains running
        */
        if ((powerPlant.desiredOutput() - reactor.currentPowerLevel()) >= 10) {
            assertTrue(reactor.currentPowerLevel() < reactor.getPowerLevelReactorCritical());
        }
        /* OR: after a few increases, the reactor goes critical; then the alarm should be sounded
        and the reactor should be shutdown, and after a few decrease steps it should reach current output of 0.
        */
        if (reactor.currentPowerLevel() >= reactor.getPowerLevelReactorCritical()) {
            assertEquals(0, reactor.currentPowerLevel());   // final power level value shall be 0
        }
    }

    @Test
    public void testIncreaseReactor() {
        System.out.println("\n>>>>> testIncreaseReactor test ");
        PowerPlant powerPlant = new PowerPlant(120);
        Reactor reactor = new Reactor(150);
        assertEquals(120, powerPlant.desiredOutput());
        assertEquals(150, reactor.getPowerLevelReactorCritical());

        // check if the current power level was increased with a value in range 1 100
        int previousPowerLevel = reactor.currentPowerLevel();
        try {
            reactor.increasePowerLevel();
            assertTrue((reactor.currentPowerLevel() - previousPowerLevel) <= 100);   // adjust case
            assertTrue((reactor.currentPowerLevel() - previousPowerLevel) >= 0);     // adjust case
        } catch (ReactorCriticalException e) {
            System.out.println("testIncreaseReactor call - catch");
        }

        // test the exception from increasePowerLevel method
        try {
            reactor.increasePowerLevel();
            reactor.increasePowerLevel();
            reactor.increasePowerLevel();
            reactor.increasePowerLevel();
            reactor.increasePowerLevel();
            fail("Out of critical range");
        } catch (ReactorCriticalException e) {
            System.out.println("testIncreaseReactor call - catch");
        }

    }

    @Test
    public void testDecreaseReactor() {
        System.out.println("\n>>>>> testDecreaseReactor test ");
        PowerPlant powerPlant = new PowerPlant(120);
        Reactor reactor = new Reactor(150);
        assertEquals(120, powerPlant.desiredOutput());  // test the PowerPlant class constructor
        assertEquals(150, reactor.getPowerLevelReactorCritical());  // test the Reactor class constructor

        try {
            // call increase some times to reach a stable/critical power level
            // if an exception is thrown, then some of increasePowerLevel() calls will not be reached
            reactor.increasePowerLevel();
            reactor.increasePowerLevel();
            reactor.increasePowerLevel();
            reactor.increasePowerLevel();
            reactor.increasePowerLevel();
        } catch (ReactorCriticalException e) {
            System.out.println("testDecreaseReactor call - catch");
        }

        // check if the current power level was decreased with a value in range 1 100
        int previousPowerLevel1 = reactor.currentPowerLevel();
        reactor.decreasePowerLevel();
        assertTrue((reactor.currentPowerLevel() - previousPowerLevel1) <= 100 || ((reactor.currentPowerLevel() - previousPowerLevel1) >= 0));   // adjust case

        // Test: The decrease method should not decrease the output to negative values (but only down to 0)
        while (reactor.currentPowerLevel() != 0) {
            reactor.decreasePowerLevel();
            assertTrue((reactor.currentPowerLevel() - previousPowerLevel1) <= 100 || ((reactor.currentPowerLevel() - previousPowerLevel1) >= 0));   // adjust case}
        }
        assertEquals(0, reactor.currentPowerLevel());
    }

    /* If the reactor goes critical call the sound alarm method
     */
    @Test
    public void testAlert() {
        System.out.println("\n>>>>> testAlert test ");
        PowerPlant powerPlant = new PowerPlant(120);
        Reactor reactor = new Reactor(150);

        try {
            // call increase some times to reach a stable/critical power level
            // if an exception is thrown, then some of increasePowerLevel() calls will not be reached
            reactor.increasePowerLevel();
            reactor.increasePowerLevel();
            reactor.increasePowerLevel();
            reactor.increasePowerLevel();
            reactor.increasePowerLevel();
        } catch (ReactorCriticalException e) {
            System.out.println("testAlert called - catch");
            powerPlant.soundAlert();
            assertTrue(reactor.currentPowerLevel() <= reactor.getPowerLevelReactorCritical());
            while (reactor.currentPowerLevel() != 0) {
                reactor.decreasePowerLevel();
                if (reactor.currentPowerLevel() == 0) {
                    System.out.println("testAlert call - catch - current power level equal to 0");
                    assertEquals(0, reactor.currentPowerLevel());
                }
            }
        }
    }

    /* c. Has a method adjust() which adjust the output by repeatedly calling the
increase() method of the reactor as long as needed (until needAdjustment() returns false)
     */
    @Test
    public void testAdjustShutDown() {
        System.out.println("\n>>>>> testAdjustShutDown test ");
        PowerPlant powerPlant = new PowerPlant(120);
        Reactor reactor = new Reactor(150);
        PlantController plantController = new PlantController(powerPlant, reactor);

        assertTrue(plantController.needAdjustment());
        assertEquals(0, plantController.getState());
        try {
            assertTrue(plantController.needAdjustment());
            plantController.adjust();
            /* The power plant reaches a stable output (with difference between it and the desired
            output being less than 10 units), and alarm is not sounded / the power remains running
            */
            if ((powerPlant.desiredOutput() - reactor.currentPowerLevel()) >= 10) {
                assertTrue(reactor.currentPowerLevel() < reactor.getPowerLevelReactorCritical());
            }
            /* OR: after a few increases, the reactor goes critical; then the alarm should be sounded
            and the reactor should be shutdown, and after a few decrease steps it should reach current output of 0.
            */
            if (reactor.currentPowerLevel() >= reactor.getPowerLevelReactorCritical()) {
                assertEquals(0, reactor.currentPowerLevel());   // final power level value shall be 0
            }
        } catch (ReactorCriticalException e) {
            plantController.shutdown();
            System.out.println("testAdjust branch reached");
        }
    }
}