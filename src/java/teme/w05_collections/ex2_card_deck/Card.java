package teme.w05_collections.ex2_card_deck;

/* Create a Card class to remember the individual cards. It should have 2 properties:
- number (2 - 14)
- suit (one of these types: diamonds ( ♦ ), clubs (♣), hearts ( ♥ ) and spades (♠) ).
 */

import java.util.Objects;

enum Suit {
    DIAMONDS,
    CLUBS,
    HEARTS,
    SPADES
}

class Card {

    private final int number;
    private final Suit cards;

    public Card(int number, Suit cards) {
        this.number = number;
        this.cards = cards;
    }

    @Override
    public String toString() {
        return "Card{" +
                number + " " +
                cards +
                "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return number == card.number &&
                cards == card.cards;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, cards);
    }
}
