package teme.w05_collections.ex1_building_registry;

/* Create a Building class representing a building, with the following properties:
● name
● category (possible values: residential, office, hospital, religious)
● price
● neighborhood
Tip: you should use an enum for the category values/field.
*/

import java.util.Objects;

enum Category {
    RESIDENTIAL,
    OFFICE,
    HOSPITAL,
    RELIGIOUS
}

class Building {
    private final String name;
    private final Category category;
    private final double price;
    private final String neighborhood;

    public Building(String name, Category category, int price, String neighborhood) {
        this.name = name;
        this.category = category;
        this.price = price;
        this.neighborhood = neighborhood;
    }

    public double getPrice() {
        return price;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public Category getCategory() {
        return category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Building)) return false;
        Building building = (Building) o;
        return Double.compare(building.price, price) == 0 &&
                name.equals(building.name) &&
                category == building.category &&
                neighborhood.equals(building.neighborhood);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, category, price, neighborhood);
    }
}
