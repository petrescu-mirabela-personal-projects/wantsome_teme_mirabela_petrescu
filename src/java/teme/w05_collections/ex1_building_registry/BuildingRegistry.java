package teme.w05_collections.ex1_building_registry;

import java.util.*;

/*
1. Buildings Registry

a) Create a Building class representing a building, with the following properties:
- name
- category (possible values: residential, office, hospital, religious)
- price
- neighborhood

Tip: you should use an enum for the values of category field.


b) Create a BuildingRegistry class, containing these static methods (each receiving a List<Building> as input parameter, and you need to decide on the return type):

- categoriesCount() - returns the number of categories (of the actual buildings, not all possible categories)
- neighborhoodsList() - return the list of names of unique neighborhoods, sorted alphabetically (hint: may use a Set; what implementation can you use to also help you with sorting?..)

- averagePriceForOneCategory() - return the average price for building from only one category (given as a 2nd input parameter to the method)
  - question: what should be the return type for this method?..

- averagePricePerCategory() - return the average price for each building category (for ALL defined categories, even the ones without building)
  - question: what kind of return type should this method have? (as it should return an average price for each used category, so kind of returning a list of pairs of 2 values - category and price)

- averagePricePerNeighborhood() - return the average price for each neighborhood.
*/

public class BuildingRegistry {

    /* return the number of categories (of the actual buildings, not all possible categories)
     */
    public static int categoriesCount(List<Building> buildings) {

        Set<Category> setCategory = new HashSet<>();
        for (Building itemBuilding : buildings) {
            setCategory.add(itemBuilding.getCategory());
        }
        return setCategory.size();
    }

    /* return the list of names of unique neighborhoods, sorted alphabetically
    (hint: may use a Set; what implementation can you use to also help you with sorting?..)
     */
    public static List<String> neighborhoodsList(List<Building> buildings) {

        Set<String> setNeighborhoods = new TreeSet<>();
        for (Building itemBuilding : buildings) {
            setNeighborhoods.add(itemBuilding.getNeighborhood());
        }
        return new ArrayList<>(setNeighborhoods);
    }

    /* return the average price for building from only one category (given as a 2nd input parameter to the method)
     */
    public static double averagePriceForOneCategory(List<Building> buildings, Category category) {
        double averagePrice = 0;
        if ((buildings.size() > 0)) {
            int count = 0;
            for (Building building : buildings) {
                if (building.getCategory().equals(category)) {
                    averagePrice += building.getPrice();
                    count++;
                }
            }
            averagePrice = count > 0 ? averagePrice / count : 0;
        }
        return averagePrice;
    }

    /* return the average price for each building category (for ALL defined categories, even the ones without building)
  As it should return an average price for each used category, so kind of returning a list of pairs of 2 values - category and price.
  */
    public static Map<Category, Double> averagePricePerCategory(List<Building> buildings) {
        Map<Category, Double> mapPricePerCategory = new HashMap<>();
        for (Category categoryItem : Category.values()) {
            mapPricePerCategory.put(categoryItem, averagePriceForOneCategory(buildings, categoryItem));
        }
        return mapPricePerCategory;
    }

    /* return the average price for one neighborhood given as 2nd input parameter to the method
     */
    private static double averagePriceForOneNeighborhood(List<Building> buildings, String neighborhood) {
        double averagePrice = 0;
        int count = 0;
        for (Building building : buildings) {
            if (building.getNeighborhood().equals(neighborhood)) {
                averagePrice += building.getPrice();
                count++;
            }
        }
        averagePrice /= count;
        return averagePrice;
    }

    /* return the average price for each neighborhood .
     */
    public static Map<String, Double> averagePricePerNeighborhood(List<Building> buildings) {
        Map<String, Double> mapPricePerNeighborhood = new HashMap<>();
        for (String neighborhoodsItem : neighborhoodsList(buildings)) {
            mapPricePerNeighborhood.put(neighborhoodsItem, averagePriceForOneNeighborhood(buildings, neighborhoodsItem));
        }
        return mapPricePerNeighborhood;
    }

    /**
     * Some manual tests
     */
    public static void main(String[] args) {


        List<Building> buildings = Arrays.asList(
                new Building("a", Category.OFFICE, 10, "tudor"),
                new Building("b", Category.OFFICE, 40, "centru"),
                new Building("c", Category.OFFICE, 20, "pacurari"),
                new Building("d", Category.RESIDENTIAL, 15, "pacurari"),
                new Building("e", Category.HOSPITAL, 35, "pacurari"),
                new Building("f", Category.HOSPITAL, 30, "copou"));

        System.out.println("Actual categories: " + categoriesCount(buildings));
        System.out.println("Actual neighborhoods: " + neighborhoodsList(buildings));

        System.out.println("Average price per categ: " + averagePricePerCategory(buildings));
        System.out.println("Average price per neighborhood: " + averagePricePerNeighborhood(buildings));
    }
}
