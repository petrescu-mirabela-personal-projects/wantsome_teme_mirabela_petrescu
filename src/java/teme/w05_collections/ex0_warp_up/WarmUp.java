package teme.w05_collections.ex0_warp_up;

import java.util.*;

public class WarmUp {

    public static void main(String[] args) {


        String s = "Once upon a time in a land far far away there lived a great king whose name was a great mystery";


        /* a) Print the number of all words (including any duplicates). Hint: may use
            String.split(), regexp..
         */
        List<String> sList = new ArrayList<>(Arrays.asList(s.toLowerCase().split("\\s+")));
        System.out.println("\nNumber of words: " + sList.size());


        /* b) Print all words in the initial order they appear in text.
         */
        System.out.println(" ------ Print the number of all words (including any duplicates) ------ ");
        System.out.println(sList);

        /* c) Print all words, but sorted alphabetically. Hint: see Collections.sort()
         */
        /* Collections.sort method is sorting the elements of ArrayList in ascending order. */
        System.out.println("\n ------ Print all words, but sorted alphabetically ------ ");
        Collections.sort(sList);
        System.out.println(sList.toString());

        /* d) Print the number of unique words.
         */
        Set<String> sHashSet = new HashSet<>(sList);
        System.out.println("\nNumber of unique words with HashSet: " + sHashSet.size());


        /* e) Print all unique words, in the initial order they appear in text.
         */
        Set<String> sLinkedHashSet = new LinkedHashSet<>(Arrays.asList(s.toLowerCase().split("\\s+")));
        System.out.println("\nNumber of unique words with LinkedHashSet: " + sLinkedHashSet.size());
        System.out.println(" ------ Print all unique words, in the initial order they appear in text. ------ ");
        System.out.println(sLinkedHashSet.toString());


        /* f) Print all unique words, sorted alphabetically.
         */
        Set<String> sTreeSet = new TreeSet<>(Arrays.asList(s.toLowerCase().split("\\s+")));
        System.out.println(" ------ Print all unique words, sorted alphabetically ------ ");
        System.out.println(sTreeSet.toString());


        /* g) For each word, count how many times it appears, and then print all unique words
            (in their initial order in text) together with the number of times each appears.
        */
        Map<String, Integer> orderedMapByWord = new LinkedHashMap<>();
        for (String word : s.toLowerCase().split("\\s+")) {
            Integer count = orderedMapByWord.get(word);
            if (count == null) {
                orderedMapByWord.put(word, 1);
            } else {
                orderedMapByWord.put(word, count + 1);
            }
        }
        System.out.println(" ------ For each word, count how many times it appears, and then print all unique words ------ ");
        System.out.println(orderedMapByWord);

        /* h) The same, but now sort the word-count pairs alphabetically by word.
         */
        Map<String, Integer> sortedMapByWord = new TreeMap<>(orderedMapByWord);
        System.out.println(" ------ sort the word-count pairs alphabetically by word ------ ");
        System.out.println(sortedMapByWord);

    }
}
