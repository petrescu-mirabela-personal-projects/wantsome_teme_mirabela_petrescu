package my_work.test_s5_s7;

class ExceptionA extends Exception {
}

class ExceptionB extends ExceptionA {
}

public class Test3 {

    public static void main(String[] args) {
        Test3 t = new Test3();

        System.out.print("1 ");

        try {
            System.out.print("2 ");

            t.thrower();

            System.out.print("3 ");

        } catch (ExceptionB b) {
            System.out.print("4 ");

        } catch (ExceptionA a) {
            System.out.print("5 ");
        }
    }

    void thrower() throws ExceptionB {
        throw new ExceptionB();
    }


}
