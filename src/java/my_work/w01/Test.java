package my_work.w01;

// exercitii_extra.txt
/*
===============================================

(Tricky?) questions:

3) Can static be also applied to classes?
   A: yes, to inner classes (only)


4) What will be the output of this program?

    public class Test {

        public static String toString(){
            System.out.println("Test toString called");
            return "";
        }

        public static void main(String args[]){
            System.out.println(toString());
        }
    }

    A: actually gives a compile error - but why?...



5) What will be the output of this program?

    public class Test {

        public static String foo(){
            System.out.println("Test foo called");
            return "";
        }

        public static void main(String args[]){
            Test obj = null;
            System.out.println(obj.foo());
        }
    }

    A: it works! (no compile error) - but why?...

       (why: works, without NPE, as obj.foo() will be translated to just accessing the static Test.foo() method,
             by a compiler optimization, even if obj is actually null)

-------------------------------------------------------------------------------------------------------------------
 */

public class Test {

    public static String foo() {
        System.out.println("Test foo called");
        return "";
    }

    public static void main(String args[]) {
        // not enough --> shall be added the def/instantiation of Test class
        // as it is now toString() is not a static method to belong to Test class -> it belongs to the instance
        Test t = new Test();    // this shall be added to work
        System.out.println(t.toString());

        // A null indicates that a variable doesn't point to any object and holds no value.
        Test obj = null;                // no instantiation is needed sice we call foo(), a static function
        System.out.println(obj.foo());  // calls the static method
    }

    //public static String toString(){  // static toString() can not override the toString() method of Object class
    public String toString() {
        System.out.println("Test toString called");
        return "";
    }
}