package my_work.w01;

public class Test1 {
    public static void main(String[] args) {

        String s = "HelloWorld".substring(0, 9);
        System.out.println(s);

        String a = new String();

        a.trim();

//        Child child = new Child( );
//        child.validate( );
//        System.out.println( child.value );
//
//        char c= '\n';

        Parent c = new Child();
        c.say("test");

        A a1 = new A();
        B b1 = new B();

        a1.printName();
        b1.printName();
    }
}

class Parent {
    void say(Object obj) {
        System.out.println("parent");
    }
}

class Child extends Parent {
    void say(Object obj) {
        System.out.println("child");
    }
}

//class Parent {
//    int value;
//
//    void validate( ) {
//        value = value + 10;
//    }
//}
//
//class Child extends Parent {
//    int value;
//
//    void validate( )  {
//        super.validate( );
//        value = value - 2;
//    }
//}

class A {
    static String name = "A";

    void printName() {
        System.out.print(name);
    }
}

class B extends A {
    B() {
        name = "B";
    }
}