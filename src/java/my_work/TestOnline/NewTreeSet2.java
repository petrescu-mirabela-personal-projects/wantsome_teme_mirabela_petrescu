package my_work.TestOnline;

import java.util.*;

public class NewTreeSet2 extends NewTreeSet {
    public static void main(String[] args) {
        NewTreeSet2 t = new NewTreeSet2();
        t.count();
    }
}

class NewTreeSet    // protected not allowed here
{
    void count() {
        for (int x = 0; x < 7; x++, x++) {
            System.out.print(" " + x);
        }
    }
}
