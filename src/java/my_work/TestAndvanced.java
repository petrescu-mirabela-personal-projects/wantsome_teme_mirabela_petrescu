package my_work;

public class TestAndvanced {

    public static void main(String[] args) {
        Subclass s1 = new Subclass();
        s1.foo();

        Super s = new Subclass();
        //s.foo();

        int i = 0, j = 9;
        do {
            i++;
            if (j < i) {
                break;
            } else {
                j--;
                i++;
            }
        } while (i > 5);
        System.out.println(i + "" + j);

        String str = "Java";
        char ch = str.charAt(2);
        System.out.println(ch);
    }
}

class Super {
    private void foo() {
        System.out.println("Super");
    }
}

class Subclass extends Super {
    public void foo() {
        System.out.println("Subclass");
    }
}