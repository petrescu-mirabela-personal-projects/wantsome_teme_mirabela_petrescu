package my_work.interview_problems.player_weapon;

public abstract class Weapon {

    private String name;
    private int strength;

    protected abstract void attack();
}
