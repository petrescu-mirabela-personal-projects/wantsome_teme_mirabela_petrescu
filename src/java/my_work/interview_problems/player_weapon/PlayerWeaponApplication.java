package my_work.interview_problems.player_weapon;

// Link: https://tests4geeks.com/java-interview-questions/#q1

public class PlayerWeaponApplication {

    public static void main(String[] args) {

        Player player1 = new Player(new Knife(), "Knife");

        player1.action();

        Weapon revolver = new Revolver();
        Player player2 = new Player(revolver, "Revolver");
        player2.action();

    }

}
