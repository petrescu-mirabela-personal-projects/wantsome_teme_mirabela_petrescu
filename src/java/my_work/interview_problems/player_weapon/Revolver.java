package my_work.interview_problems.player_weapon;

public class Revolver extends Weapon {
    @Override
    protected void attack() {
        System.out.println("Perform revolver attack");
    }
}
