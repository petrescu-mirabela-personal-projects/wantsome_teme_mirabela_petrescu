package my_work.interview_problems.player_weapon;

public class Knife extends Weapon {

    @Override
    protected void attack() {
        System.out.println("Perform knife attack");
    }
}
