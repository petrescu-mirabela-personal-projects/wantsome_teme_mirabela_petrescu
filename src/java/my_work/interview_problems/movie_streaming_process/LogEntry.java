package my_work.interview_problems.movie_streaming_process;

import java.util.Date;

public class LogEntry {

    private Date dateMovieWatched;
    private String movieName;
    private String movieLength;
    private String minutesWatched;
    private String movieGenre;

    public LogEntry(Date dateMovieWatched, String movieName, String movieLength, String minutesWatched, String movieGenre) {
        this.dateMovieWatched = dateMovieWatched;
        this.movieName = movieName;
        this.movieLength = movieLength;
        this.minutesWatched = minutesWatched;
        this.movieGenre = movieGenre;
    }

    public Date getDateMovieWatched() {
        return dateMovieWatched;
    }

    public String getMovieName() {
        return movieName;
    }

    public String getMovieLength() {
        return movieLength;
    }

    public String getMinutesWatched() {
        return minutesWatched;
    }

    public String getMovieGenre() {
        return movieGenre;
    }

    @Override
    public String toString() {
        return "LogEntry{" +
                "dateMovieWatched=" + dateMovieWatched +
                ", movieName='" + movieName + '\'' +
                ", movieLength='" + movieLength + '\'' +
                ", minutesWatched='" + minutesWatched + '\'' +
                ", movieGenre='" + movieGenre + '\'' +
                "}\n";
    }
}
