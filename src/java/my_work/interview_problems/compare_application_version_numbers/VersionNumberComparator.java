package my_work.interview_problems.compare_application_version_numbers;

// Link: https://issuu.com/net-boss/docs/top_30_java_interview_coding_tasks

/*
an application version number is used to "tag" the released version of an application
The numbers at the left side are more valuable than numbers on the right side of the dot

15.1.1 > 14.13.10
14.13.10 > 14.10.55
14.10.55 > 14.10.20

 */

import java.util.Arrays;
import java.util.Comparator;

public class VersionNumberComparator implements Comparator<String> {

    public static void main(String[] args) {

    }


    @Override
    public int compare(String version1, String version2) {

        Integer[] array1 = Arrays.stream(version1.split("\\."))
                .map(Integer::parseInt)
                .toArray(Integer[]::new);

        Integer[] array2 = Arrays.stream(version2.split("\\."))
                .map(Integer::parseInt)
                .toArray(Integer[]::new);

        int length1 = array1.length;
        int length2 = array2.length;
        int idx = 0;

        while (idx < length1 || idx < length2) {
            if (idx < length1 && idx < length2) {
                if (array1[idx] < array2[idx]) {
                    return -1;
                } else if (array1[idx] > array2[idx]) {
                    return 1;
                }
            } else if (idx < length1) {
                if (array1[idx] != 0) {
                    return 1;
                } else if (idx < length2) {
                    if (array2[idx] != 0) {
                        return -1;
                    }
                }
                idx++;
            }
        }
        return 0;
    }
}
