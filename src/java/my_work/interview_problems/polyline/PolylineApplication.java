package my_work.interview_problems.polyline;

import java.util.ArrayList;

// Link: https://tests4geeks.com/java-interview-questions/#q1

public class PolylineApplication {

    public static void main(String[] args) {

        System.out.println(Polyline.create().add(new Point(0, 1)).add(new Point(1, 2)).add(new Point(2, 3)));

        System.out.println(Polyline.create().add(new Point(0, 1)).add(new Point(1, 2)).add(new Point(2, 3)).getPoint(1));

        System.out.println(Polyline.create().add(new Point(0, 1)).add(new Point(1, 2)).add(new Point(2, 3)).size());


    }


}
